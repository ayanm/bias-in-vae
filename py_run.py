import os

attrs =  ['5_o_Clock_Shadow', 'Arched_Eyebrows', 'Attractive', 'Bags_Under_Eyes', 'Bald', 'Bangs', 'Big_Lips', 'Big_Nose', 'Black_Hair', 'Blond_Hair', 'Blurry', 'Brown_Hair', 'Bushy_Eyebrows', 'Chubby', 'Double_Chin', 'Eyeglasses', 'Goatee', 'Gray_Hair', 'Heavy_Makeup', 'High_Cheekbones', 'Male', 'Mouth_Slightly_Open', 'Mustache', 'Narrow_Eyes', 'No_Beard', 'Oval_Face', 'Pale_Skin', 'Pointy_Nose', 'Receding_Hairline', 'Rosy_Cheeks', 'Sideburns', 'Smiling', 'Straight_Hair', 'Wavy_Hair', 'Wearing_Earrings', 'Wearing_Hat', 'Wearing_Lipstick', 'Wearing_Necklace', 'Wearing_Necktie', 'Young']

attrs = [attr.lower() for attr in attrs]

for attr in attrs:
    print("Current attribute:", attr)
    os.system("mkdir checkpoints/celeba_z128_lrg_test_"+attr+"/")
    os.system("cp checkpoints/celeba_z128_lrg/last checkpoints/celeba_z128_lrg_test_"+attr+"/")
    os.system("python main.py --train False --dataset celeba_test --seed 1 --lr 1e-4 --beta1 0.9 --beta2 0.999 \
                           --objective H --model H --batch_size 1 --z_dim 128 --max_iter 2.5e6 \
                           --beta 1 --viz_name celeba_z128_lrg_test_"+attr+" --gather_step 5000 \
                           --display_step 50000 --save_step 50000 --viz_port 9097 \
                            --num_workers 10 --cuda False --attr "+attr+" | tee out_"+attr+".txt")




'''      3 mkdir checkpoints/celeba_z128_lrg_narrow_eyes/
        4 cp checkpoints/celeba_z128_lrg/last checkpoints/celeba_z128_lrg_test_narrow_eyes/
          5
             python main.py --train False --dataset celeba_test --seed 1 --lr 1e-4 --beta1 0.9 --beta2 0.999 \
                           --objective H --model H --batch_size 1 --z_dim 128 --max_iter 2.5e6 \
                           --beta 1 --viz_name celeba_z128_lrg_test_narrow_eyes --gather_step 5000 \
                           --display_step 50000 --save_step 50000 --viz_port 9097 \
                            --num_workers 10 --cuda False --attr narrow_eyes
'''
print("All Done!")
