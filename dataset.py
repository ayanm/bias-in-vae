"""dataset.py"""

import os
import numpy as np

import torch
from torch.utils.data import Dataset, DataLoader
from torchvision.datasets import ImageFolder
from torchvision import transforms
import pickle


#torch.cuda.set_device(7)
IS_AUDIT = True
dname = ""

def is_power_of_2(num):
    return ((num & (num - 1)) == 0) and num != 0


class CustomImageFolder(ImageFolder):
    def __init__(self, root, transform=None):
        super(CustomImageFolder, self).__init__(root, transform)

    def __getitem__(self, index):
        path = self.imgs[index][0]
        img = self.loader(path)
        if self.transform is not None:
            img = self.transform(img)

        return img

#class NewImageFolderAudit(ImageFolder):
#    def __init__(self, root, transform=None):
#        super(NewImageFolderAudit, self).__init__(root, transform)
#        with open('audit_dict_attrs.pickle', 'rb') as handle:
#            self.attr_dict = pickle.load(handle)

class NewImageFolderTrain(ImageFolder):
    def __init__(self, root, transform=None):
        super(NewImageFolderTrain, self).__init__(root, transform)
        with open('train_dict_attrs.pickle', 'rb') as handle:
            self.attr_dict = pickle.load(handle)

    def __getitem__(self, index):
        path = self.imgs[index][0]
        img = self.loader(path)
        if self.transform is not None:
            img = self.transform(img)
        if dname == "CelebA_M25_Train":
            path_rep = path.replace("CelebA_M25_Train", "CelebA")
        elif dname == "CelebA_M75_Train":
            path_rep = path.replace("CelebA_M75_Train", "CelebA")
        elif dname == "CelebA_M90_Train":
            path_rep = path.replace("CelebA_M90_Train", "CelebA")
        elif dname == "CelebA_M95_Train":
            path_rep = path.replace("CelebA_M95_Train", "CelebA")
        else:
            path_rep = path
        return img, index, self.attr_dict[path_rep]

class CustomImageFolderTest(ImageFolder):
    def __init__(self, root, transform=None):
        super(CustomImageFolderTest, self).__init__(root, transform)
        if not IS_AUDIT:
            with open('test_dict_attrs.pickle', 'rb') as handle:
                self.attr_dict = pickle.load(handle)
        else:
            with open('audit_dict_attrs.pickle', 'rb') as handle:
                self.attr_dict = pickle.load(handle)

    def __getitem__(self, index):
        path = self.imgs[index][0]
        img = self.loader(path)
        if self.transform is not None:
            img = self.transform(img)
        if 'small' in path and not IS_AUDIT:
            path = path.replace('_small','')
            return img, index, self.attr_dict[path]
        elif not IS_AUDIT:
            return img, index, self.attr_dict[path]
        else:
            return img, index, self.imgs[index][1], self.classes, self.attr_dict[path]


class CustomTensorDataset(Dataset):
    def __init__(self, data_tensor):
        self.data_tensor = data_tensor

    def __getitem__(self, index):
        return self.data_tensor[index]

    def __len__(self):
        return self.data_tensor.size(0)


def return_data(args):
    name = args.dataset
    dset_dir = args.dset_dir
    batch_size = args.batch_size
    num_workers = args.num_workers
    image_size = args.image_size
    assert image_size == 64, 'currently only image size of 64 is supported'

    print("Dataset Name:", name.lower())

    global IS_AUDIT
    global dname
    IS_AUDIT = args.is_audit

    if name.lower() == '3dchairs':
        root = os.path.join(dset_dir, '3DChairs')
        transform = transforms.Compose([
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),])
        train_kwargs = {'root':root, 'transform':transform}
        dset = CustomImageFolder

    elif name.lower() == 'utk':
        root = os.path.join(dset_dir, 'UTKFace')
        transform = transforms.Compose([
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),])
        train_kwargs = {'root':root, 'transform':transform}
        dset = CustomImageFolder

    elif name.lower() == 'celeba':
        root = os.path.join(dset_dir, 'CelebA')
        transform = transforms.Compose([
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),])
        train_kwargs = {'root':root, 'transform':transform}
        dname = "CelebA"
        #if args.condition:
        dset = NewImageFolderTrain
        #else:
        #    dset = CustomImageFolder

    elif name.lower() == 'celeba_audit' and args.train:
        root = os.path.join(dset_dir, 'CelebA_audit')
        transform = transforms.Compose([
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),])
        train_kwargs = {'root':root, 'transform':transform}
        dset = CustomImageFolder

    elif name.lower() == 'celeba_m25':
        # MODIFYING
        if args.train:
            root = os.path.join(dset_dir, 'CelebA_M25_Train')
        else:
            root = os.path.join(dset_dir, 'CelebA_M_25')
        transform = transforms.Compose([
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),])
        train_kwargs = {'root':root, 'transform':transform}
        dname = "CelebA_M25_Train"
        if args.train:
            dset = NewImageFolderTrain
        else:
            dset = CustomImageFolder

    elif name.lower() == 'celeba_m60':
        root = os.path.join(dset_dir, 'CelebA_M_60')
        transform = transforms.Compose([
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),])
        train_kwargs = {'root':root, 'transform':transform}
        dset = CustomImageFolder

    elif name.lower() == 'celeba_m50':
        root = os.path.join(dset_dir, 'CelebA_M_50')
        transform = transforms.Compose([
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),])
        train_kwargs = {'root':root, 'transform':transform}
        dset = CustomImageFolder

    elif name.lower() == 'celeba_m75':
        if args.train:
            root = os.path.join(dset_dir, 'CelebA_M75_Train')
        else:
            root = os.path.join(dset_dir, 'CelebA_M_75')
        transform = transforms.Compose([
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),])
        train_kwargs = {'root':root, 'transform':transform}
        dname = "CelebA_M75_Train"
        if args.train:
            dset = NewImageFolderTrain
        else:
            dset = CustomImageFolder

    elif name.lower() == 'celeba_m90':
        if args.train:
            root = os.path.join(dset_dir, 'CelebA_M90_Train')
        else:
            root = os.path.join(dset_dir, 'CelebA_M_90')
        transform = transforms.Compose([
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),])
        train_kwargs = {'root':root, 'transform':transform}
        dname = "CelebA_M90_Train"
        if args.train:
            dset = NewImageFolderTrain
        else:
            dset = CustomImageFolder

    elif name.lower() == 'celeba_m95':
        if args.train:
            root = os.path.join(dset_dir, 'CelebA_M95_Train')
        else:
            root = os.path.join(dset_dir, 'CelebA_M_95')
        transform = transforms.Compose([
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),])
        train_kwargs = {'root':root, 'transform':transform}
        dname = "CelebA_M95_Train"
        if args.train:
            dset = NewImageFolderTrain
        else:
            dset = CustomImageFolder


    elif name.lower() == 'celeba_test':
        root = os.path.join(dset_dir, 'CelebA_test')
        transform = transforms.Compose([
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),])
        train_kwargs = {'root':root, 'transform':transform}
        dset = CustomImageFolderTest

    elif name.lower() == "celeba_test_small":
        root = os.path.join(dset_dir, 'CelebA_test_small')
        transform = transforms.Compose([
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),])
        train_kwargs = {'root':root, 'transform':transform}
        dset = CustomImageFolderTest

    elif name.lower() == "celeba_audit" and not args.train:
        root = os.path.join(dset_dir, 'CelebA_audit')
        transform = transforms.Compose([
            transforms.Resize((image_size, image_size)),
            transforms.ToTensor(),])
        train_kwargs = {'root':root, 'transform':transform}
        dset = CustomImageFolderTest


    elif name.lower() == 'dsprites':
        root = os.path.join(dset_dir, 'dsprites-dataset/dsprites_ndarray_co1sh3sc6or40x32y32_64x64.npz')
        if not os.path.exists(root):
            import subprocess
            print('Now download dsprites-dataset')
            subprocess.call(['./download_dsprites.sh'])
            print('Finished')
        data = np.load(root, encoding='bytes')
        data = torch.from_numpy(data['imgs']).unsqueeze(1).float()
        train_kwargs = {'data_tensor':data}
        dset = CustomTensorDataset

    else:
        raise NotImplementedError

    if args.train == True:
        shuffle = True
    else:
        shuffle = False

    train_data = dset(**train_kwargs)
    train_loader = DataLoader(train_data,
                              batch_size=batch_size,
                              shuffle=shuffle,
                              num_workers=num_workers,
                              pin_memory=True,
                              drop_last=True)

    data_loader = train_loader

    return data_loader

if __name__ == '__main__':
    transform = transforms.Compose([
        transforms.Resize((64, 64)),
        transforms.ToTensor(),])

    dset = CustomImageFolder('data/CelebA', transform)
    loader = DataLoader(dset,
                       batch_size=32,
                       shuffle=True,
                       num_workers=1,
                       pin_memory=False,
                       drop_last=True)

    images1 = iter(loader).next()
    import ipdb; ipdb.set_trace()
