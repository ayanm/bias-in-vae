import os
import sys
import glob
from shutil import copy2

files = [f for f in glob.glob("*.jpg")]

white_m, white_f = [], []
black_m, black_f = [], []
asian_m, asian_f = [], []
indian_m, indian_f = [], []
other_m, other_f = [], []

try:
    os.mkdir("white_male")
    os.mkdir("white_female")
    os.mkdir("black_male")
    os.mkdir("black_female")
    os.mkdir("asian_male")
    os.mkdir("asian_female")
    os.mkdir("indian_male")
    os.mkdir("indian_female")
    os.mkdir("other_male")
    os.mkdir("other_female")
except OSError as e:
    print("Directory Exists")

for f in files:
    # 1_1_1_20170103210044250.jpg.chip.jpg
    attrs = f.split(".")[0].split("_")
    if attrs[1] == "0":
        if attrs[2] == "0":
            white_m.append(f)
        elif attrs[2] == "1":
            black_m.append(f)
        elif attrs[2] == "2":
            asian_m.append(f)
        elif attrs[2] == "3":
            indian_m.append(f)
        elif attrs[2] == "4":
            other_m.append(f)
    elif attrs[1] == "1":
        if attrs[2] == "0":
            white_f.append(f)
        elif attrs[2] == "1":
            black_f.append(f)
        elif attrs[2] == "2":
            asian_f.append(f)
        elif attrs[2] == "3":
            indian_f.append(f)
        elif attrs[2] == "4":
            other_f.append(f)

for f in white_m:
    copy2(f, "white_male")
for f in white_f:
    copy2(f, "white_female")
for f in black_m:
    copy2(f, "black_male")
for f in black_f:
    copy2(f, "black_female")
for f in asian_m:
    copy2(f, "asian_male")
for f in asian_f:
    copy2(f, "asian_female")
for f in indian_m:
    copy2(f, "indian_male")
for f in indian_f:
    copy2(f, "indian_female")
for f in other_m:
    copy2(f, "other_male")
for f in other_f:
    copy2(f, "other_female")

print("Done Moving Files")
print("----------")
print("STATS")
print("White Male:", len(white_m))
print("White Female:", len(white_f))
print("Black Male:", len(black_m))
print("Black Female:", len(black_f))
print("Asian Male:", len(asian_m))
print("Asian Female:", len(asian_f))
print("Indian Male:", len(indian_m))
print("Indian Female:", len(indian_f))
print("Other Male:", len(other_m))
print("Other Female:", len(other_f))
