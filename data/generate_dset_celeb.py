import os
import shutil
import glob
import pickle
import pathlib

with open('train_dict_attrs.pickle', 'rb') as f:
    train_dict = pickle.load(f)

M = 0
F = 0

for k in train_dict:
    vals = train_dict[k]
    if vals["male"] == "-1":
        F += 1
    else:
        M += 1
print(M, F)


#files_1 = [f for f in glob.glob("CelebA_M90_Train/train/*.jpg")]

#c1, c2 = 0, 0
#for f in files_1:
#    #print(f)
#    name = f.split("/")[-1]
#    k = "data/CelebA/train/"+name
#    if train_dict[k]["male"] == "-1":
#        c1 += 1
#    else:
#        c2 += 1

#print("Male:", c2, "Female:", c1)

#exit(0)
#male_path = "CelebA_M_25/male"
#female_path = "CelebA_M_25/female"
path = "CelebA_M95_Train/train"

#M = 68261
#M = 40692 # M:F is 1:3
#F = int((40692*75)/25)
#F = 94510
#M = int((F*25)/75)
M = 68261
F = int((M*5)/95)


pathlib.Path(path).mkdir(parents=True, exist_ok=True)
#pathlib.Path(female_path).mkdir(parents=True, exist_ok=True)


male_count = 0
female_count = 0

for img in train_dict:
    #print(img)
    #break
    try:
        if train_dict[img]["male"] == "1":
            if male_count >= M:
                continue
            male_count += 1
            i_path = "/".join(img.split("/")[1:])
            #os.system("cp "+i_path+" "+male_path)
            shutil.copy2(i_path, path)
        elif train_dict[img]["male"] == "-1":
            #female_count += 1
            if female_count >= F:
                continue
            female_count += 1
            i_path = "/".join(img.split("/")[1:])
            shutil.copy2(i_path, path)
    except Exception as e:
        print(i_path)
        print(e)
        continue


print(male_count)
print(female_count)
#print(os.system("ls "+male_path+" | wc -l"))

'''male_path = "CelebA_M_50/male"
female_path = "CelebA_M_50/female"

M = 68261
F = int((68261*50)/50)

pathlib.Path(male_path).mkdir(parents=True, exist_ok=True)
pathlib.Path(female_path).mkdir(parents=True, exist_ok=True)

male_count = 0
female_count = 0

for img in train_dict:
    #print(img)
    #break
    if train_dict[img]["male"] == "1":
        male_count += 1
        i_path = "/".join(img.split("/")[1:])
        #os.system("cp "+i_path+" "+male_path)
        shutil.copy2(i_path, male_path)
    elif train_dict[img]["male"] == "-1":
        #female_count += 1
        if female_count >= F:
            continue
        female_count += 1
        i_path = "/".join(img.split("/")[1:])
        shutil.copy2(i_path, female_path)


print(male_count)
print(female_count)'''
#print(os.system("ls "+male_path+" | wc -l"))
