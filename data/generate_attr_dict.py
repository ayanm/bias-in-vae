#!/usr/bin/python3

import matplotlib.pyplot as plt
import random
from PIL import Image
import glob
import os

# Read the file and all the lines first.
with open('list_attr_celeba.txt') as fh:
    lines = fh.readlines()

# Now we go line by line
num_files = int(lines[0])
print("Number of image files:", num_files)

attributes = lines[1].split()
print("We have these attributes:\n", attributes, len(attributes))

#ATTRIBUTES_OF_INTEREST = {"Male":None, "Pale_Skin":None, "Young":None}

attributes = [a.lower() for a in attributes]

male_count, female_count = 0, 0
young_count, old_count = 0, 0

# Sanity Check
#for attr in ATTRIBUTES_OF_INTEREST:
#    assert attr in attributes
#    ATTRIBUTES_OF_INTEREST[attr] = attributes.index(attr)

# Now go line by line
#male_female_dict = {"male":[], "female":[]}
#skin_dict = {"pale":[], "dark":[]}
#age_dict = {"young":[], "old":[]}
#TEST_ATTR = "Male"

test_dir = 'CelebA_audit/'
test_imgs = glob.glob(test_dir+"/*/*.jpg")
dict_imgs = {}

print("Num images in folder:", len(test_imgs))
#exit(0)

for img in test_imgs:
    img_name = img.split("/")[-1]
    for line in lines[162000:]:
        #print(line.split())
        #break
        data = line.split()
        img_file = data[0]
        #print(img_file)
        #for t_i in test_imgs:
        #test = os.path.basename(t_i)
        #print(t_i)
        #print(test)
        if img_name == img_file:
            #t_i = os.path.join(test_dir,img_file)
            t_i = img
            #if img_file == test:
            dict_imgs[os.path.join("data",t_i)] = {}
            for j in range(1, len(data)):
                dict_imgs[os.path.join("data",t_i)][attributes[j-1]] = data[j]
                if attributes[j-1] == "male":
                    if data[j] == "1":
                        male_count += 1
                    else:
                        female_count += 1
                if attributes[j-1] == "young":
                    if data[j] == "1":
                        young_count += 1
                    else:
                        old_count += 1
        #print(dict_imgs[os.path.join("data",t_i)])

#print(dict_imgs)
for key in dict_imgs:
    print(key,dict_imgs[key])
    break

print("MALE:", male_count, "FEMALE:", female_count)
print("YOUNG:", young_count, "OLD:", old_count)

import pickle

with open('audit_dict_attrs.pickle', 'wb') as handle:
    pickle.dump(dict_imgs, handle, protocol=pickle.HIGHEST_PROTOCOL)
