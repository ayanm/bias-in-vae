#!/usr/bin/python3

import matplotlib.pyplot as plt
import random
from PIL import Image
import glob

# Read the file and all the lines first.
with open('list_attr_celeba.txt') as fh:
    lines = fh.readlines()

# Now we go line by line
num_files = int(lines[0])
print("Number of image files:", num_files)

attributes = lines[1].split()
print("We have these attributes:\n", attributes, len(attributes))

ATTRIBUTES_OF_INTEREST = {"Male":None, "Pale_Skin":None, "Young":None}

# Sanity Check
for attr in ATTRIBUTES_OF_INTEREST:
    assert attr in attributes
    ATTRIBUTES_OF_INTEREST[attr] = attributes.index(attr)

# Now go line by line
male_female_dict = {"male":[], "female":[]}
skin_dict = {"pale":[], "dark":[]}
age_dict = {"young":[], "old":[]}
#TEST_ATTR = "Male"

for line in lines[2:]:
    #print(line.split())
    #break
    data = line.split()
    img_file = data[0]
    for attr in ATTRIBUTES_OF_INTEREST:
        if data[ATTRIBUTES_OF_INTEREST[attr]+1] == "1":
            if attr == "Male":
                male_female_dict["male"].append(img_file)
            elif attr == "Pale_Skin":
                skin_dict["pale"].append(img_file)
            elif attr == "Young":
                age_dict["young"].append(img_file)
        else:
            if attr == "Male":
                male_female_dict["female"].append(img_file)
            elif attr == "Pale_Skin":
                skin_dict["dark"].append(img_file)
            elif attr == "Young":
                age_dict["old"].append(img_file)


train_path = "train/"
val_path = "val/"
test_path = "test/"

train_files = [f for f in glob.glob(train_path + "*.jpg")]
val_files = [f for f in glob.glob(val_path + "*.jpg")]
test_files = [f for f in glob.glob(test_path + "*.jpg")]

# Train NUM
count1 = 0
count2 = 0
for f in train_files:
    if f in male_female_dict["male"]:
        count1 += 1
    elif f in male_female_dict["female"]:
        count2 += 1
print("TRAIN MALE:", count1, "FEMALE:", count2)

count1 = 0
count2 = 0
for f in val_files:
    if f in male_female_dict["male"]:
        count1 += 1
    elif f in male_female_dict["female"]:
        count2 += 1
print("VAL MALE:", count1, "FEMALE:", count2)

count1 = 0
count2 = 0
for f in test_files:
    if f in male_female_dict["male"]:
        count1 += 1
    elif f in male_female_dict["female"]:
        count2 += 1
print("TEST MALE:", count1, "FEMALE:", count2)

'''print("MALE:", len(male_female_dict["male"]), "FEMALE:", len(male_female_dict["female"]))
print("PALE:", len(skin_dict["pale"]), "DARK:", len(skin_dict["dark"]))
print("YOUNG:", len(age_dict["young"]), "OLD:", len(age_dict["old"]))

print(skin_dict["pale"][:5])

#random_pale = random.choices(skin_dict["pale"], 25)
#random_dark = random.choices(skin_dict["dark"], 25)

random_pale = male_female_dict["male"][:25]
random_dark = male_female_dict["female"][:25]

fig = plt.figure()
n=5
for i in range(1,26):
    ax = fig.add_subplot(n,n,i)
    try:
        image = Image.open("train/"+random_pale[i-1])
    except:
        try:
            image = Image.open("val/"+random_pale[i-1])
        except:
            image = Image.open("test/"+random_pale[i-1])

    ax.imshow(image)
plt.savefig("pale.png")

fig = plt.figure()
for i in range(1,26):
    ax = fig.add_subplot(n,n,i)
    try:
        image = Image.open("train/"+random_dark[i-1])
    except:
        try:
            image = Image.open("val/"+random_dark[i-1])
        except:
            image = Image.open("test/"+random_dark[i-1])

    ax.imshow(image)
plt.savefig("dark.png")'''
print("DONE")
