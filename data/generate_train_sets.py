import os
import shutil
import glob
import pickle
import pathlib

with open('train_dict_attrs.pickle', 'rb') as f:
    train_dict = pickle.load(f)

male_path = "CelebA_M_25/male"
female_path = "CelebA_M_25/female"

#M = 68261
M = 31503 # M:F is 1:3
F = int((68261*75)/25)

pathlib.Path(male_path).mkdir(parents=True, exist_ok=True)
pathlib.Path(female_path).mkdir(parents=True, exist_ok=True)

male_count = 0
female_count = 0

for img in train_dict:
    #print(img)
    #break
    if train_dict[img]["male"] == "1":
        if male_count >= M:
            continue
        male_count += 1
        i_path = "/".join(img.split("/")[1:])
        #os.system("cp "+i_path+" "+male_path)
        shutil.copy2(i_path, male_path)
    elif train_dict[img]["male"] == "-1":
        #female_count += 1
        if female_count >= F:
            continue
        female_count += 1
        i_path = "/".join(img.split("/")[1:])
        shutil.copy2(i_path, female_path)


print(male_count)
print(female_count)
#print(os.system("ls "+male_path+" | wc -l"))

'''male_path = "CelebA_M_50/male"
female_path = "CelebA_M_50/female"

M = 68261
F = int((68261*50)/50)

pathlib.Path(male_path).mkdir(parents=True, exist_ok=True)
pathlib.Path(female_path).mkdir(parents=True, exist_ok=True)

male_count = 0
female_count = 0

for img in train_dict:
    #print(img)
    #break
    if train_dict[img]["male"] == "1":
        male_count += 1
        i_path = "/".join(img.split("/")[1:])
        #os.system("cp "+i_path+" "+male_path)
        shutil.copy2(i_path, male_path)
    elif train_dict[img]["male"] == "-1":
        #female_count += 1
        if female_count >= F:
            continue
        female_count += 1
        i_path = "/".join(img.split("/")[1:])
        shutil.copy2(i_path, female_path)


print(male_count)
print(female_count)'''
#print(os.system("ls "+male_path+" | wc -l"))
