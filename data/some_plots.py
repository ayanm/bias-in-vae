import seaborn as sns
import matplotlib.pyplot as plt

stats_race_gender = [5477, 4601, 2318, 2208, 1575, 1859, 2261, 1714, 760, 932]
labels_race_gender = ["White_Male", "White_Female", "Black_Male", "Black_Female", "Asian_Male", "Asian_Female", "Indian_Male", "Indian_Female", "Other_Male", "Other_Female"]

#plt.pie(stats_race_gender, labels=labels_race_gender, autopct='%1.1f%%')
plt.figure(figsize=(15,18))
patches, text, _ = plt.pie(stats_race_gender, autopct='%1.1f%%')
plt.legend(patches, labels_race_gender, loc="best")
plt.axis('equal')
plt.tight_layout()
plt.title("Race and Gender Stats for UTKFace Dataset")
plt.show()
plt.clf()
stats_gender = [12391, 11314]
labels_gender = ["Male", "Female"]

patches, text, _ = plt.pie(stats_gender, autopct='%1.1f%%')
plt.legend(patches, labels_gender, loc="best")
plt.axis('equal')
plt.tight_layout()
plt.title("Gender Stats for UTKFace Dataset")
plt.show()

stats_gender = [12391, 11314]
labels_gender = ["Male", "Female"]

patches, text, _ = plt.pie(stats_gender, autopct='%1.1f%%')
plt.legend(patches, labels_gender, loc="best")
plt.axis('equal')
plt.tight_layout()
plt.title("Gender Stats for UTKFace Dataset")
plt.show()
