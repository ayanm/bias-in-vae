import visdom
import numpy as np
import matplotlib.pyplot as plt
vis = visdom.Visdom()
vis.text('Hello, beautiful world!')
vis.image(np.ones((3, 100, 100)))


trace = dict(x=[1, 2, 3], y=[4, 5, 6], mode="markers+lines", type='custom',
                     marker={'color': 'red', 'symbol': 104, 'size': "10"},
                                  text=["one", "two", "three"], name='1st Trace')
layout = dict(title="First Plot", xaxis={'title': 'x1'}, yaxis={'title': 'x2'})

vis._send({'data': [trace], 'layout': layout, 'win': 'mywin'})
