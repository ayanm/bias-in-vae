"""solver.py"""

import warnings
warnings.filterwarnings("ignore")

import os
from tqdm import tqdm
import visdom

import torch
import torch.optim as optim
import torch.nn.functional as F
from torch.autograd import Variable
from torchvision.utils import make_grid, save_image

from utils import cuda, grid2gif
from model import BetaVAE_H, BetaVAE_B
from dataset import return_data
import numpy as np
import matplotlib.pyplot as plt
import math
import heapq
from scipy.stats import multivariate_normal



#from torch.distributions import constraints
#from torch.distributions.distribution import Distribution
#from torch.distributions.utils import lazy_property

from torch._C import default_generator
from torch import _C
from torch.cuda import _lazy_init, _lazy_call, device_count, device as device_ctx_manager

import torch.distributions as dist
#from dist.multivariate_normal import MultivariateNormal

import pandas as pd
import statsmodels.api as sm
from clarifai.rest import ClarifaiApp
import glob
import json


# Ignore warnings
import warnings
warnings.filterwarnings("ignore")

#API_KEY = ''
API_KEY = 'bb8e7a2b54b641b7aedb9bf71dc25806'
TRY_EXPERIMENTAL = False # NOT WORKING CODE if True



def api_get_attr_dims(path, list_dims=[]):
    '''
    Use the Clarifai API to get predictions for random face generations.
    Then use Statistical Model to score the dimensions based on a attr.
    like gender.
    '''
    print("****PATH*****: ", path)
    files = [f for f in glob.glob(os.path.join(path, "*.png"))]
    jsons = [f for f in glob.glob(os.path.join(path, "*.json"))]

    if len(files) == 0 or (len(files) != 0 and len(files) != len(jsons)):

        app = ClarifaiApp(api_key=API_KEY)
        model = app.models.get('demographics')
        count = 0
        try:
            # File name format:
            # outputs/ayan_celeba_B_gamma200_z32_Cstop_1e5_Cmax_15/step_200000/random_traverse/random_step_200000_z_10_dim_3_val_-3.0.json
            for f in files:
                name = f.split("/")[-1]
                img_name = name.split(".png")[0]
                if not img_name+'_out.json' in jsons:
                    count += 1
                    response = model.predict_by_filename(f)
                    with open(os.path.join(path, img_name+"_out.json"), "w") as fw:
                        json.dump(response, fw)
        except Exception as e:
            # Will get here if we face an API error
            print(e)
            exit(1)

        print("Done calling the API... Processed", count, "calls to the API.")
    else:
        print("We already have the JSON responses from the API")

    # Now get all JSONs
    jsons = [f for f in glob.glob(os.path.join(path, "*.json"))]
    df = pd.DataFrame(columns=["img_id", "z_dim", "interpolation_value", \
            "predicted_gender", "feminine_prediction_confidence"])
    #print("JSONS:", jsons)
    miss = 0
    for f in jsons:
        name = f.split("/")[-1]
        json_name = name.split(".json")[0]
        # Split the name based on "_" to get individual components from the filename
        split_id = json_name.split("_")
        img_id = int(split_id[split_id.index("z") + 1])
        z_dim = int(split_id[split_id.index("dim") + 1])
        #if z_dim not in list_dims:
        interpol_val = float(split_id[split_id.index("val") + 1])
        with open(f) as fr:
            try:
                js = json.loads(fr.read().replace("\'", "\""))
                gender = js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['name']
                #gender_count[gender] += 1
                gender_pred_score = float(js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['value'])
                if gender == 'masculine':
                    gender_pred_score = 1.0 - gender_pred_score
                df.loc[len(df)] = [img_id, z_dim, interpol_val, gender, round(gender_pred_score, 3)]
            except:
                # We will enter this only when the API fails to detect a face.
                #print("ERROR")
                miss += 1
                continue
    print("MISSED FACES BY API:", miss)
    # Separate out male and female based on the non-interpolated random images.
    # TODO: See if we actually need to separate out male and female.
    #list_male = list(df_orig[df_orig.predicted_gender=='masculine'].img_id)
    #df_f = df[~df.img_id.str.contains('|'.join(list_male))]
    #df_m = df[df.img_id.str.contains('|'.join(list_male))]

    # Later if we have to do this during the training process, we will not know at first
    # WHICH dimensions actually would learn variations.
    # TODO: Maybe we can, by sending the dimensions that have KL Divergence > some non-zero value.
    # Dimensions that learn nothing actually have KLD near to zero. So maybe some value like 0.1 (??)
    #list_dims = np.array(list_dims)

    results = []
    coefs = []
    #print("DF")
    #print(df)
    list_dims = np.array(list_dims)
    #print(list_dims)

    for dim in list_dims:
        stat_df = df[df.z_dim == dim]#.groupby(['interpolation_value']).median().reset_index()
        if len(stat_df) == 0:
            continue
        #print(stat_df)
        # Fit a linear model
        lin_model = sm.OLS(stat_df['feminine_prediction_confidence'].astype(float),stat_df['interpolation_value'].astype(float))
        result = lin_model.fit(disp=0)
        results.append(result)
        coefs.append(abs(result.params[0]))

    coefs = np.array(coefs)
    sort_idx = np.argsort(coefs)[::-1]
    #print("The dimensions with statistically significant slope (ordered)")
    print("The dimensions with the scores (ordered)")
    print("*****************************************")
    for idx in sort_idx:
        #if results[idx].pvalues[0] <= 0.05:
        print("Dim:",list_dims[idx], "Score:", results[idx].params[0])
        #print("------------------------------")
        #print(results[idx].params[0])

    # Return the sorted dim numbers and the corresponding scores
    return list_dims[sort_idx], coefs[sort_idx]


def seed():
    """Sets the seed for generating random numbers to a non-deterministic
    random number. Returns a 64 bit number used to seed the RNG.
    """
    seed = default_generator.seed()
    #import torch.cuda

    #if not torch.cuda._in_bad_fork:
    #    torch.cuda.manual_seed_all(int(seed))
    return seed

def cuda_seed():
    """Sets the seed for generating random numbers to a random number for the current GPU.
    It's safe to call this function if CUDA is not available; in that
    case, it is silently ignored.

    .. warning::
        If you are working with a multi-GPU model, this function will only initialize
        the seed on one GPU.  To initialize all GPUs, use :func:`seed_all`.
    """
    _lazy_call(lambda: _C._cuda_seed())

PRINT_ALL = False
SIMPLE_TEST = False
IS_AUDIT = True
save_random = True
test_ayan = False
test_traverse = False
#torch.cuda.set_device(7)

step_C_distr = 6000
max_C_dims = 15

def reconstruction_loss(x, x_recon, distribution):
    batch_size = x.size(0)
    assert batch_size != 0

    if distribution == 'bernoulli':
        recon_loss = F.binary_cross_entropy_with_logits(x_recon, x, size_average=False).div(batch_size)
    elif distribution == 'gaussian':
        #x_recon = torch.sigmoid(x_recon)
        x_recon = torch.sigmoid(x_recon)
        recon_loss = F.mse_loss(x_recon, x, size_average=False).div(batch_size)
    else:
        recon_loss = None

    return recon_loss


def kl_divergence(mu, logvar):
    batch_size = mu.size(0)
    assert batch_size != 0
    if mu.data.ndimension() == 4:
        mu = mu.view(mu.size(0), mu.size(1))
    if logvar.data.ndimension() == 4:
        logvar = logvar.view(logvar.size(0), logvar.size(1))

    klds = -0.5*(1 + logvar - mu.pow(2) - logvar.exp())
    total_kld = klds.sum(1).mean(0, True)
    dimension_wise_kld = klds.mean(0)
    mean_kld = klds.mean(1).mean(0, True)

    return total_kld, dimension_wise_kld, mean_kld

def find_prob(mu):
    # Here we find the probability of a particular latent variable
    # coming from a standard Normal N(0,I). We just use the mu value
    # we get. This is because the latent distribution is supposed
    # to be N(mu, var), and expectation of this is simply mu.

    probs = multivariate_normal.pdf(mu.data.cpu().numpy().transpose())
    #mean_probs_dims = np.mean(probs)

    #prob = multivariate_normal.pdf(mu.data.numpy().transpose(), mean=np.zeros(128), cov=np.eye(128))

    mean_probs_dims = np.sum(np.log(probs))
    return probs, mean_probs_dims

class DataGather(object):
    def __init__(self):
        self.data = self.get_empty_data_dict()

    def get_empty_data_dict(self):
        return dict(iter=[],
                    recon_loss=[],
                    total_kld=[],
                    dim_wise_kld=[],
                    mean_kld=[],
                    mu=[],
                    var=[],
                    C_val=[],
                    images=[],
                    recons=[],
                    images1=[],
                    recons1=[],
                    total_kld1=[],
                    mean_kld1=[],
                    recon_loss1=[],
                    prob=[],
                    prob1=[],
                    total_var=[],
                    total_var1=[],
                    labels=[])

    def insert(self, **kwargs):
        for key in kwargs:
            self.data[key].extend(kwargs[key])

    def insert_bck(self, **kwargs):
        for key in kwargs:
            self.data[key].append(kwargs[key])

    def flush(self):
        self.data = self.get_empty_data_dict()

class DataGatherAudit(object):
    def __init__(self, num_classes):
        self.num_classes = num_classes
        self.data = self.get_empty_data_dict(num_classes)

    def get_empty_data_dict(self, num_classes):
        return dict(iter=[],
                    recon_loss=[[] for _ in range(num_classes)],
                    total_kld=[[] for _ in range(num_classes)],
                    mean_kld=[[] for _ in range(num_classes)],
                    prob=[[] for _ in range(num_classes)],
                    images=[[] for _ in range(num_classes)],
                    recons=[[] for _ in range(num_classes)],
                    classes=[])

    def insert(self, **kwargs):
        for key in kwargs:
            #self.data[key].extend(kwargs[key])
            if key=="label":
                continue
            if key=="classes":
                self.data[key] = kwargs[key]
            else:
                #print(kwargs["label"])
                #print(self.data[key][kwargs["label"]])
                #print(kwargs[key])
                self.data[key][kwargs["label"]].extend(kwargs[key])

    def insert_bck(self, **kwargs):
        for key in kwargs:
            if key=="classes":
                self.data[key] = kwargs[key]
            else:
                self.data[key].append(kwargs[key])

    def flush(self):
        self.data = self.get_empty_data_dict(self.num_classes)

class Solver(object):
    def __init__(self, args):

        self.cum_prob_0, self.cum_prob_1 = 0, 0
        self.use_cuda = args.use_cuda and torch.cuda.is_available()
        self.max_iter = args.max_iter
        self.global_iter = 0

        self.z_dim = args.z_dim
        self.beta = args.beta
        self.gamma = args.gamma
        self.C_max = args.C_max
        self.C_stop_iter = args.C_stop_iter
        self.objective = args.objective
        self.model = args.model
        self.lr = args.lr
        self.beta1 = args.beta1
        self.beta2 = args.beta2
        self.seed = args.seed
        #self.C_dim_weights = [1/15 for _ in range(15)] + [0 for _ in range(self.z_dim - 15)] #[1/15]*15 + [0]*(self.z_dim - 15)
        # Start C_dim_weights with only one dimension getting all of it. Then for every K steps add another dimension
        # to share this capacity.
        self.C_dim_weights = [1.0] + [0.0 for _ in range(self.z_dim - 1)]

        global PRINT_ALL
        global SIMPLE_TEST
        global save_random
        if args.only_random:
            save_random = True
            self.only_random = True
        else:
            save_random = False
            self.only_random = False
        if args.demo_test:
            self.demo_test = True
        else:
            self.demo_test = False

        if args.is_audit:
            self.is_audit = True
        else:
            self.is_audit = False

        # If only_random is not set but demo test is set and is in audit then we save all audit images and their recons in folder.
        # This would allow us to later pass these on to API and get required outputs.
        self.condition = args.condition
        self.enc = args.enc

        if args.dataset.lower() == 'celeba_test_small':
            if not args.is_audit:
                PRINT_ALL = True
            SIMPLE_TEST = True

        if args.dataset.lower() == 'celeba_audit' and not args.train:
            SIMPLE_TEST = True
            PRINT_ALL = True

        if args.dataset.lower() == 'dsprites':
            #self.nc = 1
            self.nc = 3
            self.decoder_dist = 'bernoulli'
        elif args.dataset.lower() == '3dchairs':
            self.nc = 3
            self.decoder_dist = 'gaussian'
        elif args.dataset.lower() == 'celeba':
            self.nc = 3
            self.decoder_dist = 'gaussian'
        elif "celeb" in args.dataset.lower():
            self.nc = 3
            self.decoder_dist = 'gaussian'
        elif "utk" in args.dataset.lower():
            self.nc = 3
            self.decoder_dist = 'gaussian'
        else:
            raise NotImplementedError

        self.attr = args.attr

        if not self.condition:
            self.attr_size = 0
        else:
            self.attr_size = len(self.attr)

        if args.model == 'H':
            net = BetaVAE_H
        elif args.model == 'H_mod':
            net = BetaVAE_H
        elif args.model == 'B':
            net = BetaVAE_B
        elif args.model == 'B_mod':
            net = BetaVAE_B
        else:
            raise NotImplementedError('only support model H or B')

        self.net = cuda(net(self.z_dim, self.nc, self.use_cuda, self.condition, self.enc, self.attr_size), self.use_cuda)
        self.optim = optim.Adam(self.net.parameters(), lr=self.lr,
                                    betas=(self.beta1, self.beta2))

        self.viz_name = args.viz_name
        self.viz_port = args.viz_port
        self.viz_on = args.viz_on
        self.win_recon = None
        self.win_kld = None
        self.win_mu = None
        self.win_var = None
        if self.viz_on:
            self.viz = visdom.Visdom(port=self.viz_port)

        self.ckpt_dir = os.path.join(args.ckpt_dir, args.viz_name)
        if not os.path.exists(self.ckpt_dir):
            os.makedirs(self.ckpt_dir, exist_ok=True)
        self.ckpt_name = args.ckpt_name
        # AYAN
        self.dim_klds = None
        self.last_gen_step = None
        #self.modify = args.modify
        self.sorted_dims, self.dim_scores = None, None
        self.sum_selected_kld = 0
        self.dim_kld_for_modify = None

        self.select_top_dims = 4
        self.do_modify = args.do_modify
        self.modify = False
        #self.modify_step = [500001+1, 900000+1]
        #self.modify_step = [700001+1]
        self.api_step = args.modify_step
        self.modify_step = [args.modify_step + 1]
        #self.modify_increase_step = 20000
        self.modify_increase_step = args.modify_increase_step
        #self.modify_type = "i"
        self.modify_type = args.modify_type
        self.modify_split = "2"

        self.set_audit_label = args.audit_label

        if self.ckpt_name is not None:
            self.load_checkpoint(self.ckpt_name)

        self.save_output = args.save_output
        self.output_dir = os.path.join(args.output_dir, args.viz_name)
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir, exist_ok=True)

        self.gather_step = args.gather_step
        self.display_step = args.display_step
        self.save_step = args.save_step

        self.dset_dir = args.dset_dir
        self.dataset = args.dataset
        self.batch_size = args.batch_size
        #if args.dataset.lower() == 'celeba':

        #else:
        #    self.attr = None
        self.data_loader = return_data(args)


        if args.is_audit:
            self.gather = DataGatherAudit(args.audit_classes)
            self.classes = args.audit_classes
        else:
            self.gather = DataGather()

    def train(self):
        self.net_mode(train=True)
        self.C_max = Variable(cuda(torch.FloatTensor([self.C_max]), self.use_cuda))

        self.C_dim_weights = Variable(cuda(torch.FloatTensor(self.C_dim_weights), self.use_cuda))

        out = False

        C_mult = Variable(cuda(torch.FloatTensor([5]), self.use_cuda))

        print("DEBUG: self.viz_on:", self.viz_on)
        print("DEBUG: do_modify:", self.do_modify)

        pbar = tqdm(total=self.max_iter)
        pbar.update(self.global_iter)
        count_dims_C = 1

        #mv_norm = dist.Normal(0.0, 1.0)
        mv_norm = dist.multivariate_normal.MultivariateNormal(cuda(torch.zeros(self.z_dim), self.use_cuda), cuda(torch.eye(self.z_dim), self.use_cuda))

        if self.condition:
            while not out:
                for x, idx, label in self.data_loader:
                    # This is only for CelebA for now
                    self.global_iter += 1
                    pbar.update(1)

                    x = Variable(cuda(x, self.use_cuda))

                    # CHANGE THIS... SEND CLASS INFO
                    #attr = attr_dict[self.attr]

                    # Binary label... Make it 0-1.
                    #label = label[self.attr]
                    label = list(map(label.__getitem__, self.attr))
                    #label = [[float(label[0][i]), float(label[1][i])] for i in range(len(label[0]))]

                    # Reshape the labels to feed into the N/W.
                    label = [[float(lst[i]) for lst in label] for i in range(len(label[0]))]
                    #print(label)
                    #for i in range(len(label)):
                        # Clipping value
                    #    label[i] = max(min(float(label[i]), 1.0), -1.0)
                    #label = Variable(cuda(torch.clamp(torch.FloatTensor(label), 0, 1), self.use_cuda))

                    x_recon, mu, logvar, _ = self.net(x, label)

                    recon_loss = reconstruction_loss(x, x_recon, self.decoder_dist)
                    total_kld, dim_wise_kld, mean_kld = kl_divergence(mu, logvar)
                    if self.objective == 'H':
                        beta_vae_loss = recon_loss + self.beta*total_kld
                    elif self.objective == 'B':
                        C = torch.clamp(self.C_max/self.C_stop_iter*self.global_iter, 0, self.C_max.data[0])
                        beta_vae_loss = recon_loss + self.gamma*(total_kld-C).abs()

                    # Optimize
                    self.optim.zero_grad()
                    beta_vae_loss.backward(retain_graph=True)
                    self.optim.step()

                    # AYAN
                    self.dim_klds = dim_wise_kld.data.cpu().numpy()

                    if self.viz_on and self.global_iter%self.gather_step == 0:
                        self.gather.insert_bck(iter=self.global_iter,
                                           mu=mu.mean(0).data, var=logvar.exp().mean(0).data,
                                           recon_loss=recon_loss.data, total_kld=total_kld.data,
                                           dim_wise_kld=dim_wise_kld.data, mean_kld=mean_kld.data)

                    if self.global_iter%self.display_step == 0:
                        pbar.write('[{}] recon_loss:{:.3f} total_kld:{:.3f} mean_kld:{:.3f}'.format(
                            self.global_iter, recon_loss.item(), total_kld.item(), mean_kld.item()))

                        #var = logvar.exp().mean(0).data
                        var = dim_wise_kld.data
                        var_str = ''
                        for j, var_j in enumerate(var):
                            var_str += 'kld{}:{:.4f} '.format(j+1, var_j)
                        pbar.write(var_str)

                        if self.objective == 'B':
                            pbar.write('C:{:.3f}'.format(C.item()))

                        if self.viz_on:
                            self.gather.insert_bck(images=x.data)
                            self.gather.insert_bck(labels=label)
                            self.gather.insert_bck(images=torch.sigmoid(x_recon).data)
                            self.viz_reconstruction()
                            self.viz_lines()
                            self.gather.flush()

                        if self.beta >= 1 and (self.viz_on or self.save_output):
                            # TURINING OFF FOR NOW!
                            sorted_z_idx = np.argsort(self.dim_klds)[::-1]
                            if self.objective == 'B':
                                if self.global_iter <= self.C_stop_iter:
                                    self.viz_traverse(C=C.data)
                                    pass
                                else:
                                    if self.global_iter % (self.display_step*10) == 0:
                                        self.viz_traverse(C=C.data)
                            else:
                                if self.condition:
                                    if self.global_iter <= 1e5:
                                        self.viz_traverse(z_order=sorted_z_idx)
                                    else:
                                        if self.global_iter % (self.display_step*10) == 0:
                                            self.viz_traverse(z_order=sorted_z_idx)
                                else:
                                    self.viz_traverse()

                    steps_rand = [7e5+1, 1.5e6, 2e6]

                    # CHANGE HERE

                    if self.global_iter in steps_rand:
                        self.random_img_gen(num_random=500)
                        path = os.path.join(self.output_dir,'step_'+str(self.global_iter),'random_traverse')
                        files = [f for f in glob.glob(os.path.join(path, "*.png"))]
                        jsons = [f for f in glob.glob(os.path.join(path, "*.json"))]
                        if len(files) == 0 or (len(files) != 0 and len(files) != len(jsons)):
                            self.last_gen_step = self.global_iter
                            if self.beta > 1:
                                self.random_img_traverse(num_random=5)
                        else:
                            print("Already have processed JSONs. Ignoring random generations for traversal. Number of JSONs:", len(jsons))

                    if self.global_iter%self.save_step == 0:
                        self.save_checkpoint('last')
                        pbar.write('Saved checkpoint(iter:{})'.format(self.global_iter))

                    if self.global_iter%50000 == 0:
                        self.save_checkpoint(str(self.global_iter))

                    if self.global_iter >= self.max_iter:
                        out = True
                        print("DONE TRAINING!!!")
                        break

            pbar.write("[Training Finished]")
            pbar.close()

        # Else for condition
        else:
            #cum_prob_0 = 0
            #cum_prob_1 = 0
            while not out:
                for x, idx, label in self.data_loader:
                    if self.dataset == "dsprites":
                        x_org = x
                        x = x.repeat(1, 3, 1, 1)
                        #fixed_idx1 = 87040 # square
                        #fixed_img1 = self.data_loader.dataset.__getitem__(fixed_idx1)
                        #fixed_img1 = Variable(cuda(fixed_img1, self.use_cuda), volatile=True).unsqueeze(0)
                        #color = np.repeat(np.repeat(np.random.uniform(0.5,1,[1,3,1,1]),\
                        #    fixed_img1.shape[2], axis=2),\
                        #    fixed_img1.shape[3], axis=3)
                        #color = Variable(cuda(torch.from_numpy(color).float(), self.use_cuda))
                        #color_img = fixed_img1 * color

                        #print(color_img.shape)
                        #exit(1)
                    #print(x.shape)
                    #exit(1)
                    self.global_iter += 1
                    pbar.update(1)

                    x = Variable(cuda(x, self.use_cuda))
                    x_recon, mu, logvar, _ = self.net(x)
                    recon_loss = reconstruction_loss(x, x_recon, self.decoder_dist)
                    total_kld, dim_wise_kld, mean_kld = kl_divergence(mu, logvar)

                    # AYAN: TRY 1
                    '''if self.global_iter % step_C_distr == 0:
                        # Time to update our distribution of C values.
                        # The number of dimensions we use is capped to a value. Usually we never need all dimensions.
                        count_dims_C = min(count_dims_C + 1, max_C_dims)
                        for i in range(count_dims_C):
                            self.C_dim_weights[i] = 1/count_dims_C
                    '''
                    if self.objective == 'H':
                        beta_vae_loss = recon_loss + self.beta*total_kld

                    elif self.objective == 'H_mod':
                        # Binary label... Make it 0-1.
                        label = label[self.attr]
                        probs_0, probs_1 = [], []
                        mu_0, mu_1 = [], []
                        #mask_0 = (label == '-1').nonzero().squeeze()
                        #mask_1 = (label == '1').nonzero().squeeze()
                        #mu_0 = torch.masked_select(mu, mask_0)
                        #mu_1 = torch.masked_select(mu, mask_1)
                        #print(type(mu[0,:]))
                        N = 0
                        for i in range(len(label)):
                            # Clipping value
                            label[i] = max(min(float(label[i]), 1.0), -1.0)
                            p = 0
                            #print(mu[i,:])
                            p += mv_norm.log_prob(mu[i,:])
                            #for j in range(self.z_dim):
                            #    p += mv_norm.log_prob(mu[i,j])
                            ##p = torch.exp(p)
                            ##N += p
                            if label[i] == -1:
                                probs_0.append(p)
                            else:
                                probs_1.append(p)
                            #probs, mean_prob = find_prob(mu[i,:])
                            #if label[i] == -1:
                            #    probs_0.append(mean_prob)
                            #else:
                            #    probs_1.append(mean_prob)
                        #print(probs_0)
                        #exit(0)
                        #probs_0 = np.exp(probs_0)
                        #probs_1 = np.exp(probs_1)
                        probs_0 = torch.stack(probs_0, dim=0)
                        probs_1 = torch.stack(probs_1, dim=0)

                        #N = torch.sum([torch.sum(probs_0), torch.sum(probs_1)])
                        #print("N:", N)
                        #exit(0)

                        ##probs_0 = probs_0 / N
                        ##probs_1 = probs_1 / N
                        probs = torch.cat([probs_0, probs_1])
                        ##print("SIZES:", probs_0.size(), probs_1.size(), probs.size())
                        probs = torch.nn.Softmax()(probs)
                        #print("SIZE:", probs_0.size(), probs_1.size())
                        probs_0, probs_1 = torch.split(probs, [probs_0.size()[0], probs_1.size()[0]])#torch.nn.Softmax()(probs_0)
                        #probs_1 = #torch.nn.Softmax()(probs_1)

                        #print(probs_0)
                        #print("%%%%%%%%%%%%%%%%%%%")
                        #print(probs_1)

                        grp_prob_0 = probs_0.sum()
                        grp_prob_1 = probs_1.sum()

                        #grp_prob_0 = probs_0.mean()
                        #grp_prob_1 = probs_1.mean()

                        #print(grp_prob_0)
                        #print("****************")
                        #print(grp_prob_1)

                        #exit(0)
                        self.cum_prob_0 += grp_prob_0.item()
                        self.cum_prob_1 += grp_prob_1.item()


                        # Probability Stuff Here
                        beta_vae_loss = recon_loss + self.beta*total_kld + 200*(grp_prob_0 - grp_prob_1)**2

                    elif self.objective == 'B':
                        C = torch.clamp(self.C_max/self.C_stop_iter*self.global_iter, 0, self.C_max.item())
                        #if self.global_iter == 1:
                            #C = Variable(cuda(torch.FloatTensor(0), self.use_cuda))
                        #if self.global_iter % 100000 == 0:
                            #C = torch.clamp(C * 2, 0.1, self.C_max.data[0])
                            #C = torch.clamp(C + 5, 0, self.C_max.data[0])
                        #C = torch.clamp(C_mult * (self.global_iter // self.C_stop_iter), 0.0, self.C_max.data[0])
                        beta_vae_loss = recon_loss + self.gamma*(total_kld-C).abs()

                        # NEW: Ayan
                        # TODO: REMOVE THIS CODE LATER!!! THIS IS JUST FOR TESTING
                        if self.do_modify:
                            if self.global_iter in self.modify_step:
                                self.modify = True

                            # Decay the learning rate here... We decay to lr/10 in 200k steps
                            if self.global_iter >= self.modify_step[0] and self.global_iter <= (self.modify_step[0] + 2e5):
                                # Reduce it 4 times, each time divide by half.
                                if self.global_iter % 50000 == 0:
                                    print("*****REDUCING LEARNING RATE*****")
                                    for param_group in self.optim.param_groups:
                                        param_group['lr'] = param_group['lr'] / 2


                            # If the code stops in between two modify steps, then also the flag should be True.
                            if self.global_iter > self.modify_step[0]:
                                #selected_dims = self.sorted_dims[0:self.select_top_dims]
                                selected_dims = [4, 5, 31] # Hand selected dimensions for code running on DGX1
                                self.modify = True

                            if self.modify == True and self.global_iter in self.modify_step:
                                print("*********WE ARE MODIFYING THE OBJECTIVE TO INCREASE CAPACITY ON SELECTED DIMENSIONS*********")
                                # Now we need to modify the objective


                                #selected_dims = self.sorted_dims[0:self.select_top_dims]
                                selected_dims = [4, 5, 31]


                                print("Selected Dimensions:", selected_dims)
                                sum_selected_kld = 0
                                for dim in selected_dims:
                                    sum_selected_kld += dim_wise_kld[dim]
                                self.sum_selected_kld = sum_selected_kld
                                self.dim_kld_for_modify = dim_wise_kld
                                self.sum_all_kld = total_kld

                            if self.modify:
                                # First time modifying
                                if len(self.modify_step) > 1:
                                    if self.global_iter >= self.modify_step[0] and self.global_iter < self.modify_step[1]:
                                        C -= self.sum_selected_kld
                                    else:
                                        # AYAN: Check this...
                                        C = torch.max(C - self.sum_selected_kld, self.sum_all_kld - self.sum_selected_kld)
                                else:
                                    C -= self.sum_selected_kld
                                sum_selected_kld = 0
                                #for dim in self.sorted_dims[0:self.select_top_dims]:
                                for dim in selected_dims:
                                    sum_selected_kld += dim_wise_kld[dim]
                                first_term = (total_kld - sum_selected_kld - C).abs()
                                second_term = 0
                                C_dims = []
                                sum_1 = 0
                                for score in self.dim_scores[:self.select_top_dims]:
                                    sum_1 += abs(score)
                                tot_scores = sum_1 #sum(self.dim_scores[:self.select_top_dims])
                                #print(tot_scores)

                                # AYAN: Two ways we can define the second term, "1" denotes coarse, "2" denotes fine.
                                # "2" is causing numerical instability. Trying out "1"...
                                if self.modify_split == "1":
                                    #for dim in selected_dims:
                                    C_2 = 2.0
                                    second_term = (sum_selected_kld - C_2).abs()
                                    pass
                                elif self.modify_split == "2":
                                    i = -1
                                    for dim in selected_dims:
                                        #steps = self.modify_step + 1 + self.modify_increase_step
                                        #C_selected_dim = torch.clamp((2 * self.dim_kld_for_modify[dim])/steps*self.global_iter, \
                                        #        float(self.dim_kld_for_modify[dim][0]), float((2 * self.dim_kld_for_modify[dim][0])))
                                        #C_selected_dim = torch.clamp()
                                        i += 1
                                        if len(self.modify_step) > 1:
                                            if self.global_iter >= self.modify_step[0] and self.global_iter < self.modify_step[1]:
                                                #stp = 3 * (self.dim_scores[i] / tot_scores)
                                                stp = 0.5
                                                s_1 = self.modify_step[0]
                                            #elif self.global_iter >= self.modify_step[1] and self.global_iter < self.modify_step[2]:
                                            #    stp = 4.5 * (self.dim_scores[i] / tot_scores)
                                            #    s_1 = self.modify_step[1]
                                            elif self.global_iter >= self.modify_step[1]:
                                                #stp = 2 * (self.dim_scores[i] / tot_scores)
                                                stp = 0.5
                                                s_1 = self.modify_step[1]
                                            else:
                                                # We should not be here.
                                                stp = 0.0
                                                s_1 = 0
                                        else:
                                            if self.global_iter >= self.modify_step[0]:
                                                stp = 0.5
                                                s_1 = self.modify_step[0]

                                        stp = float(stp)

                                        if self.global_iter % 1000 == 0:
                                            print("Increment Step:", stp)

                                        if self.modify_type == "d": # directly increase to target value
                                            C_selected_dim = stp + self.dim_kld_for_modify[dim]
                                        elif self.modify_type == "i": # incrementally go to target KLD value
                                            change_rate_kld = stp / self.modify_increase_step
                                            base_kld = self.dim_kld_for_modify[dim]
                                            ceil_kld = stp + self.dim_kld_for_modify[dim]
                                            C_selected_dim = torch.clamp(base_kld + (change_rate_kld * (self.global_iter - s_1)), base_kld.data[0], ceil_kld.data[0])
                                            if self.global_iter % 2500 == 0:
                                                print("***************")
                                                print("Dim:", dim, "Base_KLD:", base_kld.data[0], "Ceil_KLD:", ceil_kld.data[0], "Calc C:", C_selected_dim)
                                        C_dims.append(C_selected_dim)
                                        second_term += (dim_wise_kld[dim] - C_selected_dim).abs()
                                beta_vae_loss = recon_loss + self.gamma*(first_term + second_term)

                                if self.global_iter % 1000 == 0:
                                    #print("C:", C, "C_dims:", C_dims)
                                    print("Dim KLDs:", dim_wise_kld.data.cpu().numpy())
                                    print("Total KLD:", total_kld)
                                    print("Part_1 KLD:", (total_kld - sum_selected_kld))
                                    print("Part_2 KLD:", [dim_wise_kld[dim] for dim in selected_dims])
                                    #print("Increment Step:", stp)
                                    print("Split Type:", self.modify_split)
                                    #print("Selected top dims:", self.sorted_dims[:self.select_top_dims])
                                    print("Selected top dims:", selected_dims)
                        pass


                    # NEW: Ayan
                    #elif self.objective == 'B_mod':
                    #    C = torch.clamp(self.C_max/self.C_stop_iter*self.global_iter, 0, self.C_max.data[0])
                    #    # This gives us the vector of dimension-wise C values.
                    #    C_arr = torch.mul(C, self.C_dim_weights)
                    #    beta_vae_loss = recon_loss + self.gamma*((dim_wise_kld - C_arr).abs().sum())

                    elif self.objective == 'B_mod':
                        C_dim_weights = Variable(cuda(torch.FloatTensor([1/12 for _ in range(12)] + [0.0 for _ in range(self.z_dim - 12)]), self.use_cuda))
                        C = self.C_max.data[0]
                        C_arr = torch.mul(C_dim_weights, C)
                        beta_vae_loss = recon_loss + self.gamma*((dim_wise_kld - C_arr).abs().sum())

                    self.optim.zero_grad()
                    beta_vae_loss.backward(retain_graph=True)
                    self.optim.step()

                    # AYAN
                    self.dim_klds = dim_wise_kld.data.cpu().numpy()

                    if self.viz_on and self.global_iter%self.gather_step == 0:
                        self.gather.insert_bck(iter=self.global_iter,
                                           mu=mu.mean(0).data, var=logvar.exp().mean(0).data,
                                           recon_loss=recon_loss.data, total_kld=total_kld.data,
                                           dim_wise_kld=dim_wise_kld.data, mean_kld=mean_kld.data)
                    #if self.global_iter%1000 == 0 and self.objective == 'H_mod':
                    #        pbar.write('Group_0 Probability: {:.3f}, Group_1 Probability: {:.3f}'.format(grp_prob_0.item(), grp_prob_1.item()))

                    if self.global_iter%self.display_step == 0:
                        pbar.write('[{}] recon_loss:{:.3f} total_kld:{:.3f} mean_kld:{:.3f}'.format(
                            self.global_iter, recon_loss.item(), total_kld.item(), mean_kld.item()))

                        if self.objective == 'H_mod':
                            pbar.write('Group_0 Probability: {:.3f}, Group_1 Probability: {:.3f}'.format(self.cum_prob_0/self.global_iter, self.cum_prob_1/self.global_iter))

                        var = logvar.exp().mean(0).data
                        var_str = ''
                        for j, var_j in enumerate(var):
                            var_str += 'var{}:{:.4f} '.format(j+1, var_j)
                        pbar.write(var_str)

                        if self.objective == 'B':
                            pbar.write('C:{:.3f}'.format(C.data[0]))

                        if self.objective == 'B_mod':
                            #pbar.write('C:{:.3f}'.format(C.data[0]))
                            arr_C = C_arr.data
                            C_str = ''
                            for j, C_j in enumerate(arr_C):
                                C_str += 'C_val{}:{:.4f} '.format(j, C_j)
                            pbar.write(C_str)
                            #pbar.write('C_array:', C_arr)

                        if self.viz_on:
                            self.gather.insert_bck(images=x.data)
                            self.gather.insert_bck(images=torch.sigmoid(x_recon).data)
                            self.viz_reconstruction()
                            self.viz_lines()
                            self.gather.flush()

                        if self.viz_on or self.save_output:
                            # TURINING OFF FOR NOW!
                            if self.objective == 'B':
                                if self.global_iter <= self.C_stop_iter:
                                    self.viz_traverse(C=C.data)
                                    pass
                                else:
                                    if self.global_iter % (self.display_step*10) == 0 or self.global_iter == 1500001:
                                        self.viz_traverse(C=C.data)
                            else:
                                self.viz_traverse()

                    steps_rand = [300000, 500001, 700001, 900000, 1.2e6, 1.5e6, 1.5e6+1]

                    if self.global_iter in steps_rand:
                        self.random_img_gen(500)
                        path = os.path.join(self.output_dir,'step_'+str(self.global_iter),'random_traverse')
                        files = [f for f in glob.glob(os.path.join(path, "*.png"))]
                        jsons = [f for f in glob.glob(os.path.join(path, "*.json"))]
                        if len(files) == 0 or (len(files) != 0 and len(files) != len(jsons)):
                            self.last_gen_step = self.global_iter
                            self.random_img_traverse(num_random=5)
                        else:
                            print("Already have processed JSONs. Ignoring random generations for traversal. Number of JSONs:", len(jsons))

                    if self.last_gen_step is None or self.last_gen_step < steps_rand[0]:
                        lst_step = 0
                        #print("*** GLOBAL ITER****", self.global_iter)
                        for step in steps_rand:
                            if self.global_iter >= step:
                                lst_step = step
                        self.last_gen_step = lst_step


                    #steps_api_call = [500001, 900000]
                    steps_api_call = [self.api_step,]

                    #print("*** Last_Gen_Step: *****", self.last_gen_step)

                    if self.do_modify:
                        if self.last_gen_step is not None and self.last_gen_step >= steps_rand[0]:
                            if self.global_iter in steps_api_call:
                                #path = os.path.join(self.output_dir,'step_'+str(self.last_gen_step),'random_traverse')
                                path = os.path.join(self.output_dir, 'step_'+str(self.global_iter),'random_traverse')
                                # Get the dimensions that are over a particular threshold. These are the dimensions that have learnt
                                # some factor of variation.
                                zrange = (self.dim_klds > 0.5).nonzero()[0]
                                self.sorted_dims, self.dim_scores = api_get_attr_dims(path, list_dims=zrange)
                                with open(self.viz_name + "_api_sort_scores.txt", "w", encoding='utf8') as f:
                                    f.write("Dimensions sorted")
                                    for i in range(len(self.sorted_dims)):
                                        f.write("Dim: "+str(self.sorted_dims[i])+" Score: "+str(self.dim_scores[i]))
                                    print("File written for scores")
                                print("Sorted Dimensions:", self.sorted_dims)
                                print("Corresponding Scores:", self.dim_scores)
                    #if self.global_iter == 500000:
                    #    exit(0)
                    # Save the checkpoint
                    if self.global_iter%self.save_step == 0:
                        self.save_checkpoint('last')
                        pbar.write('Saved checkpoint(iter:{})'.format(self.global_iter))

                    if self.global_iter%50000 == 0:
                        self.save_checkpoint(str(self.global_iter))

                    if self.global_iter >= self.max_iter:
                        out = True
                        break

            pbar.write("[Training Finished]")
            pbar.close()


    def audit(self):
        '''
        See if we can have separate test data loader? If needed.
        '''
        print("DEBUG: In Audit Mode")
        print("DEBUG: Viz Mode", self.viz_on)
        print("DEBUG: Audit Label", self.set_audit_label)
       # Set training mode off.
        self.net_mode(train=False)
        num_batches = len(self.data_loader.dataset) // self.batch_size
        pbar = tqdm(total=num_batches)
        test_iter = 0
        #pbar.update(test_iter)

        list_total_kld = [[] for _ in range(self.classes)]
        list_mean_kld = [[] for _ in range(self.classes)]
        list_recon_loss = [[] for _ in range(self.classes)]
        list_prob = [[] for _ in range(self.classes)]

        #if AUDIT_EXP:
        #    list_total_kld_0, list_total_kld_1 = [], []
            #list_mean_kld = []
        #    list_recon_loss_0, list_recon_loss_1 = [], []
        #    list_prob_0, list_prob_1 = [], []


        #overall_total_kld_0, overall_mean_kld_0 = 0, 0
        #overall_recon_loss_0 = 0
        #overall_total_kld_1, overall_mean_kld_1, overall_recon_loss_1 = 0, 0, 0

        #list_total_kld_0, list_total_kld_1 = [], []
        #list_mean_kld_0, list_mean_kld_1 = [], []
        #list_prob_0, list_prob_1 = [], []

        #list_loss_0, list_loss_1 = [], []

        #img_ids_0, img_ids_1 = [], []
        if not self.only_random and self.demo_test:
            output_dir = os.path.join(self.output_dir, 'recons_audit')
            os.makedirs(output_dir, exist_ok = True)
            count_classes = None

        for x, idx, label, classes, attr_dict in self.data_loader:
            test_iter += 1
            pbar.update(1)
            #attr_dict = attr_dict[0]
            #print(label)
            x = Variable(cuda(x, self.use_cuda))
            #attr = attr_dict[self.attr]
            #print("ATTR:", attr)
            #exit(0)
            if self.condition:
                try:
                    lb = classes[label[0]][0]
                    select_attr = list(map(attr_dict.__getitem__, self.attr))
                    #select_attr = [[float(select_attr[0][i]), float(select_attr[1][i])] for i in range(len(select_attr[0]))]
                    #print(select_attr)
                    select_attr = [float(i) for x in select_attr for i in x]

                    #select_attr = attr_dict[self.attr]
                except Exception as e:
                    print(classes, label)
                    raise(e)
                #print("SELECT:", select_attr)
                #if int(select_attr[0]) == 1:
                #    c = [1]
                #else:
                #    c = [-1]
                c = [select_attr]
                if self.set_audit_label[0] == 0:
                    #if '_male' in lb:
                    x_recon, mu, logvar, _ = self.net(x, c, None)
                else:
                    #if  == 1:
                    #    dec_attr = [1]
                    #else:
                    dec_attr = [self.set_audit_label]
                    #print("Here", c, dec_attr, self.set_audit_label)
                    #print(dec_attr)
                    #self._encode(x,c)
                    #c = [self.set_audit_label]
                    x_recon, mu, logvar, _ = self.net(x, c, dec_attr)
                #if AUDIT_EXP:
                #    c = [1]
                #    x_recon_1, mu_1, logvar_1, _ = self.net(x,c)
                #    c = [-1]
                #    x_recon_0, mu_0, logvar_0, _ = self.net(x,c)
            else:
                x_recon, mu, logvar, _ = self.net(x)
            recon_loss = reconstruction_loss(x, x_recon, self.decoder_dist)
            total_kld, dim_wise_kld, mean_kld = kl_divergence(mu, logvar)
            probs, mean_prob_dims = find_prob(mu)

            # Store the stats.
            #print(label, type(label))
            label = int(label)

            list_total_kld[label].append(float(total_kld.data))
            list_mean_kld[label].append(float(mean_kld.data))
            list_recon_loss[label].append(float(recon_loss.data))
            list_prob[label].append(mean_prob_dims)

            #if AUDIT_EXP:
            #    recon_loss_0 = reconstruction_loss(x, x_recon_0, self.decoder_dist)
            #    recon_loss_1 = reconstruction_loss(x, x_recon_1, self.decoder_dist)
            #    total_kld_0, _, _ = kl_divergence(mu_0, logvar_0)
            #    total_kld_1, _, _ = kl_divergence(mu_1, logvar_1)
            #    probs_0, mean_prob_dims_0 = find_prob(mu_0)
            #    probs_1, mean_prob_dims_1 = find_prob(mu_1)

            #    list_total_kld_0.append(float(total_kld_0.data))
            #    list_total_kld_1.append(float(total_kld_1.data))
            #    list_recon_loss_0.append(float(recon_loss_0.data))
            #    list_recon_loss_1.append(float(recon_loss_1.data))
            #    list_prob_0.append(mean_prob_dims_0)
            #    list_prob_1.append(mean_prob_dims_1)


            '''if attr[0] == '-1':
                list_total_kld_0.append(float(total_kld.data))
                #print(type(list_total_kld_0[0]))
                list_mean_kld_0.append(float(mean_kld.data))
                list_prob_0.append(mean_prob_dims)
                img_ids_0.append(idx)
                list_loss_0.append(float(recon_loss.data))
                overall_recon_loss_0 += recon_loss
                overall_total_kld_0 += total_kld
                overall_mean_kld_0 += mean_kld
            elif attr[0] == '1':
                list_total_kld_1.append(float(total_kld.data))
                list_mean_kld_1.append(float(mean_kld.data))
                list_prob_1.append(mean_prob_dims)
                img_ids_1.append(idx)
                list_loss_1.append(float(recon_loss.data))
                overall_recon_loss_1 += recon_loss
                overall_total_kld_1 += total_kld
                overall_mean_kld_1 += mean_kld
            '''
            #overall_total_kld += total_kld
            #overall_mean_kld += mean_kld

            '''random_z = Variable(cuda(torch.randn(200, self.z_dim), self.use_cuda), volatile=True)
            recon_random = torch.sigmoid(self.net.decoder(random_z)).data
            count = 0
            output_dir = os.path.join(self.output_dir,'random_generate')
            os.makedirs(output_dir, exist_ok=True)'''

            if not self.only_random and self.demo_test:
                # Init counts
                if count_classes is None:
                    count_classes = {}
                    #print(classes)
                    for c in classes:
                        count_classes[c[0]] = 0
                # Update count for this image
                # Label returns idx I think.
                count_classes[classes[label][0]] += 1

                save_image(tensor=x.data.cpu(), filename=os.path.join(output_dir, classes[label][0]+'_org_'+str(count_classes[classes[label][0]])+'.png'), pad_value=0)
                save_image(tensor=torch.sigmoid(x_recon).data.cpu(), filename=os.path.join(output_dir, classes[label][0]+'_recon_'+str(count_classes[classes[label][0]])+'.png'), pad_value=0)

            if self.viz_on:
                self.gather.insert(images=x.data, label=label)
                #print(x.size())
                self.gather.insert(recons=torch.sigmoid(x_recon).data, label=label)
                #self.gather.insert(total_kld=float(total_kld.data), label=label)
                #self.gather.insert(prob=mean_prob_dims, label=label)
                #self.gather.insert(recon_loss=float(recon_loss.data), label=label)


            '''if self.viz_on:
                if attr [0] == '-1':
                    self.gather.insert(images=x.data)
                    self.gather.insert(recons=torch.sigmoid(x_recon).data)
                    self.gather.insert_bck(total_kld=float(total_kld.data))
                    self.gather.insert_bck(prob=mean_prob_dims)
                    self.gather.insert_bck(recon_loss=float(recon_loss.data))
                    #self.gather.insert(mean_kld=float(mean_kld.data))
                elif attr[0] == '1':
                    self.gather.insert(images1=x.data)
                    self.gather.insert(recons1=torch.sigmoid(x_recon).data)
                    self.gather.insert_bck(total_kld1=float(total_kld.data))
                    self.gather.insert_bck(prob1=mean_prob_dims)
                    self.gather.insert_bck(recon_loss1=float(recon_loss.data))
                    #self.gather.insert(mean_kld1=float(mean_kld.data))
            '''
        # Visualize after all test.

        list_total_kld = np.array(list_total_kld)
        list_mean_kld = np.array(list_mean_kld)
        list_recon_loss = np.array(list_recon_loss)
        list_prob = np.array(list_prob)

        classes = [cls for sublist in classes for cls in sublist]

        if self.viz_on:
            # Push in the class labes which we can use in visualizations.
            self.gather.insert(classes=classes)

        #mean_total_kld_0 = overall_total_kld_0 / len(img_ids_0)
        #mean_mean_kld_0 = overall_mean_kld_0 / len(img_ids_0)
        #mean_recon_loss_0 = overall_recon_loss_0 / len(img_ids_0)
        #mean_total_kld_1 = overall_total_kld_1 / len(img_ids_1)
        #mean_mean_kld_1 = overall_mean_kld_1 / len(img_ids_1)
        #mean_recon_loss_1 = overall_recon_loss_1 / len(img_ids_1)

        #mean_prob_0 = np.mean(list_prob_0)
        #mean_prob_1 = np.mean(list_prob_1)

        pbar.write("[Testing Finished.]")
        pbar.close()
        #pbar.write("[Test] recon_loss:{:.3f} total_kld:{:.3f} mean_kld:{:.3f}".format(mean_recon_loss, mean_total_kld, mean_mean_kld))

        print("Attributes")
        print("---------------------")
        for i in range(len(classes)):
            print(classes[i],":",len(list_total_kld[i]), end=' ')
        print("\n")

        for i in range(len(classes)):
            print("Audit "+classes[i])
            print("Recon_Loss: %.3f | Total KLD: %.3f | Mean KLD %.3f" %(np.mean(list_recon_loss[i]),\
                    np.mean(list_total_kld[i]), np.mean(list_mean_kld[i])))
            print("Probability: %.3f" %(np.mean(list_prob[i])))


        #print("Attribute:", self.attr, "-1:", len(img_ids_0), "1:", len(img_ids_1))
        '''print("TEST TIME_(-1): Recon_Loss: %.3f Total KLD: %.3f Mean KLD: %.3f" %(mean_recon_loss_0, mean_total_kld_0, mean_mean_kld_0))
        print("TEST TIME_(-1): Probability: %.3f" %(mean_prob_0))
        print("TEST TIME_(-1): Max 10 Total KLD:", heapq.nlargest(10, list_total_kld_0))
        print("TEST TIME_(-1): Max 10 Probabilities:", heapq.nlargest(10, list_prob_0))
        #print("TEST TIME_(-1): Max 10 Mean KLD:", heapq.nlargest(10, list_mean_kld_0))
        print("TEST TIME_(-1): Min 10 Total KLD:", heapq.nsmallest(10, list_total_kld_0))
        print("TEST TIME_(-1): Min 10 Probabilities:", heapq.nsmallest(10, list_prob_0))
        #print("TEST TIME_(-1): Min 10 Mean KLD:", heapq.nsmallest(10, list_mean_kld_0))
        print("TEST TIME_(1): Recon_Loss: %.3f Total KLD: %.3f Mean KLD: %.3f" %(mean_recon_loss_1, mean_total_kld_1, mean_mean_kld_1))
        print("TEST TIME_(1): Probability: %.3f" %(mean_prob_1))
        print("TEST TIME_(1): Max Total KLD:", heapq.nlargest(10, list_total_kld_1))
        print("TEST TIME_(1): Max 10 Probabilities:", heapq.nlargest(10, list_prob_1))
       #print("TEST TIME_(1): Max Mean KLD:", heapq.nlargest(10, list_mean_kld_1))
        print("TEST TIME_(1): Min Total KLD:", heapq.nsmallest(10, list_total_kld_1))
        print("TEST TIME_(1): Min 10 Probabilities:", heapq.nsmallest(10, list_prob_1))
      #print("TEST TIME_(1): Min Mean KLD:", heapq.nsmallest(10, list_mean_kld_1))'''

        if PRINT_ALL:
            print("--------------------------------------------------")
            print("*********** DUMPING ALL ****************")
            #print("$$$$$$$$$$$ RECON_LOSS_0 $$$$$$$$$$$$$$$$$")
            #print(list_loss_0)
            #print("$$$$$$$$$$$ RECON_LOSS_1 $$$$$$$$$$$$$$$$$")
            #print(list_loss_1)
            #print("$$$$$$$$$$$ PROBS_0 $$$$$$$$$$$$$$$$$")
            #print(list_prob_0)
            #print("$$$$$$$$$$$ PROBS_1 $$$$$$$$$$$$$$$$$")
            #print(list_prob_1)
            exp_probs = []
            total_count_pop = 0
            for i in range(len(classes)):
                print("HERE WE ARE:", np.mean(list_prob[i]), np.median(list_prob[i]))
                exp_probs.append(np.exp(list_prob[i]))
                print("AGAIN... EXP:", np.mean(exp_probs[-1]), np.median(exp_probs[-1]))
                total_count_pop += len(list_prob[i])
            normalizing_prob_val = np.sum([np.sum(exp_probs[i]) for i in range(len(classes))])
            uniform_prob = 1 / total_count_pop
            scaled_probs = []
            for i in range(len(classes)):
                temp = exp_probs[i]
                scaled_probs.append(temp / normalizing_prob_val)
            max_val = np.max([np.max(scaled_probs[i]) for i in range(len(classes))])
            for i in range(len(classes)):
                print("CLASS:", classes[i])
                print("Probability:")
                print(str(list(scaled_probs[i])))
                print("CLASS SUM PROB:", np.sum(scaled_probs[i]))
                print("MEAN PROBS:", np.mean(scaled_probs[i]))
                print("$$$$$$$$$$$$$$$$$$$$$$$$$$")

                sorted_l = sorted(list(scaled_probs[i]), reverse=True)
                #plt.figure(figsize=(13,9))

                plt.bar(list(range(1, 1 + len(scaled_probs[i]))), sorted_l)
                plt.xlabel("Image ID")
                plt.ylabel("Probability Score")
                plt.title("Probability of Audit Pictures. Class: "+classes[i])
                plt.axhline(y=uniform_prob, label='Uniform Probability', color='red')
                plt.legend()
                plt.ylim(0.0, max_val)
                plt.savefig(str(self.viz_name)+'_prob_'+classes[i]+'.png')
                plt.clf()

            print("DONE!!!")



        if self.viz_on:
            print("DISPLAYING PICS")
            self.audit_viz_reconstruction()
            self.gather.flush()
            a_list = [12,30,14]
            for x in range(self.z_dim):
                if x not in a_list:
                    a_list.append(x)

            #for a in a_list:
            #    print("||",a,"||")
            #self.viz_traverse(is_train=False, z_order = a_list)

        if test_traverse:
            self.test_viz_traverse(num_rands=26)



    def test(self):
        '''
        See if we can have separate test data loader? If needed.
        '''


       # Set training mode off.
        self.net_mode(train=False)
        num_batches = len(self.data_loader.dataset) // self.batch_size
        pbar = tqdm(total=num_batches)
        test_iter = 0
        #pbar.update(test_iter)
        overall_total_kld_0, overall_mean_kld_0 = 0, 0
        overall_recon_loss_0 = 0
        overall_total_kld_1, overall_mean_kld_1, overall_recon_loss_1 = 0, 0, 0

        list_total_kld_0, list_total_kld_1 = [], []
        list_mean_kld_0, list_mean_kld_1 = [], []
        list_prob_0, list_prob_1 = [], []

        list_loss_0, list_loss_1 = [], []

        img_ids_0, img_ids_1 = [], []

        # Store the variances
        list_vars_0, list_vars_1 = [], []
        # Store the means
        list_means_0, list_means_1 = [], []

        for x, idx, attr_dict in self.data_loader:
            test_iter += 1
            pbar.update(1)
            #attr_dict = attr_dict[0]
            x = Variable(cuda(x, self.use_cuda))
            #attr = attr_dict[self.attr]
            #attr[0] = float(attr[0])
            label = list(map(attr_dict.__getitem__, self.attr))
            attr = [[float(label[0][i]), float(label[1][i])] for i in range(len(label[0]))]

            if self.condition:
                x_recon, mu, logvar, _ = self.net(x, attr)
            else:
                x_recon, mu, logvar, _ = self.net(x)
            recon_loss = reconstruction_loss(x, x_recon, self.decoder_dist)
            total_kld, dim_wise_kld, mean_kld = kl_divergence(mu, logvar)
            probs, mean_prob_dims = find_prob(mu)

            tot_var = np.sum(logvar.data.numpy())
            avg_mu = np.mean(mu.data.numpy())

            if attr[0] == -1:
                list_total_kld_0.append(float(total_kld.data))
                #print(type(list_total_kld_0[0]))
                list_mean_kld_0.append(float(mean_kld.data))
                list_prob_0.append(mean_prob_dims)
                img_ids_0.append(idx)
                list_loss_0.append(float(recon_loss.data))
                overall_recon_loss_0 += recon_loss
                overall_total_kld_0 += total_kld
                overall_mean_kld_0 += mean_kld

                list_vars_0.append(tot_var)
                list_means_0.append(avg_mu)
            elif attr[0] == 1:
                list_total_kld_1.append(float(total_kld.data))
                list_mean_kld_1.append(float(mean_kld.data))
                list_prob_1.append(mean_prob_dims)
                img_ids_1.append(idx)
                list_loss_1.append(float(recon_loss.data))
                overall_recon_loss_1 += recon_loss
                overall_total_kld_1 += total_kld
                overall_mean_kld_1 += mean_kld

                list_vars_1.append(tot_var)
                list_means_1.append(avg_mu)


            #overall_total_kld += total_kld
            #overall_mean_kld += mean_kld

            if self.viz_on:
                if attr [0] == -1:
                    self.gather.insert(images=x.data)
                    self.gather.insert(recons=torch.sigmoid(x_recon).data)
                    self.gather.insert_bck(total_kld=float(total_kld.data))
                    self.gather.insert_bck(prob=mean_prob_dims)
                    self.gather.insert_bck(recon_loss=float(recon_loss.data))

                    self.gather.insert_bck(total_var=float(tot_var))
                    #self.gather.insert(mean_kld=float(mean_kld.data))
                elif attr[0] == 1:
                    self.gather.insert(images1=x.data)
                    self.gather.insert(recons1=torch.sigmoid(x_recon).data)
                    self.gather.insert_bck(total_kld1=float(total_kld.data))
                    self.gather.insert_bck(prob1=mean_prob_dims)
                    self.gather.insert_bck(recon_loss1=float(recon_loss.data))

                    self.gather.insert_bck(total_var1=float(tot_var))
                    #self.gather.insert(mean_kld1=float(mean_kld.data))

        # Visualize after all test.

        mean_total_kld_0 = overall_total_kld_0 / len(img_ids_0)
        mean_mean_kld_0 = overall_mean_kld_0 / len(img_ids_0)
        mean_recon_loss_0 = overall_recon_loss_0 / len(img_ids_0)
        mean_total_kld_1 = overall_total_kld_1 / len(img_ids_1)
        mean_mean_kld_1 = overall_mean_kld_1 / len(img_ids_1)
        mean_recon_loss_1 = overall_recon_loss_1 / len(img_ids_1)

        mean_prob_0 = np.mean(list_prob_0)
        mean_prob_1 = np.mean(list_prob_1)

        mean_var_0 = np.mean(list_vars_0)
        mean_var_1 = np.mean(list_vars_1)

        list_vars_0 = np.array(list_vars_0)
        list_vars_1 = np.array(list_vars_1)
        min_args_vars_0 = np.argsort(list_vars_0)
        min_args_vars_1 = np.argsort(list_vars_1)
        max_args_vars_0 = min_args_vars_0[::-1]
        max_args_vars_1 = min_args_vars_1[::-1]

        list_means_0 = np.array(list_means_0)
        list_means_1 = np.array(list_means_1)
        min_args_means_0 = np.argsort(list_means_0)
        min_args_means_1 = np.argsort(list_means_1)
        max_args_means_0 = min_args_means_0[::-1]
        max_args_means_1 = min_args_means_1[::-1]

        pbar.write("[Testing Finished.]")
        pbar.close()
        #pbar.write("[Test] recon_loss:{:.3f} total_kld:{:.3f} mean_kld:{:.3f}".format(mean_recon_loss, mean_total_kld, mean_mean_kld))
        print("Attribute:", self.attr, "-1:", len(img_ids_0), "1:", len(img_ids_1))
        print("TEST TIME_(-1): Recon_Loss: %.3f Total KLD: %.3f Mean KLD: %.3f" %(mean_recon_loss_0, mean_total_kld_0, mean_mean_kld_0))
        print("TEST TIME_(-1): Probability: %.3f, Total Log Variance: %.3f" %(mean_prob_0, mean_var_0))
        print("TEST TIME_(-1): Max 10 Total KLD:", heapq.nlargest(10, list_total_kld_0))
        #print("TEST TIME_(-1): Max
        print("TEST TIME_(-1): Max 10 Probabilities:", heapq.nlargest(10, list_prob_0))
        #print("TEST TIME_(-1): Max 10 Variances:", heapq.nlargest(10, list_vars_0))
        print("TEST TIME_(-1): Max 10 Variances:", list_vars_0[max_args_vars_0[:10]])
        print("TEST TIME_(-1): Corresponding Mean Values:", list_means_0[max_args_vars_0[:10]])
        #print("TEST TIME_(-1): Max 10 Mean KLD:", heapq.nlargest(10, list_mean_kld_0))
        print("TEST TIME_(-1): Min 10 Total KLD:", heapq.nsmallest(10, list_total_kld_0))
        print("TEST TIME_(-1): Min 10 Probabilities:", heapq.nsmallest(10, list_prob_0))
        #print("TEST TIME_(-1): Min 10 Variances:", heapq.nsmallest(10, list_vars_0))
        print("TEST TIME_(-1): Min 10 Variances:", list_vars_0[min_args_vars_0[:10]])
        print("TEST TIME_(-1): Corresponding Mean Values:", list_means_0[min_args_vars_0[:10]])
#print("TEST TIME_(-1): Min 10 Mean KLD:", heapq.nsmallest(10, list_mean_kld_0))
        print("TEST TIME_(1): Recon_Loss: %.3f Total KLD: %.3f Mean KLD: %.3f" %(mean_recon_loss_1, mean_total_kld_1, mean_mean_kld_1))
        print("TEST TIME_(1): Probability: %.3f, Total Log Variance: %.3f" %(mean_prob_1, mean_var_1))
        print("TEST TIME_(1): Max Total KLD:", heapq.nlargest(10, list_total_kld_1))
        print("TEST TIME_(1): Max 10 Probabilities:", heapq.nlargest(10, list_prob_1))
        #print("TEST TIME_(1): Max 10 Variances:", heapq.nlargest(10, list_vars_1))
        print("TEST TIME_(1): Max 10 Variances:", list_vars_1[max_args_vars_1[:10]])
        print("TEST TIME_(1): Corresponding Mean Values:", list_means_1[max_args_vars_1[:10]])
 #print("TEST TIME_(1): Max Mean KLD:", heapq.nlargest(10, list_mean_kld_1))
        print("TEST TIME_(1): Min Total KLD:", heapq.nsmallest(10, list_total_kld_1))
        print("TEST TIME_(1): Min 10 Probabilities:", heapq.nsmallest(10, list_prob_1))
        #print("TEST TIME_(1): Min Mean KLD:", heapq.nsmallest(10, list_mean_kld_1))
        #print("TEST TIME_(1): Min 10 Variances:", heapq.nsmallest(10, list_vars_1))
        print("TEST TIME_(1): Min 10 Variances:", list_vars_1[min_args_vars_1[:10]])
        print("TEST TIME_(1): Corresponding Mean Values:", list_means_1[min_args_vars_1[:10]])

        sorted_prob_0 = sorted(list_prob_0, reverse=True)
        list_exp_prob_0, list_exp_prob_1 = np.exp(sorted_prob_0[:]), np.exp(list_prob_1)
        normalizing_factor = np.sum([np.sum(list_exp_prob_0), np.sum(list_exp_prob_1)])
        scale_prob_0, scale_prob_1 = list_exp_prob_0 / normalizing_factor, list_exp_prob_1 / normalizing_factor
        print("**************************************************************")
        print("Probability for Attribute", self.attr, "(-1):", np.sum(scale_prob_0), np.mean(scale_prob_0))
        print("Probability for Attribute", self.attr, "(+1):", np.sum(scale_prob_1), np.mean(scale_prob_1))

        if PRINT_ALL:
            print("--------------------------------------------------")
            print("*********** DUMPING ALL ****************")
            print("$$$$$$$$$$$ RECON_LOSS_0 $$$$$$$$$$$$$$$$$")
            print(list_loss_0)
            print("$$$$$$$$$$$ RECON_LOSS_1 $$$$$$$$$$$$$$$$$")
            print(list_loss_1)
            print("$$$$$$$$$$$ PROBS_0 $$$$$$$$$$$$$$$$$")
            print(list_prob_0)
            print("$$$$$$$$$$$ PROBS_1 $$$$$$$$$$$$$$$$$")
            print(list_prob_1)



        if self.viz_on:
            self.test_viz_reconstruction()
            self.gather.flush()



    # AYAN
    def random_img_gen(self, num_random=100):
        self.net_mode(train=False)
        #torch.seed()
        #torch.cuda.seed()
        seed()
        cuda_seed()
        np.random.seed()
        random_z = Variable(cuda(torch.randn(num_random, self.z_dim), self.use_cuda), volatile=True)
        if self.condition:
            #random_labs = torch.round(torch.rand(200))
            random_labs = np.random.randint(2, size=num_random)
            random_labs[random_labs == 0] = -1
            print("**** Random Img Gen... Label_0:", np.count_nonzero(random_labs == -1))
            print("**** Random Img Gen... Label_1:", np.count_nonzero(random_labs == 1))
            #random_labs = torch.unsqueeze(Variable(cuda(torch.Tensor(random_labs), self.use_cuda)),1)
            #rand_z = torch.cat([random_z, random_labs], 1)
            #print("RAND_Z:", rand_z.size())
            recon_random = torch.sigmoid(self.net._decode(random_z, random_labs)).data
        else:
            recon_random = torch.sigmoid(self.net._decode(random_z)).data
        count = 0
        output_dir = os.path.join(self.output_dir,'step_'+str(self.global_iter),'random_gen')
        os.makedirs(output_dir, exist_ok=True)
        for img in recon_random:
            count += 1
            if self.condition:
                save_image(tensor=img.cpu(), filename=os.path.join(output_dir, 'random_step_'+str(self.global_iter)+'_label_'+str(int(random_labs[count-1]))+'_'+str(count)+'.png'), pad_value=0)
            else:
                save_image(tensor=img.cpu(), filename=os.path.join(output_dir, 'random_step_'+str(self.global_iter)+'_'+str(count)+'.png'), pad_value=0)
        print("Saved Random Image Generations")
        torch.manual_seed(self.seed)
        torch.cuda.manual_seed(self.seed)
        np.random.seed(self.seed)
        self.net_mode(train=True)

    def random_img_traverse(self, num_random=20, limit=3, inter=2/3, loc=-1):
        self.net_mode(train=False)
        #torch.seed()
        #torch.cuda.seed()
        seed()
        cuda_seed()
        np.random.seed()

        decoder = self.net.decoder
        encoder = self.net.encoder
        interpolation = torch.arange(-limit, limit+0.1, inter)

        random_zs = []
        random_labs = []
        #counts_0 = 0
        #counts_1 = 0
        for _ in range(num_random):
            random_zs.append(Variable(cuda(torch.randn(1, self.z_dim), self.use_cuda), volatile=True))
        if self.condition:
            #lab = np.random.randint(2)#torch.round(torch.rand(1))
            random_labs = np.random.randint(2, size=num_random)
            random_labs[random_labs == 0] = -1
            #if lab == 0:
            #    counts_0 += 1
            #else:
            #    counts_1 += 1
            #random_labs.append(lab)
        #print(random_labs)
        print("**** Random Img Traverse... Label_0:", np.count_nonzero(random_labs == -1))
        print("**** Random Img Traverse... Label_1:", np.count_nonzero(random_labs == 1))

        count = 0
        output_dir = os.path.join(self.output_dir,'step_'+str(self.global_iter),'random_traverse')
        os.makedirs(output_dir, exist_ok=True)

        zrange = (self.dim_klds > 0.05).nonzero()[0]
        print(zrange)
        #for random_z in random_zs:
        for i in range(len(random_zs)):
            random_z = random_zs[i]
            count += 1
            z_ori = random_z
            for row in zrange:
                if loc != -1 and row != loc:
                    continue
                z = z_ori.clone()
                for val in interpolation:
                    z[:, row] = val
                    if self.condition:
                        lab = float(random_labs[i])
                        lab = np.expand_dims(lab, 0)
                        #print("Yo Here:", z.size(), lab.shape)
                        sample = torch.sigmoid(self.net._decode(z, lab)).data
                        save_image(tensor=sample.cpu(), filename=os.path.join(output_dir, 'random_step_'+str(self.global_iter)+'_z_'+str(count)+'_lab_'+str(int(lab))+'_dim_'+str(row)+'_val_{:.2}'.format(val)+'.png'), pad_value=0)
                    else:
                        sample = torch.sigmoid(decoder(z)).data
                        save_image(tensor=sample.cpu(), filename=os.path.join(output_dir, 'random_step_'+str(self.global_iter)+'_z_'+str(count)+'_dim_'+str(row)+'_val_{:.2}'.format(val)+'.png'), pad_value=0)
        print("Done Traversal Image Saving!")
        torch.manual_seed(self.seed)
        torch.cuda.manual_seed(self.seed)
        np.random.seed(self.seed)
        self.net_mode(train=True)


    def audit_viz_reconstruction(self):
        #self.net_mode(train=False)
        imgs = self.gather.data['images']
        recons = self.gather.data['recons']
        #imgs1= self.gather.data['images1']
        #recons1 = self.gather.data['recons1']
        #total_kld = self.gather.data['total_kld']
        #total_kld1 = self.gather.data['total_kld1']
        #probs = self.gather.data['prob']
        #probs1 = self.gather.data['prob1']
        if test_ayan:
            z_var = cuda(torch.zeros(1, self.z_dim), self.use_cuda)
            z_var[0,1] = -3.0
            #z_var[0,10] = 3.0
            #z_var[0,17] = -3.0
            #z_var[0,15] = -3.0
            #z_var[0,11] = 3.0
            z_var[0,26] = -3.0
            #z_var[0,28] = 3.0
            #z_var[0,29] = 3.0
            #z_var[0,11] = 3.0
            #print("Here:", z_var)
            #z_var[0,29] = -1.66
            z_var = Variable(z_var, volatile=True)
            recon_z = torch.sigmoid(self.net.decoder(z_var)).data
            #rec_z = make_grid(recon_z, normalize=True)
            self.viz.images(recon_z, env=self.viz_name+'_reconstructions', opts=dict(title=str(self.global_iter)+"_random_gen_ayan"), nrow=10)
            return


        if save_random:
            # Generate and save random images
            random_z = Variable(cuda(torch.randn(200, self.z_dim), self.use_cuda), volatile=True)
            recon_random = torch.sigmoid(self.net.decoder(random_z)).data
            count = 0
            output_dir = os.path.join(self.output_dir,'random_generate')
            os.makedirs(output_dir, exist_ok=True)
            for img in recon_random:
                count += 1
                save_image(tensor=img.cpu(), filename=os.path.join(output_dir, 'random_'+str(count)+'.png'), pad_value=0)
            '''save_image(tensor=gifs[i][j].cpu(),
                filename=os.path.join(output_dir, '{}_{}.jpg'.format(key, j)),
                nrow=self.z_dim, pad_value=1)'''
            print("Saved Randoms")
            return



        if SIMPLE_TEST:

            print("HERE!")

            classes = self.gather.data['classes']

            for i in range(len(classes)):
                img = imgs[i]
                #print(len(img))
                #print(img[0].size())
                rec = recons[i]
                #for m in img:
                #    print(m.size())
                img = make_grid(img, normalize=True)
                rec = make_grid(rec, normalize=True)
                i_r = torch.stack([img, rec], dim=0).cpu()
                self.viz.images(i_r, env=self.viz_name+'_audit_reconstructions',\
                        opts=dict(title="Reconstructions "+str(classes[i])), nrow=8)

            #self.viz.images(i_0, env=self.viz_name+'_reconstructions',
            #            opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr), nrow=100)
            #self.viz.images(i_1, env=self.viz_name+'_reconstructions',
            #            opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr), nrow=100)
            return



        x = imgs[:100]
        x = make_grid(x, normalize=True)
        #x_recon = self.gather.data['images'][1][rand_idx]
        x_recon = recons[:100]
        x_recon = make_grid(x_recon, normalize=True)
        random_z = Variable(cuda(torch.randn(200, self.z_dim), self.use_cuda), volatile=True)
        recon_random = torch.sigmoid(self.net.decoder(random_z)).data
        rand_recon = make_grid(recon_random, normalize=True).cpu()
        images = torch.stack([x, x_recon], dim=0).cpu()
        self.viz.images(images, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr), nrow=10)

        x1 = imgs1[:100]
        x1 = make_grid(x1, normalize=True)
        x1_recon = recons1[:100]
        x1_recon = make_grid(x1_recon, normalize=True)
        images1 = torch.stack([x1, x1_recon], dim=0).cpu()
        self.viz.images(images1, env=self.viz_name+'_reconstructions',
                opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr), nrow=10)

        total_kld, total_kld1 = np.array(total_kld), np.array(total_kld1)
        # Get max 10 ids
        max_ids = total_kld.argsort()[::-1][:10]
        max_ids1 = total_kld1.argsort()[::-1][:10]

        #print("ASSERT HERE:", total_kld[max_ids])

        # Get min 10 ids
        min_ids = total_kld.argsort()[:10]
        min_ids1 = total_kld1.argsort()[:10]

        #print("ASSERT HERE:", total_kld[min_ids])

        x_max, x1_max, x_min, x1_min = [], [], [], []
        recon_max, recon1_max, recon_min, recon1_min = [], [], [], []

        for idx in max_ids:
            x_max.append(imgs[idx])
            recon_max.append(recons[idx])

        x_max = make_grid(x_max, normalize=True)
        recon_max = make_grid(recon_max, normalize=True)
        max_imgs = torch.stack([x_max, recon_max], dim=0).cpu()

        for idx in max_ids1:
            x1_max.append(imgs1[idx])
            recon1_max.append(recons1[idx])

        x1_max = make_grid(x1_max, normalize=True)
        recon1_max = make_grid(recon1_max, normalize=True)
        max1_imgs = torch.stack([x1_max, recon1_max], dim=0).cpu()

        for idx in min_ids:
            x_min.append(imgs[idx])
            recon_min.append(recons[idx])

        x_min = make_grid(x_min, normalize=True)
        recon_min = make_grid(recon_min, normalize=True)
        min_imgs = torch.stack([x_min, recon_min], dim=0).cpu()

        for idx in min_ids1:
            x1_min.append(imgs1[idx])
            recon1_min.append(recons1[idx])

        x1_min = make_grid(x1_min, normalize=True)
        recon1_min = make_grid(recon1_min, normalize=True)
        min1_imgs = torch.stack([x1_min, recon1_min], dim=0).cpu()

        self.viz.images(max_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr+'_max_kld'), nrow=1)
        self.viz.images(min_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr+'_min_kld'), nrow=1)
        self.viz.images(max1_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr+'_max_kld'), nrow=1)
        self.viz.images(min1_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr+'_min_kld'), nrow=1)



        #####################################
        # Now we do for the probability values
        #####################################
        probs, probs1 = np.array(probs), np.array(probs1)
        print(len(probs))
        print(len(probs1))
        # Get max 10 ids
        max_ids = probs.argsort()[::-1][:10]
        max_ids1 = probs1.argsort()[::-1][:10]

        #print("ASSERT HERE:", total_kld[max_ids])

        # Get min 10 ids
        min_ids = probs.argsort()[:10]
        min_ids1 = probs1.argsort()[:10]

        #print("ASSERT HERE:", total_kld[min_ids])

        x_max, x1_max, x_min, x1_min = [], [], [], []
        recon_max, recon1_max, recon_min, recon1_min = [], [], [], []

        for idx in max_ids:
            x_max.append(imgs[idx])
            recon_max.append(recons[idx])

        x_max = make_grid(x_max, normalize=True)
        recon_max = make_grid(recon_max, normalize=True)
        max_imgs = torch.stack([x_max, recon_max], dim=0).cpu()

        for idx in max_ids1:
            x1_max.append(imgs1[idx])
            recon1_max.append(recons1[idx])

        x1_max = make_grid(x1_max, normalize=True)
        recon1_max = make_grid(recon1_max, normalize=True)
        max1_imgs = torch.stack([x1_max, recon1_max], dim=0).cpu()

        for idx in min_ids:
            x_min.append(imgs[idx])
            recon_min.append(recons[idx])

        x_min = make_grid(x_min, normalize=True)
        recon_min = make_grid(recon_min, normalize=True)
        min_imgs = torch.stack([x_min, recon_min], dim=0).cpu()

        for idx in min_ids1:
            x1_min.append(imgs1[idx])
            recon1_min.append(recons1[idx])

        x1_min = make_grid(x1_min, normalize=True)
        recon1_min = make_grid(recon1_min, normalize=True)
        min1_imgs = torch.stack([x1_min, recon1_min], dim=0).cpu()

        self.viz.images(max_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr+'_max_probs'), nrow=1)
        self.viz.images(min_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr+'_min_probs'), nrow=1)
        self.viz.images(max1_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr+'_max_probs'), nrow=1)
        self.viz.images(min1_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr+'_min_probs'), nrow=1)


        self.viz.images(rand_recon, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+"_random_gen"), nrow=10)



    def test_viz_reconstruction(self):
        #self.net_mode(train=False)
        imgs = self.gather.data['images']
        recons = self.gather.data['recons']
        imgs1= self.gather.data['images1']
        recons1 = self.gather.data['recons1']
        total_kld = self.gather.data['total_kld']
        total_kld1 = self.gather.data['total_kld1']
        probs = self.gather.data['prob']
        probs1 = self.gather.data['prob1']

        var = self.gather.data['total_var']
        var1 = self.gather.data['total_var1']

        if SIMPLE_TEST:
            max_ids_0 = np.array(probs).argsort()[::-1]
            max_ids_1 = np.array(probs1).argsort()[::-1]
            loss0 = self.gather.data['recon_loss']
            loss1 = self.gather.data['recon_loss1']
            x0, r0, x1, r1 = [], [], [], []
            for ids in max_ids_0:
                x0.append(imgs[ids])
                r0.append(recons[ids])
            for ids in max_ids_1:
                x1.append(imgs1[ids])
                r1.append(recons1[ids])

            loss0, loss1 = np.array(loss0), np.array(loss1)
            loss0, loss1 = loss0[max_ids_0], loss1[max_ids_1]
            loss0, loss1 = np.round(loss0, 2), np.round(loss1, 2)
            x0 = make_grid(x0, normalize=True)
            r0 = make_grid(r0, normalize=True)
            i_0 = torch.stack([x0, r0], dim=0).cpu()
            x1 = make_grid(x1, normalize=True)
            r1 = make_grid(r1, normalize=True)
            i_1 = torch.stack([x1, r1], dim=0).cpu()

            print("######################################################")
            print("LOSS_(-1):")
            print(loss0[max_ids_0])
            print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
            print("LOSS_(+1):")
            print(loss1[max_ids_1])

            self.viz.images(i_0, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr), nrow=100)
            self.viz.images(i_1, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr), nrow=100)
            return



        x = imgs[:100]
        x = make_grid(x, normalize=True)
        #x_recon = self.gather.data['images'][1][rand_idx]
        x_recon = recons[:100]
        x_recon = make_grid(x_recon, normalize=True)
        random_z = Variable(cuda(torch.randn(200, self.z_dim), self.use_cuda), volatile=True)
        recon_random = torch.sigmoid(self.net.decoder(random_z)).data
        rand_recon = make_grid(recon_random, normalize=True).cpu()
        images = torch.stack([x, x_recon], dim=0).cpu()
        self.viz.images(images, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr), nrow=10)

        x1 = imgs1[:100]
        x1 = make_grid(x1, normalize=True)
        x1_recon = recons1[:100]
        x1_recon = make_grid(x1_recon, normalize=True)
        images1 = torch.stack([x1, x1_recon], dim=0).cpu()
        self.viz.images(images1, env=self.viz_name+'_reconstructions',
                opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr), nrow=10)

        total_kld, total_kld1 = np.array(total_kld), np.array(total_kld1)
        # Get max 10 ids
        max_ids = total_kld.argsort()[::-1][:10]
        max_ids1 = total_kld1.argsort()[::-1][:10]

        #print("ASSERT HERE:", total_kld[max_ids])

        # Get min 10 ids
        min_ids = total_kld.argsort()[:10]
        min_ids1 = total_kld1.argsort()[:10]

        #print("ASSERT HERE:", total_kld[min_ids])

        x_max, x1_max, x_min, x1_min = [], [], [], []
        recon_max, recon1_max, recon_min, recon1_min = [], [], [], []

        for idx in max_ids:
            x_max.append(imgs[idx])
            recon_max.append(recons[idx])

        x_max = make_grid(x_max, normalize=True)
        recon_max = make_grid(recon_max, normalize=True)
        max_imgs = torch.stack([x_max, recon_max], dim=0).cpu()

        for idx in max_ids1:
            x1_max.append(imgs1[idx])
            recon1_max.append(recons1[idx])

        x1_max = make_grid(x1_max, normalize=True)
        recon1_max = make_grid(recon1_max, normalize=True)
        max1_imgs = torch.stack([x1_max, recon1_max], dim=0).cpu()

        for idx in min_ids:
            x_min.append(imgs[idx])
            recon_min.append(recons[idx])

        x_min = make_grid(x_min, normalize=True)
        recon_min = make_grid(recon_min, normalize=True)
        min_imgs = torch.stack([x_min, recon_min], dim=0).cpu()

        for idx in min_ids1:
            x1_min.append(imgs1[idx])
            recon1_min.append(recons1[idx])

        x1_min = make_grid(x1_min, normalize=True)
        recon1_min = make_grid(recon1_min, normalize=True)
        min1_imgs = torch.stack([x1_min, recon1_min], dim=0).cpu()

        self.viz.images(max_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr+'_max_kld'), nrow=1)
        self.viz.images(min_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr+'_min_kld'), nrow=1)
        self.viz.images(max1_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr+'_max_kld'), nrow=1)
        self.viz.images(min1_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr+'_min_kld'), nrow=1)



        #####################################
        # Now we do for the probability values
        #####################################
        probs, probs1 = np.array(probs), np.array(probs1)
        print(len(probs))
        print(len(probs1))
        # Get max 10 ids
        max_ids = probs.argsort()[::-1][:10]
        max_ids1 = probs1.argsort()[::-1][:10]

        #print("ASSERT HERE:", total_kld[max_ids])

        # Get min 10 ids
        min_ids = probs.argsort()[:10]
        min_ids1 = probs1.argsort()[:10]

        #print("ASSERT HERE:", total_kld[min_ids])

        x_max, x1_max, x_min, x1_min = [], [], [], []
        recon_max, recon1_max, recon_min, recon1_min = [], [], [], []

        for idx in max_ids:
            x_max.append(imgs[idx])
            recon_max.append(recons[idx])

        x_max = make_grid(x_max, normalize=True)
        recon_max = make_grid(recon_max, normalize=True)
        max_imgs = torch.stack([x_max, recon_max], dim=0).cpu()

        for idx in max_ids1:
            x1_max.append(imgs1[idx])
            recon1_max.append(recons1[idx])

        x1_max = make_grid(x1_max, normalize=True)
        recon1_max = make_grid(recon1_max, normalize=True)
        max1_imgs = torch.stack([x1_max, recon1_max], dim=0).cpu()

        for idx in min_ids:
            x_min.append(imgs[idx])
            recon_min.append(recons[idx])

        x_min = make_grid(x_min, normalize=True)
        recon_min = make_grid(recon_min, normalize=True)
        min_imgs = torch.stack([x_min, recon_min], dim=0).cpu()

        for idx in min_ids1:
            x1_min.append(imgs1[idx])
            recon1_min.append(recons1[idx])

        x1_min = make_grid(x1_min, normalize=True)
        recon1_min = make_grid(recon1_min, normalize=True)
        min1_imgs = torch.stack([x1_min, recon1_min], dim=0).cpu()

        self.viz.images(max_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr+'_max_probs'), nrow=1)
        self.viz.images(min_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr+'_min_probs'), nrow=1)
        self.viz.images(max1_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr+'_max_probs'), nrow=1)
        self.viz.images(min1_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr+'_min_probs'), nrow=1)

        ###########################################################
        # ******************************************************* #
        ##########################################################
        #####################################
        # Now we do for the log variance values
        #####################################
        var, var1 = np.array(var), np.array(var1)
        print(len(var))
        print(len(var1))
        # Get max 10 ids
        max_ids = var.argsort()[::-1][:10]
        max_ids1 = var1.argsort()[::-1][:10]

        #print("ASSERT HERE:", total_kld[max_ids])

        # Get min 10 ids
        min_ids = var.argsort()[:10]
        min_ids1 = var1.argsort()[:10]

        #print("ASSERT HERE:", total_kld[min_ids])

        x_max, x1_max, x_min, x1_min = [], [], [], []
        recon_max, recon1_max, recon_min, recon1_min = [], [], [], []

        for idx in max_ids:
            x_max.append(imgs[idx])
            recon_max.append(recons[idx])

        x_max = make_grid(x_max, normalize=True)
        recon_max = make_grid(recon_max, normalize=True)
        max_imgs = torch.stack([x_max, recon_max], dim=0).cpu()

        for idx in max_ids1:
            x1_max.append(imgs1[idx])
            recon1_max.append(recons1[idx])

        x1_max = make_grid(x1_max, normalize=True)
        recon1_max = make_grid(recon1_max, normalize=True)
        max1_imgs = torch.stack([x1_max, recon1_max], dim=0).cpu()

        for idx in min_ids:
            x_min.append(imgs[idx])
            recon_min.append(recons[idx])

        x_min = make_grid(x_min, normalize=True)
        recon_min = make_grid(recon_min, normalize=True)
        min_imgs = torch.stack([x_min, recon_min], dim=0).cpu()

        for idx in min_ids1:
            x1_min.append(imgs1[idx])
            recon1_min.append(recons1[idx])

        x1_min = make_grid(x1_min, normalize=True)
        recon1_min = make_grid(recon1_min, normalize=True)
        min1_imgs = torch.stack([x1_min, recon1_min], dim=0).cpu()

        self.viz.images(max_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr+'_max_logvar'), nrow=1)
        self.viz.images(min_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(-1)_'+self.attr+'_min_logvar'), nrow=1)
        self.viz.images(max1_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr+'_max_logvar'), nrow=1)
        self.viz.images(min1_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+'_(+1)_'+self.attr+'_min_logvar'), nrow=1)


        # *********************************** #
        # * Now we want to see some special * #
        # * cases. When z is all 0, all 3,  * #
        # * all -3 and so on.               * #
        # *********************************** #
        z_min3 = Variable(cuda((-3) * torch.ones(self.z_dim), self.use_cuda), volatile=True)
        recon_min3 = torch.sigmoid(self.net.decoder(z_min3)).data
        #min3_recon = make_grid(recon_min3, normalize=True).cpu()

        z_min2 = Variable(cuda((-2) * torch.ones(self.z_dim), self.use_cuda), volatile=True)
        recon_min2 = torch.sigmoid(self.net.decoder(z_min2)).data
        #min2_recon = make_grid(recon_min2, normalize=True).cpu()

        z_min1 = Variable(cuda((-1) * torch.ones(self.z_dim), self.use_cuda), volatile=True)
        recon_min1 = torch.sigmoid(self.net.decoder(z_min1)).data
        #min1_recon = make_grid(recon_min1, normalize=True).cpu()

        z_zeros = Variable(cuda(torch.zeros(self.z_dim), self.use_cuda), volatile=True)
        recon_zeros = torch.sigmoid(self.net.decoder(z_zeros)).data
        #zeros_recon = make_grid(recon_zeros, normalize=True).cpu()

        z_plus1 = Variable(cuda(torch.ones(self.z_dim), self.use_cuda), volatile=True)
        recon_plus1 = torch.sigmoid(self.net.decoder(z_plus1)).data
        #plus1_recon = make_grid(recon_plus1, normalize=True).cpu()

        z_plus2 = Variable(cuda((2) * torch.ones(self.z_dim), self.use_cuda), volatile=True)
        recon_plus2 = torch.sigmoid(self.net.decoder(z_plus2)).data
        #plus2_recon = make_grid(recon_plus2, normalize=True).cpu()

        z_plus3 = Variable(cuda((3) * torch.ones(self.z_dim), self.use_cuda), volatile=True)
        recon_plus3 = torch.sigmoid(self.net.decoder(z_plus3)).data
        #plus3_recon = make_grid(recon_plus3, normalize=True).cpu()


        #print("^%^%^%^%^%^%^%")
        #print(recon_min3.size(), recon_min2.size(), recon_min1.size(), recon_zeros.size(), recon_plus1.size(), recon_plus2.size(), recon_plus3.size())
        spec_imgs = make_grid([recon_min3.squeeze(), recon_min2.squeeze(), recon_min1.squeeze(), recon_zeros.squeeze(), recon_plus1.squeeze(), \
                recon_plus2.squeeze(), recon_plus3.squeeze()], normalize=True).cpu()
        #spec_imgs = torch.stack([min3_recon, min2_recon, min1_recon, zeros_recon, plus1_recon, plus2_recon, plus3_recon], dim=1).cpu()

        self.viz.images(spec_imgs, env=self.viz_name+'_reconstructions',
                        opts=dict(title=str(self.global_iter)+"_vary_z_(-3_to_3)"), nrow=1)


        #self.net_mode(train=True)
        # Now the random images
        self.viz.images(rand_recon, env=self.viz_name+'_reconstructions', opts=dict(title=str(self.global_iter)+"_random_gen"), nrow=10)

    def viz_reconstruction(self):
        self.net_mode(train=False)
        x = self.gather.data['images'][0][:100]
        x = make_grid(x, normalize=True)
        x_recon = self.gather.data['images'][1][:100]
        x_recon = make_grid(x_recon, normalize=True)
        images = torch.stack([x, x_recon], dim=0).cpu()
        self.viz.images(images, env=self.viz_name+'_reconstruction',
                        opts=dict(title=str(self.global_iter)), nrow=10)
        self.net_mode(train=True)

    def viz_lines(self):
        self.net_mode(train=False)
        recon_losses = torch.stack(self.gather.data['recon_loss']).cpu()

        mus = torch.stack(self.gather.data['mu']).cpu()
        vars = torch.stack(self.gather.data['var']).cpu()

        dim_wise_klds = torch.stack(self.gather.data['dim_wise_kld'])
        mean_klds = torch.stack(self.gather.data['mean_kld'])
        total_klds = torch.stack(self.gather.data['total_kld'])
        klds = torch.cat([dim_wise_klds, mean_klds, total_klds], 1).cpu()
        iters = torch.Tensor(self.gather.data['iter'])

        legend = []
        for z_j in range(self.z_dim):
            legend.append('z_{}'.format(z_j))
        legend.append('mean')
        legend.append('total')

        if self.win_recon is None:
            self.win_recon = self.viz.line(
                                        X=iters,
                                        Y=recon_losses,
                                        env=self.viz_name+'_lines',
                                        opts=dict(
                                            width=400,
                                            height=400,
                                            xlabel='iteration',
                                            title='reconsturction loss',))
        else:
            self.win_recon = self.viz.line(
                                        X=iters,
                                        Y=recon_losses,
                                        env=self.viz_name+'_lines',
                                        win=self.win_recon,
                                        update='append',
                                        opts=dict(
                                            width=400,
                                            height=400,
                                            xlabel='iteration',
                                            title='reconsturction loss',))

        if self.win_kld is None:
            self.win_kld = self.viz.line(
                                        X=iters,
                                        Y=klds,
                                        env=self.viz_name+'_lines',
                                        opts=dict(
                                            width=400,
                                            height=400,
                                            legend=legend,
                                            xlabel='iteration',
                                            title='kl divergence',))
        else:
            self.win_kld = self.viz.line(
                                        X=iters,
                                        Y=klds,
                                        env=self.viz_name+'_lines',
                                        win=self.win_kld,
                                        update='append',
                                        opts=dict(
                                            width=400,
                                            height=400,
                                            legend=legend,
                                            xlabel='iteration',
                                            title='kl divergence',))

        if self.win_mu is None:
            self.win_mu = self.viz.line(
                                        X=iters,
                                        Y=mus,
                                        env=self.viz_name+'_lines',
                                        opts=dict(
                                            width=400,
                                            height=400,
                                            legend=legend[:self.z_dim],
                                            xlabel='iteration',
                                            title='posterior mean',))
        else:
            self.win_mu = self.viz.line(
                                        X=iters,
                                        Y=vars,
                                        env=self.viz_name+'_lines',
                                        win=self.win_mu,
                                        update='append',
                                        opts=dict(
                                            width=400,
                                            height=400,
                                            legend=legend[:self.z_dim],
                                            xlabel='iteration',
                                            title='posterior mean',))

        if self.win_var is None:
            self.win_var = self.viz.line(
                                        X=iters,
                                        Y=vars,
                                        env=self.viz_name+'_lines',
                                        opts=dict(
                                            width=400,
                                            height=400,
                                            legend=legend[:self.z_dim],
                                            xlabel='iteration',
                                            title='posterior variance',))
        else:
            self.win_var = self.viz.line(
                                        X=iters,
                                        Y=vars,
                                        env=self.viz_name+'_lines',
                                        win=self.win_var,
                                        update='append',
                                        opts=dict(
                                            width=400,
                                            height=400,
                                            legend=legend[:self.z_dim],
                                            xlabel='iteration',
                                            title='posterior variance',))
        self.net_mode(train=True)



####################################
# NEW TEST FUNCTION
####################################
    def test_viz_traverse(self, limit=3, inter=2/3, loc=-1, num_rands=10):
        self.net_mode(train=False)
        import random

        decoder = self.net.decoder
        encoder = self.net.encoder
        interpolation = torch.arange(-limit, limit+0.1, inter)

        n_dsets = len(self.data_loader.dataset)
        rand_idx = random.randint(1, n_dsets-1)


        random_zs = []
        for i in range(num_rands):
            random_zs.append(Variable(cuda(torch.randn(1, self.z_dim), self.use_cuda), volatile=True))
        #random_z = Variable(cuda(torch.rand(1, self.z_dim), self.use_cuda), volatile=True)
        fixed_z = Variable(cuda(torch.zeros(1, self.z_dim), self.use_cuda), volatile=True)
        # Save the original images first
        #random_z = Variable(cuda(torch.randn(200, self.z_dim), self.use_cuda), volatile=True)
        recon_fixed_z = torch.sigmoid(self.net.decoder(fixed_z)).data
        recon_randoms = []
        for random_z in random_zs:
            recon_randoms.append(torch.sigmoid(self.net.decoder(random_z)).data)
        count = 34
        output_dir = os.path.join(self.output_dir,'random_traverse_generate')
        os.makedirs(output_dir, exist_ok=True)
        # First save the fixed z.
        #save_image(tensor=recon_fixed_z.cpu(), filename=os.path.join(output_dir, 'fixed_z_0.png'), pad_value=0)

        for img in recon_randoms:
            count += 1
            save_image(tensor=img.cpu(), filename=os.path.join(output_dir, 'random_z_'+str(count)+'_0.png'), pad_value=0)


        #if 'celeba' in self.dataset:
        #    fixed_idx = 0
        #    fixed_img = self.data_loader.dataset.__getitem__(fixed_idx)
        #    print(fixed_img)
        #    fixed_img = Variable(cuda(fixed_img[0], self.use_cuda), volatile=True).unsqueeze(0)
        #    fixed_img_z = encoder(fixed_img)[:, :self.z_dim]
            #Z = {'fixed_img':fixed_img_z, 'random_z':random_z, 'fixed_z':fixed_z}
        Z = {'fixed_z':fixed_z, 'random_zs':random_zs}
        gifs = []
        for key in Z.keys():
            if key == 'fixed_z':
                z_ori = Z[key]
                samples = []
                #if z_order is None:
                #zrange = range(self.z_dim)
                zrange = [9,29,15,10,26,1,0,3,28,17,11,24,21,18,6,23,2]
                #else:
                #zrange = z_order
                for row in zrange:
                    if loc != -1 and row != loc:
                        continue
                    z = z_ori.clone()
                    for val in interpolation:
                        z[:, row] = val
                        if key == 'fixed_z' and row == 1:
                            #print("NOW:", z)
                            pass
                        try:
                            sample = torch.sigmoid(decoder(z)).data
                        except Exception as e:
                            print(key)
                            raise(e)
                        #samples.append(sample)
                        #gifs.append(sample)
                        #save_image(tensor=sample.cpu(), filename=os.path.join(output_dir, 'fixed_z_dim_'+str(row)+'_val_{:.2}'.format(val)+'.png'), pad_value=0)
            elif key == 'random_zs':
                count = 34
                zrange = [9,29,15,10,26,1,0,3,28,17,11,24,21,18,6,23,2]
                for random_z in Z[key]:
                    count += 1
                    z_ori = random_z
                    samples = []
                    #if z_order is None:
                    #zrange = range(self.z_dim)
                    #else:
                    #zrange = z_order
                    for row in zrange:
                        if loc != -1 and row != loc:
                            continue
                        z = z_ori.clone()
                        for val in interpolation:
                            z[:, row] = val
                            try:
                                sample = torch.sigmoid(decoder(z)).data
                            except Exception as e:
                                print(key)
                                raise(e)
                            #samples.append(sample)
                            #gifs.append(sample)
                            save_image(tensor=sample.cpu(), filename=os.path.join(output_dir, 'random_z_'+str(count)+'_dim_'+str(row)+'_val_{:.2}'.format(val)+'.png'), pad_value=0)
        print("DONE!!!")




    def viz_traverse(self, limit=3, inter=2/3, loc=-1, C=None, is_train=True, z_order=None):
        self.net_mode(train=False)
        import random

        decoder = self.net.decoder
        encoder = self.net.encoder
        interpolation = torch.arange(-limit, limit+0.1, inter)


        #dim_wise_klds = self.gather.data['dim_wise_kld'].cpu().numpy()
        #sorted_z_idx = np.argsort(dim_wise_klds)[::-1]


        n_dsets = len(self.data_loader.dataset)
        rand_idx = random.randint(1, n_dsets-1)

        if is_train:
            if self.condition:
                random_img, _, rand_label = self.data_loader.dataset.__getitem__(rand_idx)
                label = list(map(rand_label.__getitem__, self.attr))
                #print(label)
                #rand_label = [[float(label[0][i]), float(label[1][i])] for i in range(len(label[0]))]
                rand_label = [[float(x) for x in label]]
                #rand_label = rand_label[self.attr]
                #for i in range(len(rand_label)):

                #rand_label = list(np.expand_dims(max(min(float(rand_label), 1), -1), axis=1))
            else:
                random_img, _, rand_label = self.data_loader.dataset.__getitem__(rand_idx)
            random_img = Variable(cuda(random_img, self.use_cuda), volatile=True).unsqueeze(0)
            if self.dataset == 'dsprites':
                random_img = random_img.repeat(1,3,1,1)
            if self.condition:
                random_img_z = self.net._encode(random_img, rand_label)[:, :self.z_dim]
            else:
                random_img_z = encoder(random_img)[:, :self.z_dim]

        #if self.condition:
            #random_z = Variable(cuda(torch.rand(1, self.z_dim+1), self.use_cuda), volatile=True)
            #random_z_0 = random_z[0][self.z_dim] = 0.0
            #random_z_1 = random_z[0][self.z_dim] = 1.0
        #else:
        random_z = Variable(cuda(torch.randn(1, self.z_dim), self.use_cuda), volatile=True)

        #if self.condition:
        #    fixed_z = Variable(cuda(torch.zeros(1, self.z_dim+1), self.use_cuda), volatile=True)
        #    fixed_z_0 = fixed_z[0][self.z_dim] = 0.0
        #    fixed_z_1 = fixed_z[0][self.z_dim] = 1.0
        #else:
        fixed_z = Variable(cuda(torch.zeros(1, self.z_dim), self.use_cuda), volatile=True)

        if is_train:
            if self.dataset == 'dsprites':
                fixed_idx1 = 87040 # square
                fixed_idx2 = 332800 # ellipse
                fixed_idx3 = 578560 # heart

                fixed_img1 = self.data_loader.dataset.__getitem__(fixed_idx1)
                fixed_img1 = Variable(cuda(fixed_img1, self.use_cuda), volatile=True).unsqueeze(0)
                fixed_img1 = fixed_img1.repeat(1,3,1,1)
                fixed_img_z1 = encoder(fixed_img1)[:, :self.z_dim]

                fixed_img2 = self.data_loader.dataset.__getitem__(fixed_idx2)
                fixed_img2 = Variable(cuda(fixed_img2, self.use_cuda), volatile=True).unsqueeze(0)
                fixed_img2 = fixed_img2.repeat(1,3,1,1)
                fixed_img_z2 = encoder(fixed_img2)[:, :self.z_dim]

                fixed_img3 = self.data_loader.dataset.__getitem__(fixed_idx3)
                fixed_img3 = Variable(cuda(fixed_img3, self.use_cuda), volatile=True).unsqueeze(0)
                fixed_img3 = fixed_img3.repeat(1,3,1,1)
                fixed_img_z3 = encoder(fixed_img3)[:, :self.z_dim]

                color = np.repeat(np.repeat(np.random.uniform(0.5,1,[1,3,1,1]),\
                        fixed_img3.shape[2], axis=2),\
                        fixed_img3.shape[3], axis=3)
                color = Variable(cuda(torch.from_numpy(color).float(), self.use_cuda))
                color_img = fixed_img3 * color
                color_img_z = encoder(color_img)[:, :self.z_dim]

                Z = {'fixed_square':fixed_img_z1, 'fixed_ellipse':fixed_img_z2,
                        'fixed_heart':fixed_img_z3, 'color_img':color_img_z,\
                                'random_img':random_img_z}
            else:
                fixed_idx = 0
                if self.condition:
                    fixed_img, _, fixed_label = self.data_loader.dataset.__getitem__(fixed_idx)
                    label = list(map(fixed_label.__getitem__, self.attr))
                    #fixed_label = [[float(label[0][i]), float(label[1][i])] for i in range(len(label[0]))]
                    fixed_label = [[float(x) for x in label]]

                    #fixed_label = fixed_label[self.attr]
                    #for i in range(len(fixed_label)):

                    #fixed_label = list(np.expand_dims(max(min(float(fixed_label), 1), -1), axis=1))
                    #print("FIXED:", fixed_label)
                else:
                    fixed_img, _, fixed_label = self.data_loader.dataset.__getitem__(fixed_idx)
                fixed_img = Variable(cuda(fixed_img, self.use_cuda), volatile=True).unsqueeze(0)
                if self.condition:
                    #fixed_img_z = encoder(fixed_img, fixed_label)[:, :self.z_dim]
                    fixed_img_z = self.net._encode(fixed_img, fixed_label)[:, :self.z_dim]
                else:
                    fixed_img_z = encoder(fixed_img)[:, :self.z_dim]



        if 'celeba' in self.dataset:
            if is_train:
                if self.condition:
                    #Z = {'fixed_img':fixed_img_z, 'random_img':random_img_z, 'random_z_0':random_z, 'random_z_1': random_z, 'fixed_z_0':fixed_z, 'fixed_z_1': fixed_z}
                    Z = {'fixed_img':fixed_img_z, 'random_img':random_img_z}
                    from itertools import product
                    combs = list(product(range(2), repeat=self.attr_size))
                    combs = ["".join(tuple(map(str,tup))) for tup in combs]
                    for comb in combs:
                        Z["random_z_"+comb] = random_z
                        Z["fixed_z_"+comb] = fixed_z
                else:
                    Z = {'fixed_img':fixed_img_z, 'random_img':random_img_z, 'random_z':random_z, 'fixed_z':fixed_z}
            else:
                fixed_idx = 0
                fixed_img = self.data_loader.dataset.__getitem__(fixed_idx)
                print(fixed_img)
                fixed_img = Variable(cuda(fixed_img[0], self.use_cuda), volatile=True).unsqueeze(0)
                fixed_img_z = encoder(fixed_img)[:, :self.z_dim]
                Z = {'fixed_img':fixed_img_z, 'random_z':random_z, 'fixed_z':fixed_z}

        gifs = []
        for key in Z.keys():
            z_ori = Z[key]
            samples = []
            if z_order is None:
                # CHANGE HERE
                zrange = range(self.z_dim)
            else:
                zrange = z_order
            if self.condition:
                if key == 'fixed_img':
                    lab = fixed_label
                elif key == 'random_img':
                    lab_ = rand_label
                #elif key.endswith('_0'):
                #    lab = [-1]
                #elif key.endswith('_1'):
                #    lab = [1]
                elif key.startswith('random_z') or key.startswith('fixed_z'):
                    comb = list(key.split('_')[-1])
                    for i in range(len(comb)):
                        comb[i] = float(comb[i])
                        if comb[i] == 0:
                            comb[i] = -1.0
                    lab = [comb]
                #lab = torch.unsqueeze(Variable(cuda(torch.Tensor(lab), self.use_cuda)), 1)
            cntr = 0
            for row in zrange:
                cntr += 1
                if loc != -1 and row != loc:
                    continue
                z = z_ori.clone()
                for val in interpolation:
                    z[:, row] = val
                    if key == 'fixed_z' and row == 1:
                        #print("NOW:", z)
                        pass
                    try:
                        # CHANGE HERE
                        if self.condition:
                            #z = torch.cat([z, lab], 1)
                            sample = torch.sigmoid(self.net._decode(z, lab)).data
                        else:
                            sample = torch.sigmoid(decoder(z)).data
                    except Exception as e:
                        print("Error while traversal with:",key)
                        raise(e)
                    if (self.beta == 1) or (self.beta > 1 and cntr <= 32):
                        samples.append(sample)
                    gifs.append(sample)
            if self.dataset == "dsprites" and key == "color_img":
                print("COLOR_IMG:",type(color_img.data))
                samples.append(color_img.data)
                recon_orig = torch.sigmoid(decoder(z_ori)).data
                #print("RECON_COL:",type(recon_orig))
                samples.append(recon_orig)
                #samples = [color_img, recon_orig] + samples

            if "celeba" in self.dataset:
                if key == 'fixed_img':
                    samples.append(fixed_img.data)
                    if self.condition:
                        lab = fixed_label
                        samples.append(torch.sigmoid(self.net._decode(fixed_img_z, lab)).data)
                    else:
                        samples.append(torch.sigmoid(decoder(fixed_img_z)).data)
                elif key == 'random_img':
                    samples.append(random_img.data)
                    if self.condition:
                        lab = rand_label
                        samples.append(torch.sigmoid(self.net._decode(random_img_z, lab)).data)
                    else:
                        samples.append(torch.sigmoid(decoder(random_img_z)).data)
                elif key.startswith('random_z'):
                    if self.condition:
                        comb = list(key.split('_')[-1])
                        for i in range(len(comb)):
                            comb[i] = float(comb[i])
                            if comb[i] == 0:
                                comb[i] = -1.0
                        lab = [comb]
                        samples.append(torch.sigmoid(self.net._decode(random_z, lab)).data)

                #elif 'random_z' in key:
                #    if self.condition:
                #        if key.endswith('_0'):
                #            lab = [-1]
                #        elif key.endswith('_1'):
                #            lab = [1]
                #        samples.append(torch.sigmoid(self.net._decode(random_z, lab)).data)
                #    else:
                #        samples.append(torch.sigmoid(self.net._decode(random_z)).data)

                elif key.startswith('fixed_z'):
                    if self.condition:
                        comb = list(key.split('_')[-1])
                        for i in range(len(comb)):
                            comb[i] = float(comb[i])
                            if comb[i] == 0:
                                comb[i] = -1.0
                        lab = [comb]
                        samples.append(torch.sigmoid(self.net._decode(fixed_z, lab)).data)

                #elif 'fixed_z' in key:
                #    if self.condition:
                #        if key.endswith('_0'):
                #            lab = [-1]
                #        elif key.endswith('_1'):
                #            lab = [1]
                #        samples.append(torch.sigmoid(self.net._decode(fixed_z, lab)).data)
                #    else:
                #        samples.append(torch.sigmoid(self.net._decode(fixed_z)).data)


            samples = torch.cat(samples, dim=0).cpu()
            #print("BEFORE:", samples.size())
            #samples = torch.cat(samples, dim=1).cpu()
            #print("AFTER:", samples.size())

            title = '{}_latent_traversal(iter:{})'.format(key, self.global_iter)

            if C is not None:
                C = str(C)
                C = C[:C.find("[")].strip()
                C_val = '_C_'+C
            else:
                C_val = ''

            if self.viz_on:
                self.viz.images(samples, env=self.viz_name+'_traverse',
                                opts=dict(title=title+C_val), nrow=len(interpolation))

        if self.save_output:
            output_dir = os.path.join(self.output_dir, str(self.global_iter))
            os.makedirs(output_dir, exist_ok=True)
            gifs = torch.cat(gifs)
            gifs = gifs.view(len(Z), self.z_dim, len(interpolation), self.nc, 64, 64).transpose(1, 2)
            for i, key in enumerate(Z.keys()):
                for j, val in enumerate(interpolation):
                    save_image(tensor=gifs[i][j].cpu(),
                               filename=os.path.join(output_dir, '{}_{}.jpg'.format(key, j)),
                               nrow=self.z_dim, pad_value=1)

                grid2gif(os.path.join(output_dir, key+'*.jpg'),
                         os.path.join(output_dir, key+'.gif'), delay=10)

        self.net_mode(train=True)

    def net_mode(self, train):
        if not isinstance(train, bool):
            raise('Only bool type is supported. True or False')

        if train:
            self.net.train()
        else:
            self.net.eval()

    def save_checkpoint(self, filename, silent=True):
        model_states = {'net':self.net.state_dict(),}
        optim_states = {'optim':self.optim.state_dict(),}
        win_states = {'recon':self.win_recon,
                      'kld':self.win_kld,
                      'mu':self.win_mu,
                      'var':self.win_var,}
        states = {'iter':self.global_iter,
                  'win_states':win_states,
                  'model_states':model_states,
                  'optim_states':optim_states,
                  'dim_klds':self.dim_klds,
                  'last_gen_step': self.last_gen_step,
                  'sorted_dims': self.sorted_dims,
                  'dim_scores': self.dim_scores,
                  'sum_selected_kld': self.sum_selected_kld,
                  'dim_kld_for_modify': self.dim_kld_for_modify,
                  'cum_prob_0': self.cum_prob_0,
                  'cum_prob_1': self.cum_prob_1}
        file_path = os.path.join(self.ckpt_dir, filename)
        with open(file_path, mode='wb+') as f:
            torch.save(states, f)
        if not silent:
            print("=> saved checkpoint '{}' (iter {})".format(file_path, self.global_iter))

    def load_checkpoint(self, filename):
        file_path = os.path.join(self.ckpt_dir, filename)
        if os.path.isfile(file_path):
            checkpoint = torch.load(file_path)
            self.global_iter = checkpoint['iter']
            self.win_recon = checkpoint['win_states']['recon']
            self.win_kld = checkpoint['win_states']['kld']
            self.win_var = checkpoint['win_states']['var']
            self.win_mu = checkpoint['win_states']['mu']
            self.net.load_state_dict(checkpoint['model_states']['net'])
            self.optim.load_state_dict(checkpoint['optim_states']['optim'])
            try:
                self.dim_klds = checkpoint['dim_klds']
            except:
                print("dim_klds not there in checkpoint")
            try:
                self.last_gen_step = checkpoint['last_gen_step']
            except:
                print("last_gen_step not there in checkpoint")
            try:
                self.sorted_dims = checkpoint['sorted_dims']
                self.dim_scores = checkpoint['dim_scores']
            except:
                print("sorted_dims, dim_scores not there in checkpoint")
            try:
                self.sum_selected_kld = checkpoint['sum_selected_kld']
                self.dim_kld_for_modify = checkpoint['dim_kld_for_modify']
            except:
                print("sum_selected_kld, dim_kld_for_modify not there in checkpoint")

            try:
                self.cum_prob_0 = checkpoint['cum_prob_0']
                self.cum_prob_1 = checkpoint['cum_prob_1']
            except:
                print("cum_probs not there in checkpoint")
            print("=> loaded checkpoint '{} (iter {})'".format(file_path, self.global_iter))
        else:
            print("=> no checkpoint found at '{}'".format(file_path))


    def reverse_decoder(self):
        self.net_mode(train=False)
        # initialize a z

        mv_dist = dist.multivariate_normal.MultivariateNormal(cuda(torch.zeros(self.z_dim), self.use_cuda), cuda(torch.eye(self.z_dim), self.use_cuda))
        num_batches = len(self.data_loader.dataset) // self.batch_size
        pbar = tqdm(total=num_batches)
        test_iter = 0
        list_total_kld = [[] for _ in range(self.classes)]
        list_mean_kld = [[] for _ in range(self.classes)]
        list_recon_loss = [[] for _ in range(self.classes)]
        list_prob = [[] for _ in range(self.classes)]

        output_dir = os.path.join(self.output_dir, 'reverse_decoder')
        os.makedirs(output_dir, exist_ok = True)
        count_classes = None

        #if not self.only_random and self.demo_test:
        #    output_dir = os.path.join(self.output_dir, 'recons_audit')
        #    os.makedirs(output_dir, exist_ok = True)
        #    count_classes = None

        for x, idx, label, classes in self.data_loader:
            test_iter += 1
            pbar.update(1)
            #attr_dict = attr_dict[0]
            #print(label)
            x = Variable(cuda(x, self.use_cuda))
            #attr = attr_dict[self.attr]
            #print("ATTR:", attr)
            #exit(0)
            lb = classes[label[0]][0]
            if self.condition:
                try:
                    lb = classes[label[0]][0]
                except Exception as e:
                    print(classes, label)
                    raise(e)
                if '_male' in lb:
                    c = [1]
                else:
                    c = [-1]
                x_recon, mu, logvar, z_org = self.net(x, c)
                #z_org = self.net._encode(x, c)
            else:
                x_recon, mu, logvar, z_org = self.net(x)
                #z_org = self.net._encode(x)
            recon_loss = reconstruction_loss(x, x_recon, self.decoder_dist)
            total_kld, dim_wise_kld, mean_kld = kl_divergence(mu, logvar)
            probs, mean_prob_dims = find_prob(mu)

            z_approx = Variable(cuda(torch.randn(1, self.z_dim), self.use_cuda), volatile=True)
            z_approx.requires_grad = True
            #z_init = mv_dist.sample()
            #z_init.data.resize(1, self.z_dim)
            #if self.condition:
            #    z_img = self.net._decode(z_init, c)
            #else:
            #    z_img = self.net._decode(z_init)
            # Loss
            mse_loss = cuda(torch.nn.MSELoss(), self.use_cuda)
            optimizer_approx = optim.Adam([z_approx], lr=1e-2, betas=(self.beta1, self.beta2))
            for i in range(1,10001):
                if self.condition:
                    img_z_approx = self.net._decode(z_approx, c)
                else:
                    img_z_approx = self.net._decode(z_approx)
                mse_img = mse_loss(img_z_approx, x)
                mse_z = mse_loss(z_approx, z_org)
                mse_gen = mse_loss(img_z_approx, x_recon)

                if i%5000 == 0:
                    pbar.write("[Iter {}] MSE_Img: {:.4f}, MSE_z: {:.4f}, MSE_gen: {:.4f}".format(i, mse_img.item(), mse_z.item(), mse_gen.item()))

                #Backprop
                optimizer_approx.zero_grad()
                mse_img.backward()
                optimizer_approx.step()
            p1, p2 = find_prob(z_org)
            p3, p4 = find_prob(z_approx)
            pbar.write("Label: "+lb+" Prob_Org: "+str(p2)+" Prob_Approx: "+str(p4))

            if count_classes is None:
                count_classes = {}
                #print(classes)
                for c in classes:
                    count_classes[c[0]] = 0
                # Update count for this image
                # Label returns idx I think.
                count_classes[classes[label][0]] += 1

            save_image(tensor=x.data.cpu(), filename=os.path.join(output_dir, classes[label][0]+'_org_'+str(count_classes[classes[label][0]])+'.png'), pad_value=0)
            save_image(tensor=torch.sigmoid(x_recon).data.cpu(), filename=os.path.join(output_dir, classes[label][0]+'_recon_'+str(count_classes[classes[label][0]])+'.png'), pad_value=0)
            save_image(tensor=torch.sigmoid(img_z_approx).data.cpu(), filename=os.path.join(output_dir, classes[label][0]+'_approx_'+str(count_classes[classes[label][0]])+'.png'), pad_value=0)

            #pbar,("Prob_Org:", p2)
            #print("Prob_Approx:", p4)
            #exit(0)




