"""model.py"""

import torch
import torch.nn as nn
#import torch.nn.functional as F
import torch.nn.init as init
from torch.autograd import Variable
from utils import cuda

class MyDataParallel(nn.DataParallel):
    def __getattr__(self, name):
        return getattr(self.module, name)

def reparametrize(mu, logvar):
    std = logvar.div(2).exp()
    eps = Variable(std.data.new(std.size()).normal_())
    return mu + std*eps


class View(nn.Module):
    def __init__(self, size):
        super(View, self).__init__()
        self.size = size

    def forward(self, tensor):
        return tensor.view(self.size)


class BetaVAE_H(nn.Module):
    """Model proposed in original beta-VAE paper(Higgins et al, ICLR, 2017)."""

    def __init__(self, z_dim=10, nc=3, use_cuda=False, cond=False, enc=None, attr_size=0):
        super(BetaVAE_H, self).__init__()
        self.z_dim = z_dim
        self.nc = nc
        self.use_cuda = use_cuda
        self.cond = cond
        self.enc = enc
        if self.cond:
        #    nc_e = nc + 1
            if enc == 'onehot':
                dec_dim = z_dim + 2
            else:
                dec_dim = z_dim + attr_size
            print("Model initialized as Conditional")
            print("Encoding:", self.enc)
        else:
        #    nc_e = nc
            dec_dim = z_dim
        if self.cond:
            self.encoder = nn.Sequential(
                nn.Conv2d(nc, 32, 4, 2, 1),          # B,  32, 32, 32
                nn.ReLU(True),
                nn.Conv2d(32, 32, 4, 2, 1),          # B,  32, 16, 16
                nn.ReLU(True),
                nn.Conv2d(32, 64, 4, 2, 1),          # B,  64,  8,  8
                nn.ReLU(True),
                nn.Conv2d(64, 64, 4, 2, 1),          # B,  64,  4,  4
                nn.ReLU(True),
                nn.Conv2d(64, 256, 4, 1),            # B, 256,  1,  1
                nn.ReLU(True),
            )
        #    View((-1, 256*1*1)),                 # B, 256
        #    nn.Linear(256, z_dim*2),             # B, z_dim*2
        # )
            if self.enc == 'onehot':
                # This plus two thing should be made as plus num_classes that we send as arg.
                self.enc_linear = nn.Sequential(nn.Linear(256+2, z_dim*2))
            else:
                self.enc_linear = nn.Sequential(nn.Linear(256+attr_size, z_dim*2))
        else:
            self.encoder = nn.Sequential(
                nn.Conv2d(nc, 32, 4, 2, 1),          # B,  32, 32, 32
                nn.ReLU(True),
                nn.Conv2d(32, 32, 4, 2, 1),          # B,  32, 16, 16
                nn.ReLU(True),
                nn.Conv2d(32, 64, 4, 2, 1),          # B,  64,  8,  8
                nn.ReLU(True),
                nn.Conv2d(64, 64, 4, 2, 1),          # B,  64,  4,  4
                nn.ReLU(True),
                nn.Conv2d(64, 256, 4, 1),            # B, 256,  1,  1
                nn.ReLU(True),
                View((-1, 256*1*1)),                 # B, 256
                nn.Linear(256, z_dim*2),             # B, z_dim*2
             )


        # Trying parallelization
        #self.encoder = MyDataParallel(self.encoder)
        self.decoder = nn.Sequential(
            nn.Linear(dec_dim, 256),               # B, 256
            View((-1, 256, 1, 1)),               # B, 256,  1,  1
            nn.ReLU(True),
            nn.ConvTranspose2d(256, 64, 4),      # B,  64,  4,  4
            nn.ReLU(True),
            nn.ConvTranspose2d(64, 64, 4, 2, 1), # B,  64,  8,  8
            nn.ReLU(True),
            nn.ConvTranspose2d(64, 32, 4, 2, 1), # B,  32, 16, 16
            nn.ReLU(True),
            nn.ConvTranspose2d(32, 32, 4, 2, 1), # B,  32, 32, 32
            nn.ReLU(True),
            nn.ConvTranspose2d(32, nc, 4, 2, 1),  # B, nc, 64, 64
        )
        # Trying parallelization
        #self.decoder = MyDataParallel(self.decoder)

        self.weight_init()

    def weight_init(self):
        for block in self._modules:
            for m in self._modules[block]:
                kaiming_init(m)

    def forward(self, x, c=None, select_attr=None):
        if c is not None:
            #c_org = Variable(cuda(torch.Tensor(c), self.use_cuda))
            #c_org = Variable(torch.Tensor(c))
            #c = torch.unsqueeze(torch.unsqueeze(torch.Tensor(c), 1), 2)
            #print("c:", c.size())
            #print(c)
            #x_new = torch.cat((x, c.t()), 1)
            #c = c.expand(-1, x.size()[2], x.size()[3]).clone()
            #c = torch.unsqueeze(c, 3)
            #x = torch.unsqueeze(x, 4)
            #print(c)
            #c = Variable(cuda(c, self.use_cuda))
            #x[:,:,:,4] = c
            #c = torch.unsqueeze(c, 1)
            #print("c:",c.size())
            #x = torch.cat((x,c), dim=1)

            distributions = self._encode(x,c)
        else:
            distributions = self._encode(x)

        mu = distributions[:, :self.z_dim]
        logvar = distributions[:, self.z_dim:]
        z = reparametrize(mu, logvar)
        #print("z:", z.size())
        #if c is not None:
        #    z_new = torch.cat([z, torch.unsqueeze(c_org, 1)], 1)
        #    print("z_new:", z_new.size())
        #else:
        #    z_new = z
        if c is not None:
            if select_attr is None:
                x_recon = self._decode(z, c)
            else:
                x_recon = self._decode(z, select_attr)
        else:
            x_recon = self._decode(z)

        return x_recon, mu, logvar, z

    def _encode(self, x, c=None):
        #if self.use_cuda:
        #    return nn.parallel.data_parallel(self.encoder, x)
        #else:
        if c is not None:
            if self.enc == 'onehot':
                c = list(c)
                for i in range(len(c)):
                    if c[i] == -1:
                        c[i] = [1, 0]
                    elif c[i] == 1:
                        c[i] = [0, 1]

            #print(c)
                c = Variable(cuda(torch.Tensor(c), self.use_cuda))
                out_enc = self.encoder(x)
                out_enc = out_enc.view(-1, 256)
                out_enc = torch.cat((out_enc, c), 1)
            else:
                c = Variable(cuda(torch.Tensor(c), self.use_cuda))
                out_enc = self.encoder(x)
                out_enc = out_enc.view(-1, 256)
                # Before we had multi attrs
                #out_enc = torch.cat([out_enc, torch.unsqueeze(c, 1)], 1)
                out_enc = torch.cat([out_enc, c], 1)

            #return nn.Linear(257, self.z_dim*2)(out_enc)
            return self.enc_linear(out_enc)
            pass
        else:
            return self.encoder(x)

    def _decode(self, z, c=None):
        #if self.use_cuda:
        #    return nn.parallel.data_parallel(self.decoder, z)
        #else:
        if c is not None:
            if self.enc == 'onehot':
                c = list(c)
                for i in range(len(c)):
                    if c[i] == -1:
                        c[i] = [1, 0]
                    elif c[i] == 1:
                        c[i] = [0, 1]
                c = Variable(cuda(torch.Tensor(c), self.use_cuda))
                #z = torch.cat([z, torch.unsqueeze(c, 1)], 1)
                z = torch.cat([z, c], 1)
            else:
                c = Variable(cuda(torch.Tensor(c), self.use_cuda))
                # Before we had multi attrs
                #z = torch.cat([z, torch.unsqueeze(c, 1)], 1)
                z = torch.cat([z, c], 1)
            #print("Decoding... z:", z.size())
        return self.decoder(z)


class BetaVAE_B(BetaVAE_H):
    """Model proposed in understanding beta-VAE paper(Burgess et al, arxiv:1804.03599, 2018)."""

    def __init__(self, z_dim=10, nc=1, use_cuda=False, cond=False, enc=None, attr_size=None):
        super(BetaVAE_B, self).__init__()
        self.nc = nc
        self.z_dim = z_dim
        self.use_cuda = use_cuda

        self.encoder = nn.Sequential(
            nn.Conv2d(nc, 32, 4, 2, 1),          # B,  32, 32, 32
            nn.ReLU(True),
            nn.Conv2d(32, 32, 4, 2, 1),          # B,  32, 16, 16
            nn.ReLU(True),
            nn.Conv2d(32, 32, 4, 2, 1),          # B,  32,  8,  8
            nn.ReLU(True),
            nn.Conv2d(32, 32, 4, 2, 1),          # B,  32,  4,  4
            nn.ReLU(True),
            View((-1, 32*4*4)),                  # B, 512
            nn.Linear(32*4*4, 256),              # B, 256
            nn.ReLU(True),
            nn.Linear(256, 256),                 # B, 256
            nn.ReLU(True),
            nn.Linear(256, z_dim*2),             # B, z_dim*2
        )

        self.decoder = nn.Sequential(
            nn.Linear(z_dim, 256),               # B, 256
            nn.ReLU(True),
            nn.Linear(256, 256),                 # B, 256
            nn.ReLU(True),
            nn.Linear(256, 32*4*4),              # B, 512
            nn.ReLU(True),
            View((-1, 32, 4, 4)),                # B,  32,  4,  4
            nn.ConvTranspose2d(32, 32, 4, 2, 1), # B,  32,  8,  8
            nn.ReLU(True),
            nn.ConvTranspose2d(32, 32, 4, 2, 1), # B,  32, 16, 16
            nn.ReLU(True),
            nn.ConvTranspose2d(32, 32, 4, 2, 1), # B,  32, 32, 32
            nn.ReLU(True),
            nn.ConvTranspose2d(32, nc, 4, 2, 1), # B,  nc, 64, 64
        )
        self.weight_init()

    def weight_init(self):
        for block in self._modules:
            for m in self._modules[block]:
                kaiming_init(m)

    def forward(self, x):
        distributions = self._encode(x)
        mu = distributions[:, :self.z_dim]
        logvar = distributions[:, self.z_dim:]
        z = reparametrize(mu, logvar)
        x_recon = self._decode(z).view(x.size())

        return x_recon, mu, logvar

    def _encode(self, x):
        return self.encoder(x)

    def _decode(self, z):
        return self.decoder(z)


def kaiming_init(m):
    if isinstance(m, (nn.Linear, nn.Conv2d)):
        init.kaiming_normal(m.weight)
        if m.bias is not None:
            m.bias.data.fill_(0)
    elif isinstance(m, (nn.BatchNorm1d, nn.BatchNorm2d)):
        m.weight.data.fill_(1)
        if m.bias is not None:
            m.bias.data.fill_(0)


def normal_init(m, mean, std):
    if isinstance(m, (nn.Linear, nn.Conv2d)):
        m.weight.data.normal_(mean, std)
        if m.bias.data is not None:
            m.bias.data.zero_()
    elif isinstance(m, (nn.BatchNorm2d, nn.BatchNorm1d)):
        m.weight.data.fill_(1)
        if m.bias.data is not None:
            m.bias.data.zero_()


if __name__ == '__main__':
    pass
