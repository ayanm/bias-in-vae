#! /bin/sh

python main.py --dataset celeba_m75 --seed 1 --lr 1e-4 --beta1 0.9 --beta2 0.999 \
    --objective H --model H --batch_size 64 --z_dim 128 --max_iter 1.5e6 \
    --beta 1 --viz_name celeba_z128_lrg_m75 --gather_step 5000 \
    --display_step 50000 --save_step 50000 --viz_port 9097 \
    --num_workers 4 --cuda True
