#! /bin/sh

python main.py --dataset celeba --seed 2 --lr 1e-4 --beta1 0.9 --beta2 0.999 \
    --objective B --model B --batch_size 64 --z_dim 32 --max_iter 1.5e6 \
    --viz_name ayan_celeba_B_gamma50_z32_Cstop_1e5_Cmax_50 --C_stop_iter 1e5 --C_max 50 \
    --gamma 50 --viz_port 9097 --num_workers 5 \
    --gather_step 1000 --display_step 5000 --save_step 10000

