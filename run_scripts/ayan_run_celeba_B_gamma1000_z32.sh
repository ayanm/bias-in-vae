#! /bin/sh

python main.py --dataset celeba --seed 1 --lr 1e-4 --beta1 0.9 --beta2 0.999 \
    --objective B --model B --batch_size 64 --z_dim 32 --max_iter 7e5 \
    --viz_name celeba_B_gamma1000_z32_Cstop_3e5 --C_stop_iter 3e5 --C_max 50 \
    --gamma 1000 --viz_port 9097 --num_workers 5 \
    --gather_step 1000 --display_step 5000 --save_step 10000

