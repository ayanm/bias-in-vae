#! /bin/sh

#python main.py --dataset dsprites --seed 2 --lr 5e-4 --beta1 0.9 --beta2 0.999 \
#      --objective B --model B --batch_size 64 --z_dim 10 --max_iter 1.5e6 \
#      --C_stop_iter 1e5 --C_max 20 --gamma 100 --viz_name dsprites_B_gamma100_z10

python main.py --dataset dsprites --seed 2 --lr 5e-4 --beta1 0.9 --beta2 0.999 \
    --objective B --model B --batch_size 64 --z_dim 10 --max_iter 1e6 \
    --viz_name ayan_dsprites_B_gamma100_z10_Cstop_1e5 --C_stop_iter 1e5 --C_max 20 \
    --gamma 100 --viz_port 9097 --num_workers 5 \
    --gather_step 100 --display_step 5000 --save_step 10000

