#! /bin/sh

python main.py --dataset 3dchairs --seed 1 --lr 1e-4 --beta1 0.9 --beta 0.999 \
    --objective H --model H --batch_size 64 --z_dim 10 --max_iter 1.5e4 --display_step 100 \
    --gather_step 10  --beta 4 --num_workers 20 --viz_name 3dchairs_H_beta4_z10
