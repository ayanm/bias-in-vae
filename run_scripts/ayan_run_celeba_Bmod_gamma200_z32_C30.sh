#! /bin/sh

python main.py --dataset celeba --seed 2 --lr 1e-4 --beta1 0.9 --beta2 0.999 \
    --objective B_mod --model B_mod --batch_size 64 --z_dim 32 --max_iter 1.5e6 \
    --viz_name ayan_celeba_Bmod_1_gamma200_z32_Cstop_1_Cmax_30 --C_stop_iter 1 --C_max 30 \
    --gamma 200 --viz_port 9097 --num_workers 10 \
    --gather_step 1000 --display_step 10000 --save_step 10000

