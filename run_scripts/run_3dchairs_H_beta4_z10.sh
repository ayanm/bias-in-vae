#! /bin/sh

python main.py --dataset 3dchairs --seed 1 --lr 1e-4 --beta1 0.9 --beta 0.999 \
    --objective H --model H --batch_size 64 --z_dim 10 --max_iter 60000 \
    --beta 4 --viz_name 3dchairs_H_beta4_z10_2 --num_workers 6 --gather_step 40 \
    --display_step 400 --save_step 400
