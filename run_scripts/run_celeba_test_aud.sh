#! /bin/sh

#celeba_z128_lrg_m50

#celeba_z128_lrg_train_audit

#mkdir checkpoints/celeba_z128_lrg_test_m50_male/
#cp checkpoints/celeba_z128_lrg_m50/last checkpoints/celeba_z128_lrg_test_m50_male/

mkdir checkpoints/celeba_z128_lrg_test_train_audit/
cp checkpoints/celeba_z128_lrg_train_audit/last checkpoints/celeba_z128_lrg_test_train_audit/

#mkdir checkpoints/celeba_z128_lrg_test/
#cp checkpoints/celeba_z128_lrg/last checkpoints/celeba_z128_lrg_test/

python main.py --train False --dataset celeba_test --seed 1 --lr 1e-4 --beta1 0.9 --beta2 0.999 \
    --objective H --model H --batch_size 1 --z_dim 128 --max_iter 2.5e6 \
    --beta 1 --viz_name celeba_z128_lrg_test_train_audit --gather_step 5000 \
    --display_step 50000 --save_step 50000 --viz_port 9097 \
    --num_workers 10 --cuda False --attr male
