#! /bin/sh

#celeba_z128_lrg_m90
#checkpoints/celeba_z128_lrg_test_m90_male_small/last

#mkdir checkpoints/celeba_z128_lrg_audit/
#cp checkpoints/celeba_z128_lrg/last checkpoints/celeba_z128_lrg_audit/

# celeba_z128_lrg_test_m90_male_small
# celeba_test_small

mkdir checkpoints/celeba_z128_lrg_audit_m60_male/
cp checkpoints/celeba_z128_lrg_m60/last checkpoints/celeba_z128_lrg_audit_m60_male/

python main.py --train False --is_audit True --audit_classes 5 --dataset celeba_audit --seed 1 --lr 1e-4 --beta1 0.9 --beta2 0.999 \
    --objective H --model H --batch_size 1 --z_dim 128 --max_iter 2.5e6 \
    --beta 1 --viz_name celeba_z128_lrg_audit_m60_male --gather_step 5000 \
    --display_step 50000 --save_step 50000 --viz_port 9097 \
    --num_workers 10 --cuda False --only_random False --demo_test False 
