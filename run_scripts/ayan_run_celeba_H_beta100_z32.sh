#! /bin/sh
#1.8e6
python main.py --dataset celeba --seed 1 --lr 1e-4 --beta1 0.9 --beta2 0.999 \
    --objective H --model H --batch_size 64 --z_dim 32 --max_iter 500000 \
    --viz_name ayan_celeba_H_beta100_z32 --C_stop_iter 1e5 --C_max 50 \
    --gamma 1000 --viz_port 9097 --beta 100 --num_workers 15 \
    --gather_step 5000 --display_step 50000 --save_step 10000
