#! /bin/sh

#celeba_z128_lrg_m50
#checkpoints/celeba_z128_lrg_test_m50_male_small/last

mkdir checkpoints/ayan_celeba_H_beta10_z32_audit/
cp checkpoints/ayan_celeba_H_beta10_z32/last checkpoints/ayan_celeba_H_beta10_z32_audit/

#mkdir checkpoints/celeba_z128_lrg_audit_m50_male/
#cp checkpoints/celeba_z128_lrg_m50/last checkpoints/celeba_z128_lrg_audit_m50_male/

# celeba_z128_lrg_test_m50_male_small
# celeba_test_small

python main.py --train False --is_audit True --audit_classes 5 --dataset celeba_audit --seed 1 --lr 1e-4 --beta1 0.9 --beta2 0.999 \
    --objective H --model H --batch_size 1 --z_dim 32 --max_iter 2.5e6 \
    --beta 10 --viz_name ayan_celeba_H_beta10_z32_audit --gather_step 5000 \
    --display_step 50000 --save_step 50000 --viz_port 9097 \
    --num_workers 10 --use_cuda False --only_random True --demo_test True 
