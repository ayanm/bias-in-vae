#! /bin/sh

python main.py --dataset celeba --seed 2 --lr 1e-4 --beta1 0.9 --beta2 0.999 \
    --objective H --model H --batch_size 64 --z_dim 32 --max_iter 200000 \
    --viz_name ayan_celeba_H_beta10_z32_try --C_stop_iter 1e5 --C_max 50 \
    --gamma 1000 --viz_port 8097 --beta 10 --num_workers 15 \
    --gather_step 5000 --display_step 25000 --save_step 10000
