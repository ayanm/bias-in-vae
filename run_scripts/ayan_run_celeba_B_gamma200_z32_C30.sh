#! /bin/sh

python main.py --dataset celeba --seed 2 --lr 1e-4 --beta1 0.9 --beta2 0.999 \
    --objective B --model B --batch_size 64 --z_dim 32 --max_iter 1.5e6 \
    --viz_name ayan_celeba_B_gamma200_z32_Cstop_1e5_Cmax_15 --C_stop_iter 1e5 --C_max 15 \
    --gamma 200 --viz_port 7097 --num_workers 12 \
    --gather_step 1000 --display_step 10000 --save_step 10000

