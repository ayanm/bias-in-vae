import numpy as np
import os
import pickle
import seaborn as sns
import matplotlib.pyplot as plt
from scipy.stats import pearsonr
import pandas as pd

#with open("train_dict_attrs.pickle", "rb") as f:
#    train_dict = pickle.load(f)

#attrs = list(train_dict[list(train_dict.keys())[0]].keys())
#print(attrs)



#sum1 = sum(x.get(attrs[0]) == '1' for x in train_dict.values())

#sum2 = sum(x.get(attrs[0]) == '-1' for x in train_dict.values())

#corr_mat = [[0 for _ in range(len(attrs))] for _ in range(len(attrs))]

#for i in range(len(attrs)):
#    attr_0 = attrs[i]
#    for j in range(len(attrs)):
#        attr_1 = attrs[j]
#        attr_0 = [int(x.get(attrs[0])) for x in train_dict.values()]
#        attr_1 = [int(x.get(attrs[1])) for x in train_dict.values()]
#        corr_mat[i][j] = pearsonr(attr_0, attr_1)[0]

#plot = sns.heatmap(corr_mat, xticklabels=attrs, yticklabels=attrs)
#fig = plot.get_figure()
#fig.savefig("corr_map.png")
#print(sum1, sum2)


def plotPerColumnDistribution(df, nGraphShown, nGraphPerRow):
    nunique = df.nunique()
    df = df[[col for col in df if nunique[col] > 1 and nunique[col] < 50]] # For displaying purposes, pick columns that have between 1 and 50 unique values
    nRow, nCol = df.shape
    columnNames = list(df)
    nGraphRow = (nCol + nGraphPerRow - 1) / nGraphPerRow
    plt.figure(num = None, figsize = (6 * nGraphPerRow, 8 * nGraphRow), dpi = 80, facecolor = 'w', edgecolor = 'k')
    for i in range(min(nCol, nGraphShown)):
        plt.subplot(nGraphRow, nGraphPerRow, i + 1)
        columnDf = df.iloc[:, i]
        if (not np.issubdtype(type(columnDf.iloc[0]), np.number)):
            valueCounts = columnDf.value_counts()
            valueCounts.plot.bar()
        else:
            columnDf.hist()
        plt.ylabel('counts')
        plt.xticks([-1, 1], rotation = 90)
        plt.title(f'{columnNames[i]} (column {i})')
    plt.tight_layout(pad = 1.0, w_pad = 1.0, h_pad = 1.0)
    plt.savefig("attr_distr.png")
    plt.clf()

def plotCorrelationMatrix(df, graphWidth):
    filename = df.dataframeName
    df = df.dropna('columns') # drop columns with NaN
    df = df[[col for col in df if df[col].nunique() > 1]] # keep columns where there are more than 1 unique values
    if df.shape[1] < 2:
        print(f'No correlation plots shown: The number of non-NaN or constant columns ({df.shape[1]}) is less than 2')
        return
    corr = df.corr()
    plt.figure(num=None, figsize=(graphWidth, graphWidth), dpi=120, facecolor='w', edgecolor='k')
    plt.set_cmap("inferno")
    fig, ax = plt.subplots(figsize=(graphWidth, graphWidth), dpi=120, facecolor='w', edgecolor='k')
    #corrMat = plt.matshow(corr, fignum=1)
    corrMat = ax.matshow(corr)
    #sns.heatmap(corr, annot=True, cmap="inferno")
    plt.xticks(range(len(corr.columns)), corr.columns, rotation=90)
    plt.yticks(range(len(corr.columns)), corr.columns)
    plt.gca().xaxis.tick_bottom()
    plt.colorbar(corrMat)
    for (i, j), z in np.ndenumerate(corr.values):
        ax.text(j, i, '{:0.2f}'.format(z), ha='center', va='center')
    plt.title(f'Correlation Matrix for {filename}', fontsize=15)
    plt.savefig("corr_mat.png")
    plt.clf()

def plotScatterMatrix(df, plotSize, textSize):
    df = df.select_dtypes(include =[np.number]) # keep only numerical columns
    # Remove rows and columns that would lead to df being singular
    df = df.dropna('columns')
    df = df[[col for col in df if df[col].nunique() > 1]] # keep columns where there are more than 1 unique values
    #columnNames = list(df)
    #if len(columnNames) > 10: # reduce the number of columns for matrix inversion of kernel density plots
    #    columnNames = columnNames[:10]
    columnNames = ["Attractive", "Heavy_Makeup", "Male", "No_Beard", "Wearing_Necktie"]
    df = df[columnNames]

    ax = pd.plotting.scatter_matrix(df, alpha=0.75, figsize=[plotSize, plotSize], diagonal='kde')
    corrs = df.corr().values
    for i, j in zip(*plt.np.triu_indices_from(ax, k = 1)):
        ax[i, j].annotate('Corr. coef = %.3f' % corrs[i, j], (0.8, 0.2), xycoords='axes fraction', ha='center', va='center', size=textSize)
    plt.suptitle('Scatter and Density Plot')
    #plt.show()
    plt.savefig("Corr_scatter.png")

df1 = pd.read_csv('data/list_attr_celeba.csv', delimiter=' ')
df1.dataframeName = 'list_attr_celeba.csv'
nRow, nCol = df1.shape

#attrs = list(df1.columns)[1:]

#df2 = pd.DataFrame(columns=["Attr", "Value"])
#for index,row in df1.iterrows():
    #print(row)
#    for attr in attrs:
#        df2.loc[len(df2)] = {"Attr":attr, "Value":row[attr]}

#print(df2.head())


plotCorrelationMatrix(df1, 30)
plotPerColumnDistribution(df1, 40, 5)
plotScatterMatrix(df1, 20, 10)

