import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd


ages = ["0-25", "26-35", "36-50", "51-65"]
genders = ["female", "male"]
races = ["white", "hispanic/latino", "asian", "black/african_american"]
#types = ["original", "90-10", "75-25", "50-50"]

def ret_type(num):
    types = ["original"] * num + ["Female_10-Male_90"] * num + ["Female_25-Male_75"] * num + ["Female_50-Male_50"] * num
    return types

#age_vals = [66, 39, 38, 62, 2, 11, 4, 2, 109, 140, 144, 109, 21, 10, 14, 26]
age_vals = [66, 2, 109, 21, 39, 11, 140, 10, 38, 4, 144, 14, 62, 2, 109, 26]
#age_scores = [0.42, 0.44, 0.41, 0.41, || 0.32, 0.37, 0.37, 0.36, || 0.40, 0.45, 0.43, 0.42, || 0.34, 0.42, 0.37, 0.41]
age_scores = [0.42, 0.32, 0.40, 0.34, 0.44, 0.37, 0.45, 0.42, 0.41, 0.37, 0.43, 0.37, 0.41, 0.36, 0.42, 0.41]
#gender_vals = [141, 37, ||62, 119, ||57, 163, ||138, 80]
gender_vals = [141, 57, 37, 163, 62, 138, 119, 80]
#gender_scores = [0.84, 0.66, ||0.78, 0.77,|| 0.78, 0.86, ||0.82, 0.76]
gender_scores = [0.84, 0.78, 0.66, 0.86, 0.78, 0.82, 0.76, 0.77]
#race_vals = [102, 125, 107, 109, ||68, 47, 61, 63, ||19, 27, 27, 24, ||9, 1, 5, 3]
race_vals = [102, 68, 19, 9, 125, 47, 27, 1, 107, 61, 27, 5, 109, 63, 24, 3]
#race_scores = [0.58, 0.59, 0.55, 0.56, ||0.46, 0.44, 0.46, 0.44, ||0.48, 0.49, 0.5, 0.44, ||0.47, 0.41, 0.49, 0.56]
race_scores = [0.58, 0.46, 0.48, 0.47, 0.59, 0.44, 0.49, 0.41, 0.55, 0.46, 0.5, 0.49, 0.56, 0.44, 0.44, 0.56]

'''df_count = pd.DataFrame({"Attribute":attrs, "Count":counts, "Type":types})
df_probs = pd.DataFrame({"Attribute":attrs, "Log_Probability":avg_probs, "Type":types})
df_tot_klds = pd.DataFrame({"Attribute":attrs, "Total_KLD":total_kld, "Type":types})
df_max_probs = pd.DataFrame({"Attribute":attrs, "Max_Probability":max_probs, "Type":types})
df_min_probs = pd.DataFrame({"Attribute":attrs, "Min_Probability":min_probs, "Type":types})
df_max_klds = pd.DataFrame({"Attribute":attrs, "Max_KLD":max_klds, "Type":types})
df_min_klds = pd.DataFrame({"Attribute":attrs, "Min_KLD":min_klds, "Type":types})
df_losses = pd.DataFrame({"Attribute":attrs, "Recon_Loss":losses, "Type":types})
'''

df_ages = pd.DataFrame({"Age_Range": ages * 4, "Age_Counts": age_vals, "Age_Prob": age_scores, "Model_Type": ret_type(4)})
df_genders = pd.DataFrame({"Gender": genders * 4, "Gender_Counts": gender_vals, "Gender_Prob": gender_scores, "Model_Type": ret_type(2)})
df_races = pd.DataFrame({"Race": races * 4, "Race_Counts": race_vals, "Race_Prob": race_scores, "Model_Type": ret_type(4)})

plt.figure(figsize=(16,10))
sns.set_style("whitegrid")

sns.barplot(y='Age_Counts', hue='Age_Range', x='Model_Type', data=df_ages)
plt.title('Age Count Distribution')
plt.savefig("Age_Counts_Hist.png")
plt.clf()

sns.barplot(y='Age_Prob', hue='Age_Range', x='Model_Type', data=df_ages)
plt.title('Age Decision Prob Strength')
plt.savefig("Age_Prob_Hist.png")
plt.clf()


sns.barplot(y='Gender_Counts', hue='Gender', x='Model_Type', data=df_genders)
plt.title("Gender Count Distribution")
plt.savefig("Gender_Counts_Hist.png")
plt.clf()

sns.barplot(y='Gender_Prob', hue='Gender', x='Model_Type', data=df_genders)
plt.title("Gender Decision Prob Strength")
plt.savefig("Gender_Prob_Hist.png")
plt.clf()

sns.barplot(y='Race_Counts', hue='Race', x='Model_Type', data=df_races)
plt.title("Race Count Distribution")
plt.savefig("Race_Counts_Hist.png")
plt.clf()

sns.barplot(y='Race_Prob', hue='Race', x='Model_Type', data=df_races)
plt.title("Race Decision Prob Strength")
plt.savefig("Race_Prob_Hist.png")
plt.clf()

