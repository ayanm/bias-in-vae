import json
import glob
import os
from collections import Counter
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

PATH = os.getcwd()

'''with open('out_2.json') as f:
    json_file = f.read()

json_file = json_file.replace("\'", "\"")

parsed = json.loads(json_file)
print(json.dumps(parsed, indent=4))
'''

models = ["", "_m90_male", "_m75_male", "_m50_male"]
#models = ["_m60_male"]

classes = ["black_female", "black_male", "white_male", "white_female"]


for m in models:

    df_gender_err = pd.DataFrame(columns=["Error_Rate","Demography", "Image_Type"])
    df_race_org = pd.DataFrame(columns=["Predicted_Race", "Probability_Score", "Demography", "Image_Type"])
    for c in classes:

        org_genders = {}
        recon_genders = {}
        for tp in ["org", "recon"]:

            files = [f for f in glob.glob(os.path.join(PATH, "celeba_z128_lrg_audit"+m, "recons_audit",c+"*"+tp+"*.json"))]

            age_count = {"0-25":0, "26-35":0, "36-50":0, "51-65":0, "65_plus":0}
            age_values = {"0-25":0, "26-35":0, "36-50":0, "51-65":0, "65_plus":0}

            gender_count = None
            gender_values = None

            race_count = None
            race_values = None

            missed_faces = 0
            missed_files = []

            race_wrong = []
            race_mistake = []
            race_mistake_scores = []
            gender_wrong = []

            for f in files:
                with open(f) as fr:
                    js = json.loads(fr.read().replace("\'", "\""))
                # First let us process the age
                # This gives most probable age. (Highest probability score)
                # print(js)
                file_num = int(f.split(".")[0].split("_")[-2])
                if tp == "org":
                    org_genders[file_num] = None
                else:
                    recon_genders[file_num] = None
                try:
                    age = int(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['name'])
                    if age > 0 and age <= 25:
                        age_count["0-25"] += 1
                        age_values["0-25"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                    elif age > 25 and age <= 35:
                        age_count["26-35"] += 1
                        age_values["26-35"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                    elif age > 35 and age <= 50:
                        age_count["36-50"] += 1
                        age_values["36-50"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                    elif age > 50 and age <= 65:
                        age_count["51-65"] += 1
                        age_values["51-65"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                    elif age > 65:
                        age_count["65_plus"] += 1
                        age_values["65_plus"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                    # Now to gender.
                    if gender_count is None:
                        gender_count = {}
                        gender_values = {}
                        # Fill in gender specs according to the JSON
                        list_g = js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts']
                        for g in list_g:
                            gender_count[g['name']] = 0
                            gender_values[g['name']] = 0
                    # Get gender
                    gender = js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['name']
                    gender_count[gender] += 1
                    gender_values[gender] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['value'])
                    if gender == 'feminine' and '_male' in c:
                        gender_wrong.append(f)
                    elif gender == 'masculine' and '_female' in c:
                        gender_wrong.append(f)
                    if tp == "org":
                        org_genders[file_num] = gender
                    else:
                        recon_genders[file_num] = gender
                    # Now the race.
                    if race_count is None:
                        race_count = {}
                        race_values = {}
                        # Fill in the race list from JSON
                        list_r = js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts']
                        for r in list_r:
                            race_count[r['name']] = 0
                            race_values[r['name']] = 0
                    # Get the race
                    race = js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts'][0]['name']
                    race_count[race] += 1
                    race_values[race] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts'][0]['value'])

                    # ["Predicted_Race", "Probability_Score", "Demography", "Image_Type"]

                    if m=="":
                        if "white" in race:
                            rc = 'white'
                        elif "black" in race:
                            rc = "black/african_american"
                        elif "asian" in race:
                            rc = "asian"
                        else:
                            rc = "hispanic/brown"
                        df_race_org.loc[len(df_race_org)] = [rc, float(js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts'][0]['value']), c, "Actual" if tp == "org" else "Recon"]

                    if 'white' in c and not 'white' in race:
                        #print("White:",f)
                        race_wrong.append(f)
                        race_mistake.append(race)
                        race_mistake_scores.append(float(js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts'][0]['value']))
                        pass
                    elif 'black' in c and not 'black' in race:
                        race_wrong.append(f)
                        race_mistake.append(race)
                        race_mistake_scores.append(float(js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts'][0]['value']))
                except Exception as e:
                    print(f)
                    missed_files.append(f)
                    #print(js)

            print("Done Parsing!")

            #for k in age_values:
            #    if age_count[k] != 0:
            #        age_values[k] = age_values[k] / age_count[k]
            #        age_values[k] = "{0:.2f}".format(age_values[k])

            #for k in gender_values:
            #    if gender_count[k] != 0:
            #        gender_values[k] = gender_values[k] / gender_count[k]
            #        gender_values[k] = "{0:.2f}".format(gender_values[k])

            #for k in race_values:
            #    if race_count[k] != 0:
            #        race_values[k] = race_values[k] / race_count[k]
            #        race_values[k] = "{0:.2f}".format(race_values[k])

            #print("Result of Demographics")
            #print(c, "::", tp)
            #print("----------------------")
            #print("Age")
            #print("*******")
            #print(age_count)
            #print("^^^^^^^^^^^^^^^^")
            '''print("Probability Scores")
            print(age_values)
            print("*******")
            print("Gender")
            print(gender_count)
            print("^^^^^^^^^^^^^^^^")
            print("Probability Scores")
            print(gender_values)
            print("*******")
            print("Race")
            print(race_count)
            print("^^^^^^^^^^^^^^^^")
            print("Probability Scores")
            print(race_values)
            print("---------------------")
            print("Missed Faces:", len(missed_files))
            print("Files:", missed_files)

            print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
            print("PERCENTAGE VALUES OF API PERFORMANCE")
            print("FACE MISS RATE:", len(missed_files) / len(files))
            print("GENDER MISS RATE:", len(gender_wrong) / len(files))
            print("RACE MISS RATE:", len(race_wrong) / len(files))
            print("RACE MISTAKES:", Counter(race_mistake))
            print("------------------------------")'''

            print(m)
            print(c)
            df_gender_err.loc[len(df_gender_err)] = [len(gender_wrong)/len(files), c, "Original" if tp=="org" else "Recon"]

        print("%%%%%%%%%%%END%%%%%%%%%%%%%%%%")

        if m == "":
            print("**** PLOT GENDER PREDICTIONS FOR ORIGINAL ****")
            print("ORIGINAL IMAGES")
            plt.figure(figsize=(16,10))
            sns.set_style("whitegrid")

            # ["Predicted_Race", "Probability_Score", "Demography", "Image_Type"]

            #print(df_race_org)

            sns.barplot(y="Probability_Score", x="Demography", hue="Predicted_Race", data=df_race_org[df_race_org["Image_Type"] == "Actual"], hue_order=["white", "hispanic/brown", "asian", "black/african_american"], capsize=0.05, order=["black_female", "black_male", "white_female", "white_male"])
            plt.title("Demography Prediction Probability Strength For Original Images")
            plt.savefig("Dem_Pred_Org_Scores.png")
            plt.clf()

            #sns.countplot(x="Demography", hue="Predicted_Race", data=df_race_org[df_race_org["Image_Type"] == "Actual"])
            df = df_race_org[df_race_org["Image_Type"] == "Actual"]


            prop_df = (df["Predicted_Race"].groupby(df["Demography"])\
                    .value_counts(normalize=True).rename("Proportion").reset_index())

            sns.barplot(x="Demography", y="Proportion", data=prop_df, hue="Predicted_Race", hue_order=["white", "hispanic/brown", "asian", "black/african_american"], order=["black_female", "black_male", "white_female", "white_male"])
            #sns.barplot(x="Demography", y="Demography", hue="Predicted_Race", data=df, estimator=lambda x: len(x) / len(df), orient="v")
            plt.title("Demography Prediction For Original Images")
            plt.savefig("Dem_Pred_Org.png")
            plt.clf()

            sns.barplot(y="Probability_Score", x="Demography", hue="Predicted_Race", capsize=0.05, data=df_race_org[df_race_org["Image_Type"] == "Recon"], hue_order=["white", "hispanic/brown", "asian", "black/african_american"], order=["black_female", "black_male", "white_female", "white_male"])
            plt.title("Demography Prediction Probability Strength For Reconstructed Images")
            plt.savefig("Dem_Pred_Recon_Scores.png")
            plt.clf()


            df = df_race_org[df_race_org["Image_Type"] == "Recon"]
            #sns.countplot(x="Demography", hue="Predicted_Race", data=df_race_org[df_race_org["Image_Type"] == "Recon"])
            prop_df = (df["Predicted_Race"].groupby(df["Demography"])\
                    .value_counts(normalize=True).rename("Proportion").reset_index())

            sns.barplot(x="Demography", y="Proportion", data=prop_df, hue="Predicted_Race", hue_order=["white", "hispanic/brown", "asian", "black/african_american"], order=["black_female", "black_male", "white_female", "white_male"])

            #sns.barplot(x="Demography", y="Demography", hue="Predicted_Race", data=df, estimator=lambda x: len(x) / len(df), orient="v")
            plt.title("Demography Prediction For Reconstructed Images")
            plt.savefig("Dem_Pred_Recon.png")
            plt.clf()

            print("DONE PRINTING GENDER_PREDS")

        print("*** NOW GENDER MISMATCHES ****")
        mismatches = 0
        count = 0
        for i in org_genders.keys():
            count += 1
            if org_genders[i] != recon_genders[i]:
                mismatches += 1
        print("MISMATCHES GENDER", c, "::", tp, ":", mismatches/count)
        print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

    print("PLOTTING!")
    plt.figure(figsize=(16,10))
    sns.set_style("whitegrid")

    sns.barplot(y="Error_Rate", x="Demography", hue="Image_Type", data=df_gender_err)
    if m == "":
        plt.title("Error_Rate Gender For Original Data")
        plt.savefig("Gender_Err_orig")
        plt.clf()
    elif m == "_m90_male":
        plt.title("Error_Rate Gender When Training Data Male:Female is 90:10")
        plt.savefig("Gender_Err_m90")
        plt.clf()
    elif m == "_m75_male":
        plt.title("Error_Rate Gender When Training Data Male:Female is 75:25")
        plt.savefig("Gender_Err_m75")
        plt.clf()
    elif m == "_m50_male":
        plt.title("Error_Rate Gender When Training Data Male:Female is 50:50")
        plt.savefig("Gender_Err_m50")
        plt.clf()


print("DONE!!!")
