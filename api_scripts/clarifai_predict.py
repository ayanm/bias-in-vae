#!/usr/bin/python3
from clarifai.rest import ClarifaiApp
import glob
import os
import json

PATH = os.getcwd()


# Read all png files in the directory. Write output to corresponding JSON files.
# I think this is better as we can dump the output onto our storage and process later.
# This prevents multiple API calls.

files = [f for f in glob.glob(PATH+"/*.png")]


app = ClarifaiApp(api_key='bb8e7a2b54b641b7aedb9bf71dc25806')
model = app.models.get('demographics')

for f in files:
    name = f.split("/")
    img_name = name[-1].split(".")[0]
    #print(img_name)
    response = model.predict_by_filename(f)
    with open(img_name+"_out.json", "w") as fw:
        json.dump(response, fw)

#response = model.predict_by_filename('random_2.png')

#print(response)
print("Done!")
