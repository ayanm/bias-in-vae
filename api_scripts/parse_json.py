import json
import glob
import os
from collections import Counter

PATH = os.getcwd()

'''with open('out_2.json') as f:
    json_file = f.read()

json_file = json_file.replace("\'", "\"")

parsed = json.loads(json_file)
print(json.dumps(parsed, indent=4))
'''

classes = ["black_female", "black_male", "white_male", "white_female"]

for c in classes:

    org_genders = {}
    recon_genders = {}
    for tp in ["org", "recon"]:

        files = [f for f in glob.glob(os.path.join(PATH,c+"*"+tp+"*.json"))]

        age_count = {"0-25":0, "26-35":0, "36-50":0, "51-65":0, "65_plus":0}
        age_values = {"0-25":0, "26-35":0, "36-50":0, "51-65":0, "65_plus":0}

        gender_count = None
        gender_values = None

        race_count = None
        race_values = None

        missed_faces = 0
        missed_files = []

        race_wrong = []
        race_mistake = []
        gender_wrong = []

        for f in files:
            with open(f) as fr:
                js = json.loads(fr.read().replace("\'", "\""))
            # First let us process the age
            # This gives most probable age. (Highest probability score)
            # print(js)
            file_num = int(f.split(".")[0].split("_")[-2])
            if tp == "org":
                org_genders[file_num] = None
            else:
                recon_genders[file_num] = None
            try:
                age = int(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['name'])
                if age > 0 and age <= 25:
                    age_count["0-25"] += 1
                    age_values["0-25"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                elif age > 25 and age <= 35:
                    age_count["26-35"] += 1
                    age_values["26-35"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                elif age > 35 and age <= 50:
                    age_count["36-50"] += 1
                    age_values["36-50"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                elif age > 50 and age <= 65:
                    age_count["51-65"] += 1
                    age_values["51-65"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                elif age > 65:
                    age_count["65_plus"] += 1
                    age_values["65_plus"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                # Now to gender.
                if gender_count is None:
                    gender_count = {}
                    gender_values = {}
                    # Fill in gender specs according to the JSON
                    list_g = js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts']
                    for g in list_g:
                        gender_count[g['name']] = 0
                        gender_values[g['name']] = 0
                # Get gender
                gender = js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['name']
                gender_count[gender] += 1
                gender_values[gender] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['value'])
                if gender == 'feminine' and '_male' in c:
                    gender_wrong.append(f)
                elif gender == 'masculine' and '_female' in c:
                    gender_wrong.append(f)
                if tp == "org":
                    org_genders[file_num] = gender
                else:
                    recon_genders[file_num] = gender
                # Now the race.
                if race_count is None:
                    race_count = {}
                    race_values = {}
                    # Fill in the race list from JSON
                    list_r = js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts']
                    for r in list_r:
                        race_count[r['name']] = 0
                        race_values[r['name']] = 0
                # Get the race
                race = js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts'][0]['name']
                race_count[race] += 1
                race_values[race] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts'][0]['value'])
                if 'white' in c and not 'white' in race:
                    #print("White:",f)
                    race_wrong.append(f)
                    race_mistake.append(race)
                    pass
                elif 'black' in c and not 'black' in race:
                    race_wrong.append(f)
                    race_mistake.append(race)
            except Exception as e:
                print(f)
                missed_files.append(f)
                #print(js)

        print("Done Parsing!")

        for k in age_values:
            if age_count[k] != 0:
                age_values[k] = age_values[k] / age_count[k]
                age_values[k] = "{0:.2f}".format(age_values[k])

        for k in gender_values:
            if gender_count[k] != 0:
                gender_values[k] = gender_values[k] / gender_count[k]
                gender_values[k] = "{0:.2f}".format(gender_values[k])

        for k in race_values:
            if race_count[k] != 0:
                race_values[k] = race_values[k] / race_count[k]
                race_values[k] = "{0:.2f}".format(race_values[k])

        print("Result of Demographics")
        print(c, "::", tp)
        print("----------------------")
        print("Age")
        print("*******")
        print(age_count)
        print("^^^^^^^^^^^^^^^^")
        print("Probability Scores")
        print(age_values)
        print("*******")
        print("Gender")
        print(gender_count)
        print("^^^^^^^^^^^^^^^^")
        print("Probability Scores")
        print(gender_values)
        print("*******")
        print("Race")
        print(race_count)
        print("^^^^^^^^^^^^^^^^")
        print("Probability Scores")
        print(race_values)
        print("---------------------")
        print("Missed Faces:", len(missed_files))
        print("Files:", missed_files)

        print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
        print("PERCENTAGE VALUES OF API PERFORMANCE")
        print("FACE MISS RATE:", len(missed_files) / len(files))
        print("GENDER MISS RATE:", len(gender_wrong) / len(files))
        print("RACE MISS RATE:", len(race_wrong) / len(files))
        print("RACE MISTAKES:", Counter(race_mistake))
        print("------------------------------")

    print("%%%%%%%%%%%END%%%%%%%%%%%%%%%%")
    print("*** NOW GENDER MISMATCHES ****")
    mismatches = 0
    count = 0
    for i in org_genders.keys():
        count += 1
        if org_genders[i] != recon_genders[i]:
            mismatches += 1
    print("MISMATCHES GENDER", c, "::", tp, ":", mismatches/count)
    print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
