import json
import glob
import os
from collections import Counter
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

PATH = os.getcwd()

'''with open('out_2.json') as f:
    json_file = f.read()

json_file = json_file.replace("\'", "\"")

parsed = json.loads(json_file)
print(json.dumps(parsed, indent=4))
'''

#models = ["", "_m90_male", "_m75_male", "_m60_male", "_m50_male"]
models = ["celeba_z128_lrg_audit","ayan_celeba_H_beta10_z8_audit", "ayan_celeba_H_beta10_z16_audit", "ayan_celeba_H_beta10_z32_audit"]
#models = ["_m60_male"]

#classes = ["black_female", "black_male", "white_male", "white_female"]

df_age = pd.DataFrame(columns=["Predicted_Age", "Probability_Score", "Model_Type"])
df_race = pd.DataFrame(columns=["Predicted_Race", "Probability_Score", "Model_Type"])
df_gender = pd.DataFrame(columns=["Predicted_Gender", "Probability_Score", "Model_Type"])


for m in models:

    if m == "celeba_z128_lrg_audit":
        model_type = "Beta1_Z128"
    elif "z8" in m:
        model_type = "Beta10_Z8"
    elif "z16" in m:
        model_type = "Beta10_Z16"
    elif "z32" in m:
        model_type = "Beta10_Z32"
    #elif m == "_m50_male":
    #    model_type = "Female_50-Male_50"

    #df_gender_err = pd.DataFrame(columns=["Error_Rate","Demography", "Image_Type"])
        #for c in classes:

        #org_genders = {}
        #recon_genders = {}
        #for tp in ["org", "recon"]:

    #files = [f for f in glob.glob(os.path.join(PATH, "celeba_z128_lrg_audit"+m, "recons_audit",c+"*"+tp+"*.json"))]
    files = [f for f in glob.glob(os.path.join(PATH, m, "random_generate", "*.json"))]

    age_count = {"0-25":0, "26-35":0, "36-50":0, "51-65":0, "65_plus":0}
    age_values = {"0-25":0, "26-35":0, "36-50":0, "51-65":0, "65_plus":0}

    gender_count = None
    gender_values = None

    race_count = None
    race_values = None

    missed_faces = 0
    missed_files = []

    #race_wrong = []
    #race_mistake = []
    #race_mistake_scores = []
    #gender_wrong = []

    for f in files:
        with open(f) as fr:
            js = json.loads(fr.read().replace("\'", "\""))
        # First let us process the age
        # This gives most probable age. (Highest probability score)
        # print(js)
        #file_num = int(f.split(".")[0].split("_")[-2])
        #if tp == "org":
        #    org_genders[file_num] = None
        #else:
        #    recon_genders[file_num] = None
        try:
            age = int(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['name'])
            if age > 0 and age <= 25:
                age_count["0-25"] += 1
                age_values["0-25"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                df_age.loc[len(df_age)] = ["0-25", float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value']), model_type]
            elif age > 25 and age <= 35:
                age_count["26-35"] += 1
                age_values["26-35"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                df_age.loc[len(df_age)] = ["26-35", float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value']), model_type]
            elif age > 35 and age <= 50:
                age_count["36-50"] += 1
                age_values["36-50"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                df_age.loc[len(df_age)] = ["36-50", float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value']), model_type]
            elif age > 50 and age <= 65:
                age_count["51-65"] += 1
                age_values["51-65"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                df_age.loc[len(df_age)] = ["51-65", float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value']), model_type]
            elif age > 65:
                age_count["65_plus"] += 1
                age_values["65_plus"] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value'])
                df_age.loc[len(df_age)] = ["65_plus", float(js['outputs'][0]['data']['regions'][0]['data']['face']['age_appearance']['concepts'][0]['value']), model_type]
           # Now to gender.
            if gender_count is None:
                gender_count = {}
                gender_values = {}
                # Fill in gender specs according to the JSON
                list_g = js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts']
                for g in list_g:
                    gender_count[g['name']] = 0
                    gender_values[g['name']] = 0

            # Get gender
            gender = js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['name']
            gender_count[gender] += 1
            gender_values[gender] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['value'])

            df_gender.loc[len(df_gender)] = [gender, float(js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['value']), model_type]

            #if gender == 'feminine' and '_male' in c:
            #    gender_wrong.append(f)
            #elif gender == 'masculine' and '_female' in c:
            #    gender_wrong.append(f)
            #if tp == "org":
            #    org_genders[file_num] = gender
            #else:
            #    recon_genders[file_num] = gender
            # Now the race.
            if race_count is None:
                race_count = {}
                race_values = {}
                # Fill in the race list from JSON
                list_r = js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts']
                for r in list_r:
                    race_count[r['name']] = 0
                    race_values[r['name']] = 0
            # Get the race
            race = js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts'][0]['name']
            race_count[race] += 1
            race_values[race] += float(js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts'][0]['value'])

            # ["Predicted_Race", "Probability_Score", "Demography", "Image_Type"]

            #if m=="":
            if "white" in race:
                rc = 'white'
            elif "black" in race:
                rc = "black/african_american"
            elif "asian" in race:
                rc = "asian"
            else:
                rc = "hispanic/brown"
                #df_race_org.loc[len(df_race_org)] = [rc, float(js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts'][0]['value']), c, "Actual" if tp == "org" else "Recon"]

            df_race.loc[len(df_race)] = [rc, float(js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts'][0]['value']), model_type]

            #if 'white' in c and not 'white' in race:
                #print("White:",f)
            #    race_wrong.append(f)
            #    race_mistake.append(race)
            #    race_mistake_scores.append(float(js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts'][0]['value']))
            #    pass
            #elif 'black' in c and not 'black' in race:
            #    race_wrong.append(f)
            #    race_mistake.append(race)
            #    race_mistake_scores.append(float(js['outputs'][0]['data']['regions'][0]['data']['face']['multicultural_appearance']['concepts'][0]['value']))
        except Exception as e:
            #print(f)
            missed_files.append(f)
            #print(js)

    print("Finished Model", model_type)
    print("Undetected Faces", missed_files)

print("ALL DONE!")
# NOW WE PLOT!
print("Now Plotting")
plt.figure(figsize=(16,10))
sns.set_style("whitegrid")

sns.barplot(y="Probability_Score", x="Model_Type", hue="Predicted_Age", data=df_age, hue_order=["0-25", "26-35", "36-50", "51-65", "65_plus"], capsize=0.05, order=["Beta1_Z128", "Beta10_Z8", "Beta10_Z16", "Beta10_Z32"])
plt.title("Age Prediction Probability Strength For Randomly Generated Images")
plt.savefig("Beta_Z_Rand_Gen_Age_Prob.png")
plt.clf()

sns.barplot(y="Probability_Score", x="Model_Type", hue="Predicted_Gender", data=df_gender, hue_order=["feminine", "masculine"], capsize=0.05, order=["Beta1_Z128", "Beta10_Z8", "Beta10_Z16", "Beta10_Z32"])
plt.title("Gender Prediction Probability Strength For Randomly Generated Images")
plt.savefig("Beta_Z_Rand_Gen_Gender_Prob.png")
plt.clf()

sns.barplot(y="Probability_Score", x="Model_Type", hue="Predicted_Race", data=df_race, hue_order=["white", "hispanic/brown", "asian", "black/african_american"], capsize=0.05, order=["Beta1_Z128", "Beta10_Z8", "Beta10_Z16", "Beta10_Z32"])
plt.title("Race Prediction Probability Strength For Randomly Generated Images")
plt.savefig("Beta_Z_Rand_Gen_Race_Prob.png")
plt.clf()


prop_df = (df_age["Predicted_Age"].groupby(df_age["Model_Type"])\
        .value_counts(normalize=True).rename("Proportion").reset_index())

sns.barplot(y="Proportion", x="Model_Type", hue="Predicted_Age", data=prop_df, hue_order=["0-25", "26-35", "36-50", "51-65", "65_plus"], order=["Beta1_Z128", "Beta10_Z8", "Beta10_Z16", "Beta10_Z32"])
plt.title("Age Prediction For Randomly Generated Images")
plt.savefig("Beta_Z_Rand_Gen_Age_Dist.png")
plt.clf()

prop_df = (df_gender["Predicted_Gender"].groupby(df_gender["Model_Type"])\
        .value_counts(normalize=True).rename("Proportion").reset_index())

sns.barplot(y="Proportion", x="Model_Type", hue="Predicted_Gender", data=prop_df, hue_order=["feminine", "masculine"], order=["Beta1_Z128", "Beta10_Z8", "Beta10_Z16", "Beta10_Z32"])
plt.title("Gender Prediction For Randomly Generated Images")
plt.savefig("Beta_Z_Rand_Gen_Gender_Dist.png")
plt.clf()

prop_df = (df_race["Predicted_Race"].groupby(df_race["Model_Type"])\
        .value_counts(normalize=True).rename("Proportion").reset_index())

sns.barplot(y="Proportion", x="Model_Type", hue="Predicted_Race", data=prop_df, hue_order=["white", "hispanic/brown", "asian", "black/african_american"], order=["Beta1_Z128", "Beta10_Z8", "Beta10_Z16", "Beta10_Z32"])
plt.title("Race Prediction For Randomly Generated Images")
plt.savefig("Beta_Z_Rand_Gen_Race_Dist.png")
plt.clf()

