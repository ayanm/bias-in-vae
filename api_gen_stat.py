#!/usr/bin/python
import os
import glob
import json
from clarifai.rest import ClarifaiApp
import numpy as np
from PIL import Image
import visdom
import torch
from torchvision.utils import make_grid
import matplotlib.pyplot as plt

API_KEY = 'bb8e7a2b54b641b7aedb9bf71dc25806'

#API_KEY = '6e26ce5a12f34258835961572f6d4913'

VIZ_PORT = 7097

root_path = 'outputs/'

#paths = ['ayan_celeba_B_gamma200_z32_Cstop_1e5_Cmax_15_mod3_500k_lr_decay/step_500001', 'ayan_celeba_B_gamma200_z32_Cstop_1e5_Cmax_15_mod3_500k_lr_decay/step_1500000']

#paths = ['ayan_celeba_cond_H_beta1_z64/step_1500000', 'ayan_celeba_m25_cond_H_beta10_z64/step_1500000']

#paths = ['ayan_celeba_cond_H_beta10_z32/step_1500001', 'ayan_celeba_cond_H_beta1_z64/step_1500000']


#paths = ["ayan_celeba_m75_cond_H_beta10_z64/step_1500000"]
#paths = ["ayan_celeba_Hmod_beta1_z64/step_1500000"]
#paths = ["ayan_celeba_H_beta1_z64/step_1500000"]
paths = ['ayan_celeba_Hmod_beta1_z64_actual/step_1500001']
COND, VIZ = False, False
#COND, VIZ = True, True
#paths = ['ayan_celeba_B_gamma200_z32_Cstop_1e5_Cmax_15_mod3_500k_lr_decay/step_1500000', 'ayan_celeba_B_gamma200_z32_Cstop_1e5_Cmax_15_mod3_500k_lr_decay_unmoded/step_1500000']


for path in paths:
    path = os.path.join(root_path, path, 'random_gen')
    print("*** LOOKING AT ****", path)
    files = [f for f in glob.glob(os.path.join(path, "*.png"))]
    jsons = [f for f in glob.glob(os.path.join(path, "*.json"))]

    if len(files) == 0 or (len(files) != 0 and len(files) != len(jsons)):
        # We have to call the API
        count = 0
        app = ClarifaiApp(api_key=API_KEY)
        model = app.models.get('demographics')
        try:
            for f in files:
                name = f.split("/")[-1]
                img_name = name.split(".png")[0]
                if not img_name+'_out.json' in jsons:
                    count += 1
                    response = model.predict_by_filename(f)
                    with open(os.path.join(path, img_name+'_out.json'), "w") as fw:
                        json.dump(response, fw)
        except Exception as e:
            print(e)
            exit(1)

        print("Done calling API... Processed", count, "calls to the API.")
    else:
        print("We already have the JSON responses from the API!")

    # Now use the JSONs
    jsons = [f for f in glob.glob(os.path.join(path, "*.json"))]
    print("NUM JSONs:", len(jsons))
    #assert len(jsons) == count

    male_count = 0
    female_count = 0
    male_scores = []
    female_scores = []
    miss = 0
    count = 0
    male_errs = 0
    female_errs = 0
    male_labs, female_labs = 0, 0
    male_err_scores, female_err_scores = [], []
    male_err_imgs, female_err_imgs = [], []
    male_corr_scores = []
    female_corr_scores = []
    for f in jsons:
        count += 1
        name = f.split("/")[-1]
        f_path = f.split("/")[:-1]
        json_name = name.split(".json")[0]
        json_name_split = json_name.split("_")
        if COND:
            label = int(json_name_split[json_name_split.index("label")+1])
            if label == -1:
                female_labs += 1
            else:
                male_labs += 1
        with open(f) as fr:
            try:
                js = json.loads(fr.read().replace("\'", "\""))
                gender = js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['name']
                gender_pred_score = float(js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['value'])
                #i_p = "/".join(f_path.append(name.split("_out.json")[0]+".png"))
                #print(i_p)
                #print(f_path, name)
                f_path.append(name.split("_out.json")[0]+".png")
                i_p = "/".join(f_path)
                if gender == 'masculine':
                    male_count += 1
                    if COND:
                        if label == -1:
                            female_errs += 1
                            female_err_scores.append(gender_pred_score)
                            #female_err_imgs.append(name.split("_out.json")[0]+".png")
                            female_err_imgs.append(i_p)
                        else:
                            male_corr_scores.append(gender_pred_score)
                    male_scores.append(gender_pred_score)
                elif gender == 'feminine':
                    female_count += 1
                    if COND:
                        if label == 1:
                            male_errs += 1
                            male_err_scores.append(gender_pred_score)
                            #male_err_imgs.append(name.split("_out.json")[0]+".png")
                            male_err_imgs.append(i_p)
                        else:
                            female_corr_scores.append(gender_pred_score)
                    female_scores.append(gender_pred_score)
            except Exception as e:
                miss += 1
                #print("OOPS!")
                continue

    print("MISSED FACES:", miss)
    print("Original Male Count:", male_labs, "Predicted Male Count:", male_count, "Score:", np.mean(male_scores))
    print("Original Female Count:",female_labs, "Predicted Female Count:", female_count, "Score:", np.mean(female_scores))
    print("Original Male Predicted Female Errors:", male_errs,"Score:", np.mean(male_err_scores), np.median(male_err_scores))
    print("Original Female Predicted Male Errors:", female_errs, "Score:", np.mean(female_err_scores), np.median(female_err_scores))
    print("Correct Male Predictions:", len(male_corr_scores), np.mean(male_corr_scores))
    print("Correct Female Predictions:", len(female_corr_scores), np.mean(female_corr_scores))

    # Now visualize the images generated
    if VIZ:
        viz_env_name = path.split("/")[-3]
        # Loop through the images and store them in list of Tensors
        list_gens_0 = []
        list_gens_1 = []
        for f in files:
            img = Image.open(f)
            #img.show()
            img = np.array(img, dtype=np.uint8)
            #img = np.reshape(img, (3, 64, 64))
            img_t = torch.Tensor(img)
            # Need to convert from HxWxC to CxHxW for PyTorch
            img_t = img_t.permute(2, 0, 1)
            name = f.split("/")[-1]
            img_name = name.split(".png")[0]
            img_name_split = img_name.split("_")
            label = int(img_name_split[img_name_split.index("label")+1])
            if label == -1:
                list_gens_0.append(img_t)
            elif label == 1:
                list_gens_1.append(img_t)


        list_gens_0 = make_grid(list_gens_0, normalize=False).cpu()
        list_gens_1 = make_grid(list_gens_1, normalize=False).cpu()

        viz = visdom.Visdom(port=VIZ_PORT)
        viz.images(list_gens_0, env=viz_env_name+'_random_gen', \
                opts=dict(title='Original Label Female Generations', nrow=10))
        viz.images(list_gens_1, env=viz_env_name+'_random_gen', \
                opts=dict(title='Original Label Male Generations', nrow=10))

        # Now for the error predictions
        list_female_e = []
        for f in female_err_imgs:
            img = Image.open(f)
            img = np.array(img, dtype=np.uint8)
            img_t = torch.Tensor(img)
            img_t = img_t.permute(2, 0, 1)
            list_female_e.append(img_t)
        list_female_e = make_grid(list_female_e, normalize=False).cpu()
        viz.images(list_female_e, env=viz_env_name+'_random_gen', \
                opts=dict(title='Original Female Marked Male', nrow=10))

        list_male_e = []
        for f in male_err_imgs:
            img = Image.open(f)
            img = np.array(img, dtype=np.uint8)
            img_t = torch.Tensor(img)
            img_t = img_t.permute(2, 0, 1)
            list_male_e.append(img_t)
        list_male_e = make_grid(list_male_e, normalize=False).cpu()
        viz.images(list_male_e, env=viz_env_name+'_random_gen', \
                opts=dict(title='Original Male Marked Female', nrow=10))


        print("Done!")
        print("----------------------")
