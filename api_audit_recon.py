#!/usr/bin/python
import os
import glob
import json
from clarifai.rest import ClarifaiApp
import numpy as np
from PIL import Image
import torch
from torchvision.utils import make_grid
import matplotlib.pyplot as plt

#API_KEY = 'bb8e7a2b54b641b7aedb9bf71dc25806'

#API_KEY = '6e26ce5a12f34258835961572f6d4913'
#API_KEY = '1c7852c0a8304722972ef92f947e1d3e'

API_KEY = '6fa03ced7453408b95d88d8af258c611'

root_path = "outputs"
paths = ["ayan_celeba_m75_cond_H_beta10_z64_audit", "ayan_celeba_m25_cond_H_beta10_z64_audit", "ayan_celeba_cond_H_beta10_z32_audit", "ayan_celeba_B_gamma200_z32_Cstop_1e5_Cmax_15_mod3_500k_lr_decay_unmoded_audit"]

COND = True

for path in paths:
    path = os.path.join(root_path, path, 'recons_audit')
    print("*** LOOKING AT ****", path)
    files = [f for f in glob.glob(os.path.join(path, "*recon*.png"))]
    jsons = [f for f in glob.glob(os.path.join(path, "*.json"))]

    if len(files) == 0 or (len(files) != 0 and len(files) != len(jsons)):
        # We have to call the API
        count = 0
        app = ClarifaiApp(api_key=API_KEY)
        model = app.models.get('demographics')
        try:
            for f in files:
                name = f.split("/")[-1]
                img_name = name.split(".png")[0]
                if not img_name+'_out.json' in jsons:
                    count += 1
                    response = model.predict_by_filename(f)
                    with open(os.path.join(path, img_name+'_out.json'), "w") as fw:
                        json.dump(response, fw)
        except Exception as e:
            print(e)
            exit(1)

        print("Done calling API... Processed", count, "calls to the API.")
    else:
        print("We already have the JSON responses from the API!")

    # Now use the JSONs
    jsons = [f for f in glob.glob(os.path.join(path, "*.json"))]
    print("NUM JSONs:", len(jsons))
    #assert len(jsons) == count

    male_count = 0
    female_count = 0
    male_scores = []
    female_scores = []
    miss = 0
    count = 0
    male_errs = 0
    female_errs = 0
    male_labs, female_labs = 0, 0
    male_err_scores, female_err_scores = [], []
    male_err_imgs, female_err_imgs = [], []
    male_corr_scores = []
    female_corr_scores = []
    black_male_errs, black_male_corrs = [], []
    black_female_errs ,black_female_corrs = [], []
    white_male_errs, white_male_corrs = [], []
    white_female_errs, white_female_corrs = [], []
    black_female_count, white_female_count, black_male_count, white_male_count = 0, 0, 0, 0
    bf_miss, wf_miss, bm_miss, wm_miss = 0, 0, 0, 0
    for f in jsons:
        count += 1
        name = f.split("/")[-1]
        f_path = f.split("/")[:-1]
        json_name = name.split(".json")[0]
        json_name_split = json_name.split("_")
        #if COND:
        #    label = int(json_name_split[json_name_split.index("label")+1])
        #    if label == -1:
        #        female_labs += 1
        #    else:
        #        male_labs += 1
        with open(f) as fr:
            try:
                js = json.loads(fr.read().replace("\'", "\""))
                gender = js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['name']
                gender_pred_score = float(js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['value'])
                #i_p = "/".join(f_path.append(name.split("_out.json")[0]+".png"))
                #print(i_p)
                #print(f_path, name)
                f_path.append(name.split("_out.json")[0]+".png")
                i_p = "/".join(f_path)
                if "black_female" in json_name:
                    black_female_count += 1
                elif "black_male" in json_name:
                    black_male_count += 1
                elif "white_female" in json_name:
                    white_female_count += 1
                elif "white_male" in json_name:
                    white_male_count += 1
                if gender == 'masculine':
                    male_count += 1
                    #if COND:
                    #    if label == -1:
                    #        female_errs += 1
                    #        female_err_scores.append(gender_pred_score)
                            #female_err_imgs.append(name.split("_out.json")[0]+".png")
                    #         female_err_imgs.append(i_p)
                    #     else:
                    #         male_corr_scores.append(gender_pred_score)
                    # male_scores.append(gender_pred_score)
                    if 'black_female' in json_name:
                        black_female_errs.append(gender_pred_score)
                    elif 'white_female' in json_name:
                        white_female_errs.append(gender_pred_score)
                    elif 'black_male' in json_name:
                        black_male_corrs.append(gender_pred_score)
                    elif 'white_male' in json_name:
                        white_male_corrs.append(gender_pred_score)
                elif gender == 'feminine':
                    female_count += 1
                    #if COND:
                    #    if label == 1:
                    #        male_errs += 1
                    #        male_err_scores.append(gender_pred_score)
                    #        #male_err_imgs.append(name.split("_out.json")[0]+".png")
                    #        male_err_imgs.append(i_p)
                    #    else:
                    #        female_corr_scores.append(gender_pred_score)
                    # female_scores.append(gender_pred_score)
                    if 'black_female' in json_name:
                        black_female_corrs.append(gender_pred_score)
                    elif 'white_female' in json_name:
                        white_female_corrs.append(gender_pred_score)
                    elif 'black_male' in json_name:
                        black_male_errs.append(gender_pred_score)
                    elif 'white_male' in json_name:
                        white_male_errs.append(gender_pred_score)

            except Exception as e:
                if 'black_female' in json_name:
                    bf_miss += 1
                elif 'white_female' in json_name:
                    wf_miss += 1
                elif 'black_male' in json_name:
                    bm_miss += 1
                elif 'white_male' in json_name:
                    wm_miss += 1
                #print("OOPS!")
                continue

    print("MISSED FACES... Black Female:", bf_miss, "Black Male:", bm_miss, "White Female:", wf_miss, "White Male:", wm_miss)
    #print("Original Male Count:", male_labs, "Predicted Male Count:", male_count, "Score:", np.mean(male_scores))
    #print("Original Female Count:",female_labs, "Predicted Female Count:", female_count, "Score:", np.mean(female_scores))
    #print("Original Male Predicted Female Errors:", male_errs,"Score:", np.mean(male_err_scores), np.median(male_err_scores))
    #print("Original Female Predicted Male Errors:", female_errs, "Score:", np.mean(female_err_scores), np.median(female_err_scores))
    #print("Correct Male Predictions:", len(male_corr_scores), np.mean(male_corr_scores))
    #print("Correct Female Predictions:", len(female_corr_scores), np.mean(female_corr_scores))
    print("Black Female. Count:", black_female_count, "Correct:", len(black_female_corrs), "Correct Score:", np.mean(black_female_corrs), "Wrong:", len(black_female_errs), "Wrong Score:", np.mean(black_female_errs))
    print("Black Male. Count:", black_male_count, "Correct:", len(black_male_corrs), "Correct Score:", np.mean(black_male_corrs), "Wrong:", len(black_male_errs), "Wrong Score:", np.mean(black_male_errs))
    print("White Female. Count:", white_female_count, "Correct:", len(white_female_corrs), "Correct Score:", np.mean(white_female_corrs), "Wrong:", len(white_female_errs), "Wrong Score:", np.mean(white_female_errs))
    print("White Male. Count:", white_male_count, "Correct:", len(white_male_corrs), "Correct Score:", np.mean(white_male_corrs), "Wrong:", len(white_male_errs), "Wrong Score:", np.mean(white_male_errs))


