Done Parsing!
Result of Demographics
----------------------
Age
*******
{'0-25': 38, '26-35': 4, '36-50': 144, '51-65': 14, '65_plus': 0}
^^^^^^^^^^^^^^^^
Probability Scores
{'0-25': '0.41', '26-35': '0.37', '36-50': '0.43', '51-65': '0.37', '65_plus': 0}
*******
Gender
{'masculine': 138, 'feminine': 62}
^^^^^^^^^^^^^^^^
Probability Scores
{'masculine': '0.82', 'feminine': '0.78'}
*******
Race
{'white': 107, 'hispanic, latino, or spanish origin': 61, 'asian': 27, 'american indian or alaska native': 0, 'middle eastern or north african': 0, 'black or african american': 5, 'native hawaiian or pacific islander': 0}
^^^^^^^^^^^^^^^^
Probability Scores
{'white': '0.55', 'hispanic, latino, or spanish origin': '0.46', 'asian': '0.50', 'american indian or alaska native': 0, 'middle eastern or north african': 0, 'black or african american': '0.49', 'native hawaiian or pacific islander': 0}
---------------------
Missed Faces: 0
Files: []
