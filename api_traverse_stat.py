import pandas as pd
import statsmodels.api as sm
from clarifai.rest import ClarifaiApp
import glob
import json
import os
import numpy as np

#API_KEY = ''
#API_KEY = 'bb8e7a2b54b641b7aedb9bf71dc25806'

API_KEY = '6e26ce5a12f34258835961572f6d4913'

root_path = 'outputs/'

paths = ['ayan_celeba_B_gamma200_z32_Cstop_1e5_Cmax_15_mod3_500k_lr_decay/step_500001', 'ayan_celeba_B_gamma200_z32_Cstop_1e5_Cmax_15_mod3_500k_lr_decay/step_1500000']


for path in paths:
    '''
    Use the Clarifai API to get predictions for random face generations.
    Then use Statistical Model to score the dimensions based on a attr.
    like gender.
    '''
    path = os.path.join(root_path, path, 'random_traverse')
    print("****PATH*****: ", path)
    files = [f for f in glob.glob(os.path.join(path, "*.png"))]
    jsons = [f for f in glob.glob(os.path.join(path, "*.json"))]

    if len(files) == 0 or (len(files) != 0 and len(files) != len(jsons)):

        app = ClarifaiApp(api_key=API_KEY)
        model = app.models.get('demographics')
        count = 0
        try:
            # File name format:
            # outputs/ayan_celeba_B_gamma200_z32_Cstop_1e5_Cmax_15/step_200000/random_traverse/random_step_200000_z_10_dim_3_val_-3.0.json
            for f in files:
                name = f.split("/")[-1]
                img_name = name.split(".png")[0]
                if not img_name+'_out.json' in jsons:
                    count += 1
                    response = model.predict_by_filename(f)
                    with open(os.path.join(path, img_name+"_out.json"), "w") as fw:
                        json.dump(response, fw)
        except Exception as e:
            # Will get here if we face an API error
            print(e)
            exit(1)

        print("Done calling the API... Processed", count, "calls to the API.")
    else:
        print("We already have the JSON responses from the API")

    # Now get all JSONs
    jsons = [f for f in glob.glob(os.path.join(path, "*.json"))]
    df = pd.DataFrame(columns=["img_id", "z_dim", "interpolation_value", \
            "predicted_gender", "feminine_prediction_confidence"])
    #print("JSONS:", jsons)
    miss = 0
    list_dims = []
    for f in jsons:
        name = f.split("/")[-1]
        json_name = name.split(".json")[0]
        # Split the name based on "_" to get individual components from the filename
        split_id = json_name.split("_")
        img_id = int(split_id[split_id.index("z") + 1])
        z_dim = int(split_id[split_id.index("dim") + 1])
        if z_dim not in list_dims:
            list_dims.append(z_dim)
        #if z_dim not in list_dims:
        interpol_val = float(split_id[split_id.index("val") + 1])
        with open(f) as fr:
            try:
                js = json.loads(fr.read().replace("\'", "\""))
                gender = js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['name']
                #gender_count[gender] += 1
                gender_pred_score = float(js['outputs'][0]['data']['regions'][0]['data']['face']['gender_appearance']['concepts'][0]['value'])
                if gender == 'masculine':
                    gender_pred_score = 1.0 - gender_pred_score
                df.loc[len(df)] = [img_id, z_dim, interpol_val, gender, round(gender_pred_score, 3)]
            except:
                # We will enter this only when the API fails to detect a face.
                #print("ERROR")
                miss += 1
                continue
    print("MISSED FACES BY API:", miss)
    # Separate out male and female based on the non-interpolated random images.
    # TODO: See if we actually need to separate out male and female.
    #list_male = list(df_orig[df_orig.predicted_gender=='masculine'].img_id)
    #df_f = df[~df.img_id.str.contains('|'.join(list_male))]
    #df_m = df[df.img_id.str.contains('|'.join(list_male))]

    # Later if we have to do this during the training process, we will not know at first
    # WHICH dimensions actually would learn variations.
    # TODO: Maybe we can, by sending the dimensions that have KL Divergence > some non-zero value.
    # Dimensions that learn nothing actually have KLD near to zero. So maybe some value like 0.1 (??)
    #list_dims = np.array(list_dims)

    results = []
    coefs = []
    #print("DF")
    #print(df)
    list_dims = np.array(list_dims)
    #print(list_dims)

    for dim in list_dims:
        stat_df = df[df.z_dim == dim]#.groupby(['interpolation_value']).median().reset_index()
        if len(stat_df) == 0:
            continue
        #print(stat_df)
        # Fit a linear model
        lin_model = sm.OLS(stat_df['feminine_prediction_confidence'].astype(float),stat_df['interpolation_value'].astype(float))
        result = lin_model.fit(disp=0)
        results.append(result)
        coefs.append(abs(result.params[0]))

    coefs = np.array(coefs)
    sort_idx = np.argsort(coefs)[::-1]
    #print("The dimensions with statistically significant slope (ordered)")
    print("The dimensions with the scores (ordered)")
    print("*****************************************")
    for idx in sort_idx:
        #if results[idx].pvalues[0] <= 0.05:
        print("Dim:",list_dims[idx], "Score:", results[idx].params[0])
        #print("------------------------------")
        #print(results[idx].params[0])

    # Return the sorted dim numbers and the corresponding scores
    #return list_dims[sort_idx], coefs[sort_idx]
